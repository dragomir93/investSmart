<form id="searchForm" name="searchForm" method="post" >
    <div class="form-group">
        <label for="" class="control-label">Email</label>
        <div>
            <input type="email" class="form-control" id="searchEmail" name="" placeholder="Email" >
        </div>
    </div>
    <div class="form-group">
        <label for="" class="control-label">First Name</label>
        <div>
            <input type="text" class="form-control" id="searchFirstName" name="" placeholder="First Name" >
        </div>
    </div>
    <div class="form-group">
        <label for="" class="control-label">Last Name</label>
        <div>
            <input type="text" class="form-control" id="searchLastName" name="" placeholder="Last Name" >
        </div>
    </div>
    <div class="form-group">
        <label for="" class="control-label">Country</label>
        <div>
            <input type="text" class="form-control" id="searchCountry" name="" placeholder="Country" >
        </div>
    </div>
    <div class="form-group">
        <label for="" class="control-label">Partner Type</label>
        <div>
            <input type="text" class="form-control" id="searchPartnerType" name="" placeholder="Partner Type" >
        </div>
    </div>
    <div class="form-group submit-group">
        <button type="submit" name="search" id="search" class="btn btn-primary" style="margin-top: 10px;margin-bottom: 10px; letter-spacing: 2px;">SEARCH</button>
    </div>
                                     
</form>

