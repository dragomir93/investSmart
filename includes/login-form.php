<?php
error_reporting(0);
?>
<form class="login" name="login-form" method="post" action="accountdetails.php">
    <div class="form-group pass">
        <label for="username" class="col-sm-2 control-label">Username</label>
        <div class="col-sm-10">
            <input id="userVal" type="text" class="form-control" name="loginUSERNAME" placeholder="email address" value="<?php echo $_COOKIE['IBProfileUsername'] ?>" >
        </div>
    </div>
    <div class="form-group pass">
        <label for="password" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input  type="password" class="form-control" name="loginPASSWORD" placeholder="password" value="">
        </div>
    </div>
    <button class="btn btn-danger" type="submit" name="LOGIN" id="loginButton">Login</button>  
    <div class="form-group remember-me">
        <i>Remember Me?</i><input type="checkbox" name="remember-me" class="check">
        <?php if (stripos($_SERVER['REQUEST_URI'], 'login.php')){ ?>
            <a id="showRequestForm" onclick="darkovaFunkcija()">Forgot Password?</a>  
        <?php }else{?>
             <a id="showRequestForm" href="login.php">Forgot Password?</a>

        <?php } ?>
        

    </div>

    <?php
    if ($_SESSION['message'] != "") {
        echo "<div class='error_msg' style=''><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;" . $_SESSION['message'] . " </div>";
        unset($_SESSION['message']);
        session_destroy();
    } else {
        //echo "<div class='error_msg' style=''> Please LogIn! </div>";
    }
    ?>
    <div class="login-procesbar" id="login-processing" hidden>
        <!--                                <div class="loading">-->
        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
        <span class="sr-only">Loading...</span>
        <!--                                </div>-->
        <p id="processingText">Processing... Please wait ...</p>
    </div>
</form>

