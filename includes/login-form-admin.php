<?php
error_reporting(0);
?>
<form class="login" name="login-form" method="post" action="../public/dashboard.php">
    <div class="form-group pass">
        <label for="username" class="col-sm-2 control-label">Username</label>
        <!--<span>Enter Your Security Password</span>-->
        <div class="col-sm-10">
            <input id="userVal" type="text" class="form-control" name="username" placeholder="username" value="">
        </div>
    </div>
    <div class="form-group pass">
        <label for="password" class="col-sm-2 control-label">Password</label>
        <!--<span>Enter Your Security Password</span>-->
        <div class="col-sm-10">
            <input id="passVal" type="password" onClick="validateForm()" class="form-control" name="password" placeholder="password" value="">
        </div>
    </div>
    <button class="btn btn-danger" type="submit" name="adminLogin" id="adminLoginButton">Login</button>                            
    <div id="login-processing" hidden>
        <!--                                <div class="loading">-->
        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
        <span class="sr-only">Loading...</span>
        <!--                                </div>-->
        <p id="processingText">Processing... Please wait ...</p>
    </div>

    <?php
    if ($_SESSION['adminMessage']!="") {
        $sessionMessage = $_SESSION['adminMessage'];
        echo "<div id='session-message' class='error_msg' style='font-size:25px;'>" . $_SESSION['adminMessage'] . " </div>";
        unset($_SESSION['adminMessage']);
        session_destroy();
    }
    ?>
</form>

