<nav class="navbar navbar-inverse dashboard-menu" role="navigation">
    <div class="container-fluid">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
                    <span class="glyphicon glyphicon-home"></span>
                    Dashboard
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <!--                        <li class=" homeDashboard">
                                                <a href="#">Home<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a>
                                            </li>-->
                    <li class="pendingRequests">
                        <a href="#">Pending Requests<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a>
                    </li>
                    <li class="editPartner">
                        <a href="#">Edit Partner<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-zoom-in"></span></a>
                    </li>
                    <li class="create-campaign">
                        <button id="createCampaignBtn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">CREATE CAMPAIGN</button>
                        <?php include("../public/create-campaign.php"); ?>
                    </li>
                    <li class="edit-campaign">
                        <button id="editCampaignBtn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal1">EDIT CAMPAIGN</button>
                        <?php include("../public/edit-campaign.php"); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
