<div class="container-fluid " id="personalDetailsContainer"> <!-- start of container -->
    <div class="row">
        <div class="col-lg-12 acount-details">
            <div class="mainDetailsHeader" ><span class="glyphicon glyphicon-user mainGlyphicon"></span>PERSONAL DETAILS</div>
            <!--Account Information-->
            <div class="detailsHeader"> <span class="glyphicon glyphicon-user detailsGlyphicon"></span> Edit Profile</div>
            <form class="form-horizontal" role="form" method="post" action="" id="userDetailsUpdateForm" >
                <div class="accountdetails-form1">
                    <div class="form-group">
                        <label for="FN" class="col-sm-2 col-sm-offset-1 control-label">First Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="IBFirstName" name="FN" value="<?php echo $firstName ?>" required pattern="[A-Z][a-z]+" title="First Letter MUST be CAPITAL! Example:John">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="LN" class="col-sm-2 col-sm-offset-1 control-label">Last Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="IBLastName" name="LN" value="<?php echo $lastName ?>" pattern="[A-Z][a-z]+" title="First Letter MUST be CAPITAL! Example:Smith" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="PHONE" class="col-sm-2 col-sm-offset-1 control-label">Phone</label>
                        <div class="col-sm-5">
                            <input type="tel" class="form-control" id="IBphone" name="PHONE" value="<?php echo $phone ?>" title="+(381)646644555" pattern="\+\(\d{1,4}\)\d{8,}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="MOBILE" class="col-sm-2 col-sm-offset-1 control-label">Mobile</label>
                        <div class="col-sm-5">
                            <input type="tel" class="form-control" id="IBmobile" name="MOBILE" value="<?php echo $mobile ?>" title="+(381)646644555" pattern="\+\(\d{1,4}\)\d{8,}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="COUNTRY" class="col-sm-2 col-sm-offset-1  control-label">Country</label>
                        <div class="col-sm-5">
                            <!--<input type="text" class="form-control" id="IBCountry" name="COUNTRY" value="<?php //echo $country  ?>" >-->
                            <!-- Country list -->
                            <select id="filter-projects" title="Select Your Country of Residence" required class="countryList CountryList form-control" name="Country">
                                <option value='<?php echo $country  ;?>' selected id=""><?php echo $country  ;?></option>
                                <?php
//                                $url = "http://192.168.0.183:200/ToolService.svc?wsdl";
//                                $client = new SoapClient($url);
//                                //$res = $client->__getFunctions();
                                $quote2 = $client->GetAllCountries();
                                $response2 = $quote2->GetAllCountriesResult;

                                for ($i = 0; $i < count($response2->string); $i++) {
                                    $country = $response2->string[$i];
                                    echo '<option  class="contryList" value="' . htmlspecialchars($country) . '" >' . htmlspecialchars($country) . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="CR" class="col-sm-2 col-sm-offset-1  control-label">City/Region</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="IBCity" name="CR" value="<?php echo $city ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ADDRESS" class="col-sm-2 col-sm-offset-1  control-label">Address</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="IBAddress" name="ADDRESS" value="<?php echo $address ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ZIP" class="col-sm-2 col-sm-offset-1  control-label">ZIP/POSTAL CODE</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="IBZip" name="ZIP" value="<?php echo $ZIP ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="COMPANY" class="col-sm-2 col-sm-offset-1  control-label">Company</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="IBCompany" name="COMPANY" value="<?php echo $company ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="URL" class="col-sm-2 col-sm-offset-1  control-label">WebSite URL</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="IBUrl" name="URL"  value="<?php echo $websiteUrl ?>" pattern="(http|https):\/\/(www\.)?[\S]+" title="http://www.site.com">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="PARTNERTYPE" class="col-sm-2 col-sm-offset-1  control-label">Partner Type</label>
                        <div class="col-sm-5">
                            <!--<input type="text" class="form-control" id="IBPartnerType" name="PARTNERTYPE"  value="<?php //echo $partnerType     ?>">-->
                            <select id="filter-projects" class="form-control partnershipType" name="partnership" required>
                                <option selected value="<?php echo $partnerType ?>"><?php echo $partnerType ?></option>
                                <option  value="Junior Partner">CPA</option>
                                <option value="Senior Partner">Revenue Partnership</option>
<!--                                <option value="Expert Partner">Expert Partner</option>
                                <option value="VIP Partner">VIP Partner</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="form-group1 register-button-home">
                        <div class="col-sm-4 col-sm-offset-4">
                            <input id="updateDetails" name="UPDATE" type="submit" value="UPDATE DETAILS" class="register btn btn-primary ">
                        </div>
                    </div>
                </div>

                <div class="userDetailsUpdateResult">

                </div>
                <div class="updatingDetailsAnimation" hidden>
                    <i class="fa fa-spinner" ></i>
                </div>
            </form>
        </div>
    </div>
</div>

