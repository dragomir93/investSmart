<div class="container-fluid" id="paymentDetailsContainer">
    <div class="row">
        <div class="col-lg-12 acount-details">
            <div class="mainDetailsHeader" ><span class="glyphicon glyphicon-credit-card mainGlyphicon"></span>PAYMENT DETAILS</div>
            <!-- Password Settings -->
            <div class="detailsHeader"> <span class="glyphicon glyphicon-cog detailsGlyphicon"></span> Payment Details Settings</div>
            <form class="form-horizontal" role="form" method="post" action="" id="updatePaymentDetails">
                <div class="accountdetails-form1 ">
                    <div class="form-group ">
                        <label for="" class="col-sm-2 control-label ">User Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="" value="<?php echo $firstName . ' ' . $lastName; ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Method</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="paymentMethod" name="" value="Bank Wire" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Account Number</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="accountNumber" name="" value="<?php echo $accountNumber; ?>" placeholder="Enter Your Account Number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Bank Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $bankName; ?>" id="bankName" name="" placeholder="Enter Your Bank Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Bank Address</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $bankAddress; ?>" id="bankAddress" name="" placeholder="Enter Your Bank Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Country" class="col-sm-2 control-label">Bank Country</label>
                        <div class="col-sm-5">
                            <!-- Country list -->
                            <select id="bankCountry" title="Select Your Country of Residence" class="countryList CountryList form-control" name="paymentCountry">
                                <option value="<?php echo $bankCountry; ?>" selected ><?php echo $bankCountry; ?></option>
                                <?php
                                $quote2 = $client->GetAllCountries();
                                $response2 = $quote2->GetAllCountriesResult;
                                for ($i = 0; $i < count($response2->string); $i++) {
                                    $country = $response2->string[$i];
                                    echo '<option  class="contryList" value="' . htmlspecialchars($country) . '" >' . htmlspecialchars($country) . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Bank State/Province</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $bankState; ?>" id="bankState" name="" placeholder="State/Province">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Swift</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $swift; ?>" id="swift" name="" placeholder="Swift">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">IBAN</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $iban; ?>" id="iban" name="" placeholder="IBAN">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">ABA/Routing Number</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $aba; ?>" id="aba" name="" placeholder="Enter Your ABA/Routing Number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Bank Correspondence</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" value="<?php echo $correspondence; ?>" id="correspondence" name="" placeholder="If Required by Bank">
                        </div>
                    </div>
                    <?php
                    if ($accountNumber == "") {
                        echo "<div class='form-group1 register-button-home'>
                                        <div class='col-sm-4'>
                                            <input id='paymentButton'  type='submit' name='addPaymentButton' value='ADD PAYMENT DETAILS' class='register btn btn-primary'>
                                        </div>
                                        </div>";
                    } else {
                        echo "<div class='form-group1 register-button-home'>
                                        <div class='col-sm-4'>
                                            <input id='paymentButton'  type='submit' name='editPaymentButton' value='EDIT PAYMENT DETAILS' class='register btn btn-primary'>
                                        </div>
                                        </div>";
                    }
                    ?>
                </div>  
                <div id="paymentUpdate">
                    
                </div>
                <div class="updatingPaymentAnimation" hidden>
                    <i class="fa fa-spinner" ></i>
                </div>
            </form>
        </div>
    </div>
</div> 

