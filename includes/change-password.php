<div class="container-fluid" id="changePasswordContainer"> <!-- start of container -->
    <div class="row">
        <div class="col-lg-12 acount-details">
            <div class="mainDetailsHeader" ><span class="glyphicon glyphicon-lock mainGlyphicon"></span>CHANGE PASSWORD</div>
            <!-- Password Settings -->
            <div class="detailsHeader"> <span class="glyphicon glyphicon-wrench detailsGlyphicon"></span> Password Settings</div>
            <form class="form-horizontal" role="form" method="post" action="" id="updatePasswordForm">
                <div class="accountdetails-form1">
                    <div class="form-group">
                        <label for="Password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-5 newPassTooltip">
                            <input type="text" class="form-control" id="oldPasswordIB" value="<?php echo $password ?>" name="Password" readonly >
                            <span class="newPassTooltipText">This is Your Current Password</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="NewPassword" class="col-sm-2 control-label">New Password</label>
                        <div class="col-sm-5 newPassTooltip">
                            <input type="password" title="" class="form-control" id="NewPassword" name="NewPassword" placeholder="Enter Your New Password" 
                                   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
                                           if (this.checkValidity())
                                               form.confirmNewPassword.pattern = this.value;" title="Combine UPPER/lower Case and Number(s). At Least 5 Chars Long">
                            <span class="newPassTooltipText">Combine UPPER/lower Case and Number(s). At Least 5 Chars Long</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmNewPassword" class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-5 newPassTooltip">
                            <input type="password" title="Combine UPPER/lower Case and Number(s). At Least 5 Chars Long" class="form-control" id="confirmNewPassword" 
                                   name="confirmNewPassword" placeholder="Reenter Your New Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}"
                                   onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" title="Combine UPPER/lower Case and Number(s). At Least 5 Chars Long">
                            <span class="newPassTooltipText">Combine UPPER/lower Case and Number(s). At Least 5 Chars Long</span>
                        </div>
                    </div>
                    <div class="form-group1 register-button-home">
                        <div class="col-sm-4 col-sm-offset-2 ">
                            <input id="updatePassword"  type="submit" name="updatePassword" disabled value="SAVE PASSWORD" class="register btn btn-primary">
                        </div>
                    </div>
                </div>
                <div class="passUpdateResult">

                </div>
                <div class="changePasswordAnimation" hidden>
                    <i class="fa fa-spinner" ></i>
                </div>
            </form>
        </div>
    </div>
</div>

