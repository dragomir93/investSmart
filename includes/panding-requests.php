<!--CREATING ADMIN GRID FOR PENDING PARTNERS-->
<?php
if ($ADMIN == TRUE) {
    $pendingPartners = $client->GetPendingPartners(array('token' => $_SESSION['adminToken']));
    $partnerData = $pendingPartners->GetPendingPartnersResult;
    $promenljiva = count($partnerData->DMPending);
    
    if($promenljiva == 1){
        $partnerData->DMPending = array($partnerData->DMPending);
    }
    
    if (count($partnerData->DMPending) < 1) {

        echo "<div style='font-size:30px;text-align:center;'>NO PENDING REQUESTS</div>";

    } elseif (count($partnerData->DMPending) > 0) {
        for ($i = 0; $i < count($partnerData->DMPending); $i++) {

            $partnerID = $partnerData->DMPending[$i]->Id;
            $partnerFirstName = $partnerData->DMPending[$i]->FirstName;
            $partnerLastName = $partnerData->DMPending[$i]->LastName;
            $partnerEmail = $partnerData->DMPending[$i]->Email;
            $partnerCountry = $partnerData->DMPending[$i]->Country;
            $partnerCompany = $partnerData->DMPending[$i]->Company;
            $partnerWebsiteUrl = $partnerData->DMPending[$i]->WebsiteUrl;
            $partnerPartnerType = $partnerData->DMPending[$i]->PartnerType;
            $partnerNumberOfClients = $partnerData->DMPending[$i]->NumberOfClients;
            $partnerDepositAmount = $partnerData->DMPending[$i]->DepositAmount;
            $partnerComment = $partnerData->DMPending[$i]->Comment;
            $partnerPhone = $partnerData->DMPending[$i]->Phone;
            //echo var_dump($partnerPhone);
            echo "<div class='forms-wrapper'><form method='POST' action='' >
                    <div class='form-group left-group'>
                        <label for='ID' class='control-label'>ID</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerID' name='ID$i' readonly>
                        </div>
                    </div> 

                    <div class='form-group left-group'>
                        <label for='firstName' class='control-label'>First Name</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerFirstName' name='firstName' readonly>
                        </div>
                    </div> 
                    <div class='form-group left-group'>
                        <label for='lastName' class='control-label'>Last Name</label>
                        <div>
                            <input type='text' class='form-control ' id='' value='$partnerLastName' name='lastName' readonly>
                        </div>
                    </div>

                    <div class='form-group left-group'>
                        <label for='Email' class='control-label'>Email</label>
                        <div>
                            <input type='email' class='form-control' id='' value='$partnerEmail' name='Email' readonly>
                        </div>
                    </div>
                    <div class='form-group left-group'>
                        <label for='Country' class='control-label'>Country</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerCountry' name='Country' readonly>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for='Company' class='control-label'>Company</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerCompany' name='Company' readonly>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='websiteUrl' class='control-label'>Website Url</label>
                        <div>
                            <input type='url' class='form-control' id='' value='$partnerWebsiteUrl' name='websiteUrl' readonly>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for='partnerType' class='control-label'>Partner Type</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerPartnerType' name='partnerType' readonly>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for='numberOfClients' class='control-label'>Number of Clients</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerNumberOfClients' name='numberOfClients' readonly>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for='depositAmount' class='control-label'>Deposit Amount</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerDepositAmount' name='depositAmount' readonly>
                        </div>
                    </div>
                    
                    <div class='form-group'>
                        <label for='phone' class='control-label'>Phone</label>
                        <div>
                            <input type='text' class='form-control' id='' value='$partnerPhone' name='partnerPhone' readonly>
                        </div>
                    </div>
                    
                    <div class='form-group'>
                        <label for='Comment' class='control-label'>Comment</label>
                        <div>
                            <textarea class='form-control' rows='2' style='background-color:#E0E1DF; border:none;' name='Comment' readonly>$partnerComment</textarea>
                        </div>
                    </div>
                    <div class='form-group1 register-button-home'>
                        <div class='col-sm-2 panding-request'>
                            <a href='#'><input id='' onClick='processBar();'  name=$i type='submit' value='Approve' class='register btn btn-primary '></a>
                               
                        </div>
                    </div>
                </form>";
            echo"<form action='delete.php' method='post' onsubmit=\"return confirm('THIS WILL PERMANENTLY DELETE THIS USER!!! Are you sure you want to continue?');\">";
            echo"<button type='submit' class='register btn btn-primary' name='delete' value='$partnerID'>Delete</button>";
            echo"</form></div>";
            $_SESSION['counter'] = $i;
        }
    }
}
else {
    echo "<div style='font-size:30px;text-align:center;'>YOU HAVE NO PERMISSION TO SEE THIS DETAILS</div>";
}
?>
