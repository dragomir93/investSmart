<div class="account-sidebar animated fadeInLeft" id="sidebar-wrapper">
    <ul class="sidebar-nav ">
        <li class="sidebar-brand">
            <p>
                Options
            </p>
        </li>
        <li>
            <a href="#" id="personalInformation">PERSONAL INFORMATION</a>
        </li>
        <li>
            <a href="#" id="personalDetails">PERSONAL DETAILS</a>
        </li>
        <li>
            <a href="#" id="changePassword">CHANGE PASSWORD</a>
        </li>
        <!--                    <li>
                                <a href="#" id="marketingInformation">MARKETING INFORMATION</a>
                            </li>-->
        <li>
            <a href="#" id="paymentDetails">PAYMENT DETAILS</a>
        </li>
    </ul>
</div>
