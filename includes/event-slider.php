<div id="owl-demo">
    <div class="item">
        <div id="slideshow" class="  revealOnScroll" data-animation="fadeInLeft">
            <?php include('../includes/slideshow-first.php'); ?>
        </div>
        <div id="news" class="  revealOnScroll" data-animation="fadeInRight">
            <?php include('../includes/service-news.php'); ?>
            <div id="titleContent"><h1 id="Title"></h1></div>
            <div id="Desc"></div>
        </div>
    </div>
    <div class="item">
        <div class="slideImg">
            <?php include('../includes/slideshow-second.php'); ?>
        </div>
        <div class="newsSlide">
            <?php include('../includes/services-news-second.php'); ?>
            <div id="titleContent2"><h1 id="Title2"></h1></div>
            <div id="Desc2"></div>
        </div>
    </div>
</div>
