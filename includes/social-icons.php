<ul class="pull-right">
    <li>
        <a href="https://www.facebook.com/Stock-STP-1668768046754499/">
            <span class="fa-stack fa-lg" >
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
            </span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/StockSTP">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
            </span>
        </a>
    </li>
    <li>
        <a href="https://www.youtube.com/channel/UCwW-v9OR3ETgdP8vpwHZZdg">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i>
            </span>
        </a>
    </li>
    <li>
        <a href="https://plus.google.com/u/1/b/112823802050358473718/112823802050358473718">
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
            </span>
        </a>
    </li>
</ul>

