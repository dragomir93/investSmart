<?php

        $firstNameErr = $lastNameErr = $emailErr = $genderErr = $websiteErr = $passComErr = $passErr = $commentErr = "";
        $firstName = $lastName = $email = $gender = $comment = $website = $passCom = $pass = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["FirstName"])) {
                $firstNameErr = "Name is required";
            } else {
                $firstName = test_input($_POST["FirstName"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $firstName)) {
                    $nameErr = "Only letters and white space allowed";
                }
            }
            if (empty($_POST["LastName"])) {
                $lastNameErr = "Name is required";
            } else {
                $lastName = test_input($_POST["LastName"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $lastName)) {
                    $lastNameErr = "Only letters and white space allowed";
                }
            }
            if (empty($_POST["Email"])) {
                $emailErr = "Email is required";
            } else {
                $email = test_input($_POST["Email"]);
                // check if e-mail address is well-formed
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid email format";
                }
            }
            if (empty($_POST["Password"])) {
                $passErr = "Name is required";
            } else {
                $pass = test_input($_POST["Password"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $pass)) {
                    $passErr = "Only letters and white space allowed";
                }
            }
            if (empty($_POST["PasswordConfirm"])) {
                $passComfErr = "Name is required";
            } else {
                $passCom = test_input($_POST["PasswordConfirm"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $passCom)) {
                    $passComfErr = "Only letters and white space allowed";
                }
            }
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

