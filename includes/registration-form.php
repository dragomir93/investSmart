<form class="container form-horizontal register-form form-style  animated bounceInUp" style="margin-bottom: 0px;margin-top: 0px; float:none; overflow:visible" role="form" method="post" action="test-registration.php" id="regFormSubmitJS"> <!---->
    <!--<input type="text" id="codic" >-->
    <div class="form-left col-md-6">
        <div class="form-group">
            <label for="FirstName" class="col-sm-4 control-label">First Name<span style="color: red;">&nbsp;*</span></label>
            <div class="col-sm-8 newPassTooltip">
                <input type="text" class="form-control Fname" id="Fname" required pattern="[A-Z][a-z]+" title="First letter must be Uppercase Example:John" name="FirstName" placeholder="First Name" value="<?php echo $firstName; ?>">
                <span class="newPassTooltipText">Enter First Name</span>
            </div>
        </div>
        <div class="form-group">
            <label for="LastName" class="col-sm-4 control-label">Last Name<span style="color: red;">&nbsp;*</span></label>
            <div class="col-sm-8 newPassTooltip">
                <input pattern="[A-Z][a-z]+" title="First letter must be Uppercase Example:Smith" type="text" class="form-control Lname" id="Lname" name="LastName" required placeholder="Last Name" value="<?php echo $lastName; ?>">
                <span class="newPassTooltipText">Enter Last Name</span>
            </div>
        </div>
        <div class="form-group">
            <label for="Email" class="col-sm-4 control-label">Email<span style="color: red;">&nbsp;*</span></label>
            <div class="col-sm-8 newPassTooltip">
                <input type="email" class="form-control Email" id="email" name="Email" pattern="([a-z0-9]{0,})+(?:.[a-z0-9]{1,})+@([a-z]{1,}\.[a-z]{1,})+$" title="example@domain.com" required placeholder="example@domain.com" value="<?php echo $email; ?>">
                <span class="newPassTooltipText">Enter Email</span>
            </div>
        </div>
        <div class="form-group">
            <label for="Phone" class="col-sm-4 control-label inputFields">Primary Phone<span style="color: red;">&nbsp;*</span></label>
            <div class="col-sm-8 newPassTooltip">
                <input title="Enter Mobile Phone Number" type="tel" class="form-control Phone" id="phone" name="mobilePhone" minlength="8" required placeholder="">
                <span class="newPassTooltipText">Enter Primary Phone Number</span>
            </div>
        </div>
        <div class="form-group">
            <label for="StabilePhone" class="col-sm-4 control-label">Alternative Phone<sup style="color: #62b646;">(optional)</sup></label>
            <div class="col-sm-8 newPassTooltip">
                <input  class="form-control" id="phone-stable" name="phone-stable" type="text"  minlength="8" placeholder="+(XXX)XXXXXXX" title="+(381)646644555" pattern="\+\(\d{1,4}\)\d{8,}">
                <span class="newPassTooltipText">Enter Alternative Phone Number</span>
            </div>
        </div>
        <div class="form-group ">
            <label for="NewPassword" class="col-sm-4 control-label">Password<span style="color: red;">&nbsp;*</span></label>
            <div class="col-sm-8 newPassTooltip">
                <input id="passVal" type="password" class="form-control Pass"
                       value="<?php echo $pass; ?>"
                       title="Password must contain at least 5 characters, including UPPER/lowercase and numbers" 
                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}" 
                       name="Password" required placeholder="Password..." 
                       onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
                               if (this.checkValidity())
                                   form.PasswordConfirm.pattern = this.value;"
                       />
                <span class="newPassTooltipText">Combine UPPER/lower Case and Number(s). At Least 5 Chars Long</span>
            </div>
        </div>
        <div class="form-group ">
            <label for="PasswordConfirm" class="col-sm-4 control-label">Confirm Password<span style="color: red;">&nbsp;*</span></label>
            <div class="col-sm-8 newPassTooltip">
                <input id="RepassVal" type="password" name="PasswordConfirm" class="form-control CPass"
                       value="<?php echo $passCom; ?>"
                       title="Combine UPPER/lower Case and Number(s). At Least 5 Chars Long"
                       required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}"
                       onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');"
                       placeholder="Confirm password...">
                <span class="newPassTooltipText">Combine UPPER/lower Case and Number(s). At Least 5 Chars Long</span>
            </div>
        </div>
        <div class="form-group">
            <label for="Country" class="col-sm-4 control-label">Select Country<span style="color: red;">&nbsp;*</span></label>
            <div class="col-sm-8 newPassTooltip">
                <!-- Country list -->
                <select id="filter-projects" title="Select Your Country of Residence" required class="countryList CountryList form-control" name="Country">
                    <option value='' selected id="baseCountry"></option>
                    <?php
//                    $url = "http://192.168.0.183:200/ToolService.svc?wsdl";
//                    $client = new SoapClient($url);
                    //$res = $client->__getFunctions();

                    $quote2 = $client->GetAllCountries();
                    $response2 = $quote2->GetAllCountriesResult;

                    for ($i = 0; $i < count($response2->string); $i++) {
                        $country = $response2->string[$i];
                        //echo $country."<br>";
                        echo '<option  class="contryList" value="' . htmlspecialchars($country) . '" >' . htmlspecialchars($country) . '</option>';
                    }
                    ?>
                </select>
                <span class="newPassTooltipText">Select Your Country From List</span>
            </div>
        </div>
        <div class="form-group">
            <label for="City" class="col-sm-4 control-label">City/Region<sup style="color: #62b646;">(optional)</sup></label>
            <div class="col-sm-8 newPassTooltip">
                <input type="text" class="form-control City" title="Enter Your City Name" id="city" name="City"  placeholder="City/Region">
                <span class="newPassTooltipText">Enter Your City Name</span>
            </div>
        </div>
        <div class="form-group">
            <label for="Address" class="col-sm-4 control-label">Address <sup style="color: #62b646;">(optional)</sup></label>
            <div class="col-sm-8 newPassTooltip">
                <input title="Enter Your Address of Residence" type="text" class="form-control Address" id="address" name="Address"  placeholder="Residence...">
                <span class="newPassTooltipText">Enter Address</span>
            </div>
        </div>
<!--        <div class="form-group">
            <label for="ZIP" class="col-sm-4 control-label">ZIP/Postal Code</label>
            <div class="col-sm-8 newPassTooltip">
                <input title="Enter Postal Code of Your Residence" type="text" class="form-control ZIP" id="zip" name="ZIP"  placeholder="ZIP/Postal Code">
                <span class="newPassTooltipText">Enter Postal Code of Your Residence</span>
            </div>
        </div>-->
        <div class="form-group">
            <label for="companyName" class="col-sm-4 control-label">Company Name<sup style="color: #62b646;">(optional)</sup>
<!--                <span style="color: red;">&nbsp;*</span>-->
            </label>
            <div class="col-sm-8 newPassTooltip">
                <input title="Enter the Company Name" type="text" class="form-control company-name" id="companyName" name="companyName" placeholder="Company Name">
                <span class="newPassTooltipText">Enter the Company Name</span>
            </div>
        </div>
        <div class="form-group">
            <label for="website" class="col-sm-4 control-label">Website URL<sup style="color: #62b646;"> (optional)</sup></label>
            <div class="col-sm-8 newPassTooltip">
                <input  type="text" class="form-control website" 
                        id="website" name="website"  placeholder="(http/http://)www.site.com" pattern="((http|https):\/\/)?(www)+\.+?(\S{5,})+" title="Requested format: (http/https://)www.site.com">
                <span class="newPassTooltipText">Enter the website address www.website.com</span>
            </div>
        </div>
    </div>
    <div class="form-right col-md-6 text-right">
        <div class="form-group">
            <label for="partnership" class="col-sm-4 control-label">
                Partnership Type
                <span style="color: red;">&nbsp;*</span>
            </label>
            <div class="col-sm-8 newPassTooltip">
                <select id="filter-projects" class="selectpicker form-control partnershipType" name="partnership" required>
                    <option ></option>
                    <option value="Junior Partner">CPA</option>
                    <option value="Senior Partner">Revenue Partnership</option>
                    <option value="Hybrid Partner">Hybrid Partner</option>
<!--                    <option value="Expert Partner">Expert Partner</option>
                    <option value="VIP Partner">VIP Partner</option>-->
                </select>
                <!--                        <select name="#" id="filter-taxonomy" class="selectpicker"></select>-->
                <span class="newPassTooltipText">Choose Partnership Type</span>
            </div>
        </div>
<!--        <div class="form-group ">
            <label for="noClients" class="col-sm-4 control-label">Number of Clients
                <span style="color: red;">&nbsp;*</span>
            </label>
            <div class="col-sm-8 newPassTooltip">
                <input class="form-control" type="number" id="noClients" name="noClients" min="1" step="1">
                <span class="newPassTooltipText">Enter Expected Number of Clients</span>
            </div>
        </div>-->
        <!--        <div class="form-group">
                    <label for="customCommision" class="col-sm-2 control-label">Custom Commision</label>
                    <div class="col-sm-10 newPassTooltip">
                        <input class="form-control" id="customCommision" type="number" name="customCommision" min="0" max="300" step="1" required>
                        <span class="newPassTooltipText">Enter Custom Commision</span>
                    </div>
                </div>-->
        <!--        <div class="form-group">
                    <label for="averageDeposit" class="col-sm-2 control-label">Average deposit amount</label>
                    <div class="col-sm-10 newPassTooltip">
                        <input class="form-control" type="number" id="averageDeposit" name="averageDeposit" min="0" max="300" step="1" required>
                        <span class="newPassTooltipText">Enter Average Deposit Amount</span>
                    </div>
                </div>-->
<!--        <div class="form-group textarea-form">
            <label for="comment" class="col-sm-4 control-label">Comment</label>
            <div class="col-sm-8 newPassTooltip">
                <textarea class="form-control" rows="4" name="comment" id="comment" ></textarea>
                <span class="newPassTooltipText">Enter Any Comment You Have</span>
            </div>
        </div>-->
        <div class="form-group termsAndConditions" style="width: auto;display: inline-block; margin-right:100px;">
            
            <label class="col-sm-4 control-label" style="width: auto;display: inline-block;">I Accept <a style="color: #F9A754;" href="../public/STPaffiliatesTermsAndConditions.pdf">Terms and Conditions</a></label>
            <div class="col-sm-8 newPassTooltip" style="width: auto;display: inline-block;">
                <input  type="checkbox" class="form-control " name="termsAndConditions" required style="width:20px;height:20px; float: right ;display: inline-block;margin-top: 5px;">
                <span class="newPassTooltipText" style="margin-left: -50px;width: auto;">Terms And Conditions</span>
            </div>
        </div>
    </div>
    <div class="col-md-6 form-group1 register-button text-center">
        <div class="capture-text">
            <h3 id="DemoSettings">Capture Code Is Required</h3>
        </div>
    </div>
    <!-- Capture -->
    <div class="col-md-6 form-group1 register-button" id="mainCaptcha">
        <div id="captchaStatusFalse" class="robotStatus"><p class="text-center">Enter Right Code To Enable Registration&#33;</p></div>
        <div hidden id="captchaStatusTrue"  class="robotStatus"><p class="text-center">You Can Register Now&#33;</p></div>
    </div>
    
    <div class="col-md-6 captcha text-center">
        <h3 id="DemoSettings">Capture Code</h3>
        <a id="getNewCode" style="cursor:pointer">&gt;&gt; Generate another code &lt;&lt;</a>
        <!-- TESTING ICAPTCHA -->
        <div class="generate-code" >
            <!-- Start Captcha -->
            <input type="hidden" name="captcha" id="forPHP">
            <p id="captcha"></p>
            <input type="text" name="kepcaInput" id="mojTekst">
            
            <!-- / Captcha -->
        </div>
    </div>
    
    <!-- Registration Button -->
    <div class="col-md-6 form-group1 register-button text-center">
        <input id="submit" name="test" type="submit" value="Register" onclick="bar();" disabled>
    </div>
    
    <!-- Start Processing Bar -->
    <div id="processingBar" hidden>
        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
        <span class="sr-only">Loading...</span>
        <p id="processingText">Processing... Please wait ...</p>
    </div>
    <!-- / Processing Bar -->
</form>
<!-- Processing bar -->
        <!--<script src="js/processingAnimation.js" type="text/javascript"></script>-->
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for( var i=0; i < 5; i++ ) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));  
        }
        
        $("#getNewCode").click(function() {
            $("#submit").prop('disabled', true);
            $("#captchaStatusTrue").hide();
            $('#captchaStatusFalse').show();
            text = "";
            for( var i=0; i < 5; i++ ) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            $('#captcha').text(text);
            $("#forPHP").val(text);
        });

        $('#captcha').text(text);
        $("#forPHP").val(text);
        
        $('#mojTekst').keyup(function() {
            var mojTekst = $('#mojTekst').val()
            var kepca = text;
            
            if(mojTekst == kepca) {
                $("#submit").prop('disabled', false);
                $('#captchaStatusFalse').hide();
                $("#captchaStatusTrue").show();
            } else {
                $("#submit").prop('disabled', true);
                $("#captchaStatusTrue").hide();
                $('#captchaStatusFalse').show();
            }
        });
    });
</script>    
     
        <script src="js/keyup.js" type="text/javascript"></script>
        <script src="js/registration-validation.js" type="text/javascript"></script>
        
 
        