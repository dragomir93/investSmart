<div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="home-menu col-md-8">
        <div class="top-menu-bg">
            <div class="top-menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="move-left nav navbar-nav">
                    <li >
                        <a href="../public/about.php" class="activeAbout">
                        <span class="top-menu-text">About</span>
                    </a>
                </li>
                <li>
                    <a href="../public/registration.php" class="activeAccount">
                        <span class="top-menu-text">Account</span>
                    </a>
                </li>
                <li>
                    <a href="../public/news.php" class="activeNews">
                        <span class="top-menu-text">News</span>
                    </a>
                </li>
                <li>
                    <a href="../public/contact-us.php" class="activeContact">
                        <span class="top-menu-text">Contact Us</span>
                    </a>
                </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>