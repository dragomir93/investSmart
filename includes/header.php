
 <!-- Start Top Header -->
        <div class="top-header container-fluid">
            <!-- Facebook Pixel Code -->
            <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '1437630786271681'); // Insert your pixel ID here. 
            fbq('track', 'PageView');
            </script>
            <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=1437630786271681&ev=PageView&noscript=1"
            /></noscript>
            <!-- DO NOT MODIFY -->
            <!-- End Facebook Pixel Code -->
            <!-- Start Logo -->
            <div class="header-logo">
                <a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>
            </div>
            <!-- / Logo -->
           
            
            
            <div class="login-buttons">
                 <?php if (stripos($_SERVER['REQUEST_URI'], 'registration.php')){ ?>
                
                <?php }else{?>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#register-modal">REGISTER</button>
                
                <?php } ?>
                
                 <?php if (stripos($_SERVER['REQUEST_URI'], 'login.php')){ ?>
                
                <?php }else{?>
                     <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#login-modal">LOGIN</button>
                
                <?php } ?>
                
               
                
                <span>Register with:&nbsp;</span>
                
                <button type="button" class="g-signin2 google-login" data-toggle="modal" data-target="#register-modal" data-onsuccess="onSignIn">GOOGLE</button>
                
                <span>or&nbsp;</span>
                
                <button id="fb-login" data-toggle="modal" data-target="#register-modal" class="facebook-login-btn"><i class="fa fa-facebook"></i></button>
                <!-- Modal -->
                <?php if (stripos($_SERVER['REQUEST_URI'], 'login.php')){ ?>
                
                <?php }else{?>
                     <div id="login-modal" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">LOGIN</h4>
                              </div>
                              <div class="modal-body">
                                <div class="admin">
                                    <div class="container login-form animated fadeInLeft">
                                        <div class="col-md-8 col-md-offset-2">
                                            <h2>Login &#58;</h2>
                                            <?php include('../includes/login-form.php'); ?>
                                        </div>
                                    </div>
                                    <div class='forgot-pass-container container  ' hidden>
                                        <?php include('../includes/forgot-password.php'); ?>
                                    </div>
                                    <div id="passResponse">
                                    </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>

                          </div>
                    </div>
                
                <?php } ?>
                
                
                <div id="register-modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Registration</h4>
                      </div>
                      <div class="modal-body">
                            <?php
                                include '../configs/config.inc.php';
                                $client = new SoapClient(URL);
                            ?>
                            <div class="container-fluid registration-form-wrapper">
                                
                                <?php 
                               if (stripos($_SERVER['REQUEST_URI'], 'registration.php')){
                                   
                               }else{
                               include('../includes/registration-form.php'); }?>
                            </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src="js/PassCheck.js"></script>
