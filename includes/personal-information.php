<div class="container-fluid " id="personalInformationContainer"> <!-- start of container -->
    <div class="row">
        <div class="col-lg-12 acount-details">
            <div class="mainDetailsHeader animated fadeInDown" ><span class="glyphicon glyphicon-info-sign mainGlyphicon"></span>PERSONAL INFORMATION</div>
            <!-- Personal Info -->
            <div class="detailsHeader animated fadeInDown"> <span class="glyphicon glyphicon-user detailsGlyphicon"></span> Basic Info</div>
            <form class="form-horizontal animated bounceInUp" role="form" method="post" action="">
                <div class="accountdetails-form1">
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1 control-label">First Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $firstName ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1 control-label">Last Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $lastName ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1 control-label">Password</label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id=""  value="<?php echo $password ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1 control-label">Phone</label>
                        <div class="col-sm-5">
                            <input type="tel" class="form-control" id=""  value="<?php echo $phone ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1 control-label">Mobile</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $mobile ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1 control-label">Email</label>
                        <div class="col-sm-5">
                            <input type="email" class="form-control" id=""  value="<?php echo $email ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Country</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $country ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">City/Region</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $city ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Address</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $address ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">ZIP/POSTAL CODE</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $ZIP ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Company</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $company ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">WebSite URL</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $websiteUrl ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Partner Type</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $partnerType ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Custom Commission</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $customCommission ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Number of Clients</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $clientNumber ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Deposit Amount</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id=""  value="<?php echo $depositAmount ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-1  control-label">Comment</label>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="5" readonly    ><?php echo $comment ?></textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
