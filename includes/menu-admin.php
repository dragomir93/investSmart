<style>
    /* css for hovering dropdown menu */
    .dropdown-menu .sub-menu {
        left: 100%;
        position: relative;
        top: 0;
        visibility: hidden;
        margin-top: -1px;
    }
    .dropdown-menu li:hover .sub-menu {
        visibility: visible;
    }
    .dropdown:hover .dropdown-menu {
        display: block;
    }
    .nav-tabs .dropdown-menu, .nav-pills .dropdown-menu, .navbar .dropdown-menu {
        margin-top: 0;
    }
    .dropdown:hover .dropdown-menu{
        border: none;
    }
    .dropdown-menu li{
        border-left: none;
    }
    .navbar-nav > li > .dropdown-menu{
        padding: 0;
    }
</style>
<div class="col-lg-9 col-lg-pull-3">
    <div class="navbar-header" style="color: red;">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only" >Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="top-menu-bg menu-home">
        <div class="top-menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="move-left nav navbar-nav glyphMenu">
                <li>
                    <a href="../public/calculate-cpa.php" class="activeReports"> 
                        <span class="fa fa-line-chart" aria-hidden="true"></span>
                        <span class="top-menu-text">Reports</span>
                    </a>
                </li>
                <li>
                    <a href="../public/accountdetails.php" class="activeProfile"> 
                        <span class="glyphicon glyphicon-user"></span>
                        <span class="top-menu-text">Profile</span>
                    </a>
                </li>
                <li>
                    <a href="../public/marketing-tools.php" class="activeTools">
                        <span class="glyphicon glyphicon-globe"></span>
                        <span class="top-menu-text">Marketing Tools</span>
                    </a>
                </li>
                <li>
                    <a href="../public/contact-us-IB.php" class="activeContact">
                        <span class="glyphicon glyphicon-envelope"></span>
                        <span class="top-menu-text">Contact Us</span>
                    </a>
                </li>
                <li>
                    <a href="../public/FAQ.php" class="activeFAQ">
                        <span class="glyphicon glyphicon-question-sign"></span>
                        <span class="top-menu-text">FAQ</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
