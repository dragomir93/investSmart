<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 footer-left">
                <p>&copy; Copyright 2016. by <a>STP Affiliate</a></p>
                <p>&copy; Design and Develop by <a>Alpha Digital Solution</a> | <a>Web Support</a></p>
                 <p><a style="color: #F9A754;" href="../public/STPaffiliatesTermsAndConditions.pdf">Terms and Conditions</a></p>
                
            </div>
            <div class="col-md-6 footer-right">
              <!-- Start Social -->
            <?php include('../includes/social-icons.php'); ?>
            <!-- / Social -->
            </div>
          
            
        </div>
    </div>
</footer>
