<?php //include("../configs/config.inc.php");                                       ?>
<html class="demo-1 no-js">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../public/img/logo.png"/>
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display' rel='stylesheet' type='text/css'/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Oswald:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../public/css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="../public/css/component.css" />
        <link rel="stylesheet" type="text/css" href="../public/css/defaults.css">
        <link href="../public/css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="../public/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link rel="stylesheet" type="text/css" href="../public/css/main.css" />
        <script src="../public/js/snap.svg-min.js"></script>
        <script type="text/javascript" src="../public/js/modernizr-custom.js"></script>
        <style>
            .error {color: #ff0000;}
        </style>
        <title>Welcome</title>
    </head>
    <body onload="icaptcha(1, '#000000', 'Wrong Code', 20, 'php')">
        <div class="top-header col-md-6">
            <!-- Start Logo -->
            <div class="header-logo">
                <a href="#">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                    <span>invest smart</span>
                </a>
            </div>
            <!-- / Logo -->
            <!-- Start Social -->
            <?php include('../public/social-icons.php'); ?>
            <!-- / Social -->
        </div>
        <!-- Start Menu Main -->
        <?php include('../public/manu-main.php'); ?>
        <!-- / Menu Main -->
        <div class="header-bg">
            <!-- Start Currency Line -->
            <div class="currency-line"id="scriptNews">
                <div id="currency-div">
                    <script type="text/javascript">
                        var w = '9999';
                        var s = '3';
                        var mbg = '174169';
                        var bs = 'yes';
                        var bc = 'no';
                        var f = 'verdana';
                        var fs = '15px';
                        var fc = 'FFFFFF';
                        var lc = 'FFFFFF';
                        var lhc = 'fe9a00';
                        var vc = 'FFFFFF';

                        var ccHost = (("https:" == document.location.protocol) ? "https://www." : "http://www.");
                        document.write(unescape("%3Cscript src='" + ccHost + "currency.me.uk/remote/CUK-LFOREXRTICKER-1.php' type='text/javascript'%3E%3C/script%3E"));
                    </script> 
                </div>
            </div>
            <!-- / Currency Line -->
            <div id="slideshow">
                <?php include('../public/slideshow-first.php'); ?>
            </div>
            <div id="news">
                <div id="titleContent">
                    <p id="Title" ></p>
                </div>
                <div id="Desc">
                </div>
                <?php include('../public/service-news.php'); ?>
            </div>
        </div>
        <div class="safe">
            <h2>SECURITY OF FUNDS</h2>
            <div class="container security">
                <!-- Start Event Slider -->
                <?php include('../public/event-slider.php'); ?>
                <!-- / Event Slider -->
            </div>
        </div>
        <!-- End Safe Carousel -->
        <!-- Start Services Two -->
        <div class="body-bg">
            <div class="slideImg">
                <?php include('../public/slideshow-second.php'); ?>
            </div>
            <div class="newsSlide">
                <div id="titleContent2">
                    <p id="Title2"></p>
                </div>
                <div id="Desc2">
                </div>
                <!-- PHP code for gathering the news form webservice -->
                <?php include('../public/services-news-second.php'); ?>
            </div>
        </div>
        <!-- Start Accordions -->
        <div class="main">
            <h2>Introducing Broker &#40;Revshare&#41;</h2>
            <?php include('../public/broker-information.php'); ?>
        </div>
        <!-- Start Home Form -->
        <div class="col-lg-6">
            <div class="formDiv">
                <?php echo include('../public/register.php'); ?>
            </div>
            <div class="captcha-home">
                <h3 id="DemoSettings">Capture Code</h3>
                <!-- TESTING ICAPTCHA -->
                <div class="generate-code">
                    <!-- Start Captcha -->
                    <?php include('../public/captcha-form.php'); ?>
                    <!-- / Captcha -->
                </div>
            </div>
        </div>
        <!-- / Home Form -->
        <!-- Start Welcome Text -->
        <div class="col-lg-6">
            <section class="col-xs-12 welcome-text">
                <h1>Guide For Instant Registration</h1>
                <p>
                    Open an account with us in 3 easy steps:
                    <br>
                    Fill out our simple forms,
                    submit documentation,
                    fund your account and you are ready to go!
                    <br>
                    ALL INFORMATION MUST BE RELEVANT!
                </p>
            </section>
            <!-- /Welcome Text -->
            <!-- Start Information Box -->
            <section id="grid" class="grid clearfix">
                <a class="col-xs-4 dark-blue-box" href="#" data-path-hover="m 180,34.57627 -180,0 L 0,0 180,0 z">
                    <figure>
                        <img src="../public/img/12.jpg" alt=""/>
                        <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,218 0,0 180,0 z"/></svg>
                        <figcaption>
                            <h2>Introducing Brokers</h2>
                            <p class="view-box">
                                View
                            </p>
                            <span class="link-view">
                                Earn commissions for each investor referred to AvaTrade. 
                                With a range of trading platforms, tools, and support, 
                                we offer a blend of power and simplicity customized to suit each trader.
                            </span>
                        </figcaption>
                    </figure>
                </a>
                <a class="col-xs-4 coffe-box" href="#" data-path-hover="m 180,115.57627 -180,0 L 0,0 180,0 z">
                    <figure>
                        <img src="../public/img/12.jpg" alt=""/>
                        <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,218 0,0 180,0 z"/></svg>
                        <figcaption>
                            <h2>Affiliates &#47; Website owners</h2>
                            <p class="view-box">
                                View
                            </p>
                            <span class="link-view">
                                Website owners, plug into the hot online forex and commodity market 
                                for high referral and traffic conversion revenue.
                            </span>
                        </figcaption>
                    </figure>
                </a>
                <a class="col-xs-4 green-box" href="#" data-path-hover="m 180,115.57627 -180,0 L 0,0 180,0 z">
                    <figure>
                        <img src="../public/img/12.jpg" alt=""/>
                        <svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="M 180,160 0,218 0,0 180,0 z"/></svg>
                        <figcaption>
                            <h2>Money Managers</h2>
                            <p class="view-box">
                                View
                            </p>
                            <span class="link-view">
                                AvaTrade offers a wide range of the best tools to help 
                                you manage client funds while earning commission.
                            </span>
                        </figcaption>
                    </figure>
                </a>
            </section>
        </div>
        <!-- / Information Box -->
        <!-- Start Footer -->
        <?php include('../public/footer.php'); ?>
        <!-- / Footer -->
        <script type="text/javascript">

            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {
                country = data.address.country;
                p.text = country;
                p.value = country;
            }
        </script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.3.0.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../public/js/bootstrap.js"></script>
        <script type="text/javascript" src="../public/js/owl.carousel.min.js"></script>
<!--        <script type="text/javascript" src="js/InfoShow.js"></script>-->
        <script type="text/javascript" src="../public/js/PassCheck.js"></script>
        <!-- CAPTCHA 2LINE CODE JS -->
<!--        [if lt IE 9]><script type='text/javascript' src='js/excanvas.js'></script><![endif]-->
        <script type='text/javascript' src='../public/js/icaptcha.js'></script>
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
        <!-- keyup -->
        <script src="../public/js/keyup.js" type="text/javascript"></script>
        <!-- main -->
        <script src="../public/js/main.js" type="text/javascript"></script>
    </body>
</html>
