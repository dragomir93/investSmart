<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display' rel='stylesheet' type='text/css'/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Oswald:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../public/css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="../public/css/component.css" />
        <link rel="stylesheet" type="text/css" href="../public/css/defaults.css">
        <link href="../public/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../public/css/registrationLandingPage.css"/>
    
        <title>Registration Error</title>
    </head>
    
  <body class="registration-message">
    <div class="errorContainer">
        <div class="note">
            ERROR Occurred!<span class="glyphicon glyphicon-remove FalseGlyph"></span>
        </div>
        <div class="noteText">
            There was an ERROR during campaign creation!<span class="glyphicon glyphicon-exclamation-sign falseTextGlyph"></span>
        </div>
        <div id="errorMessage" >
            <?php echo $_SESSION['createCampError']; ?>
        </div>
        <div class="goHome">
            <a href="../public/dashboard.php" ><p><span class="glyphicon glyphicon-menu-left goHomeGlyph"></span><span class="glyphicon glyphicon-menu-left "></span>go Home</p></a>
        </div>
    </div>
      
    </body>
</html>