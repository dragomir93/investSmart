<?php
session_start();
error_reporting(0);
//NOT TO ALLOW APPROACH IF THERE IS NO TOKEN
if ($_SESSION['IBFname'] == "" || $_SESSION['username'] == "") {
//    setcookie("IBProfilePassword", '' , time() - 3600,'/');
    header("location: login.php"); //back to login page
}
?>
<html>
    <head>
        <title>FAQ</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" href="css/select-jquery.css"/>
        <link rel="stylesheet" type="text/css" href="css/FAQ.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    </head>
    <body>
        <?php include_once("analyticstracking.php"); ?>  
        <div class="top-header">
            <div class="header-logo">
                <?php
                if (isset($_COOKIE['IBProfilePassword'])) {
                    echo '<a href="../public/accountdetails.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                } else {
                    echo '<a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                }
                ?>
            </div>
            <?php include('../includes/social-icons.php'); ?>
        </div>
        <div class="container-fluid account-details">
            <div class="row">
                <div class="col-lg-3 col-lg-push-9 text-right login-info">
                    <p>Hello&#58;</p>
                    <h3 style="color: #13869E;"><?php echo $_SESSION['username']; ?></h3>
                    <form method="post" action="logout.php">
                        <button name="LOGOUT" type="submit" class="btn btn-primary">Log Out</button>
                    </form>
                </div>
                <!-- Start Menu -->
                <?php include('../includes/menu-admin.php'); ?>
            </div>
        </div>
        <i style='display: none;' class="fa fa-angle-double-up backToTop" aria-hidden="true"></i>
        <div class="container-fluid">
            <div class="row background shadowContainer">
                <div class="FAQcontent col-md-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="animated fadeInDown" >Frequently Asked Questions</h1>
                                <h3 class="animated fadeInDown">What is STP Affiliates?</h3>
                                <p class="animated fadeInDown">
                                    STP Afilliates is the world's most significant Partnership
                                    Program in the Forex and CFD markets. As an affiliate, 
                                    you don’t need to know forex or meet clients in person. 
                                    You will be provided with a complete set of marketing 
                                    tools to promote STP  Afilliates and we have various 
                                    commission structures to help you meet your revenue goals.
                                    For more information please email: <a>something@stpaffiliate.com</a>
                                </p>
                                <h3 class="animated fadeInDown">What is Forex?</h3>
                                <p class="animated fadeInDown">
                                    <b>FOREX</b> is a short form of for “<b>foreign exchange</b>” 
                                    (<b>FOR</b>eign <b>EX</b>change). The point of the Forex financial
                                    market is the trading buying and selling of currencies.
                                </p>
                                <h3 class="animated fadeInDown">What is CFD?</h3>
                                <p class="animated fadeInDown">
                                    CFD, or Contracts for Difference, is a financial instrument
                                    similar to an index or share which allows you to trade an 
                                    underlying index, share or commodity contract without having 
                                    to own the underlying asset itself. 
                                </p>
                                <h3 class="animated fadeInDown">Do you offer White Label?</h3>
                                <p class="animated fadeInDown">
                                    STP Afilliates gives you an excellent White Label program 
                                    that provides you with turnkey solutions with minimal cost
                                    and risk. Additionally, you gain access to STP Afilliates 
                                    technology, customer service, and spreads that give you and
                                    your customers the ideal trading environment. This program
                                    requires also a fee depending on the extent of your desired 
                                    white label. 
                                </p>
                                <h3 class="animated fadeInDown">Do you offer 2nd tier (sub commission)?</h3>
                                <p class="animated fadeInDown">
                                    Yes, by signing up others to STP Afilliates you will receive
                                    a 10% sub commission. When referring sub affiliates please 
                                    use the Sub Affiliate Link found on your home page.  
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- / Information Box -->
        <?php include('../includes/footer.php'); ?>

        <!-- Include JS File Here -->
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
        <!--<script src='js/select-jquery.js'></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/IB-contact-validation.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(".activeFAQ span").css("color", "#ff9900");

            $(document).ready(function () {
                $(window).scroll(function () {
                    if ($(window).scrollTop() > 500) {
                        $('.backToTop').show(500);
                    } else {
                        $('.backToTop').hide(500);
                    }
                });
                $('.backToTop').click(function () {
                    $("html,body").animate({scrollTop: 0}, 1000);
                });
            });
            /* Back to top change collor on scroll */
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();

                if (scroll >= 2500) {
                    $(".fa-angle-double-up").addClass("orange");
                } else {
                    $(".fa-angle-double-up").removeClass("orange");
                }
            });
        </script>
    </body>
</html>
