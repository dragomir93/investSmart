<?php
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);

$mail = $_POST['mail'];

$passRequest = $client->SendForgotPassword(array('email' => $mail))->SendForgotPasswordResult;
$passMessage = $passRequest->Message;
$passSuccess = $passRequest->Success;

if ($passSuccess == TRUE) {
    echo "<div id ='successRequest' >Recovery password is sent to Your mail!</div>";
} else {
    echo "<div id ='errorRequest' >$passMessage</div>";
}
?>
<script type="text/javascript">
    $(".requestPass").attr("hidden", true);
    $('.forgot-pass')[0].reset();
</script>