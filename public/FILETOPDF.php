<?php
include '../configs/config.inc.php';
$client = new SoapClient(URL);
require('../fpdf/fpdf.php');
session_start();
$lastName = $_POST['fName'];
$firstName = $_POST['lName'];
$start = $_POST['start'];
$end = $_POST['end']; 
$countFTD = $_POST['countFTD'];
$extraCPA = $_POST['extraCPA'];
$totalCPA = $_POST['totalCPA'];
$countSA = $_POST['countSA'];
$SACPA = $_POST['SACPA'];
$CountClients = $_POST['CountClients'];  
$numberOfItems = $_POST['numberOfItems'];


$pdf = new FPDF();

$pdf->AddPage();

//$pdf->SetFillColor(255, 0, 0);
//$pdf->SetTextColor(255);
//$pdf->SetDrawColor(128, 0, 0);
//$pdf->SetLineWidth(.3);

$pdf->SetFont("Arial", "B", "25");
$pdf->SetXY(5, 5);
$pdf->SetTextColor(25, 54, 72);
$pdf->Cell(0, 10, "Profit Report for: $firstName $lastName", 0, 0, "C");

//$pdf->SetFont("Arial", "B", "25");
//$pdf->SetXY(5, 5);
//$pdf->SetTextColor(25, 54, 72);
//$pdf->Cell(0, 10, "Profit Report for: $firstName $lastName", 0, 0, "C");

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(25, 25);
$pdf->SetTextColor(15, 14, 72);

$pdf->Cell(0, 10, "For Period:", 0, 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(10, 35);
$pdf->Cell(0, 10, "Start Date:", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(40, 35);
$pdf->Cell(0, 10, $start, 0, 0);
//$pdf->Cell(50, 5,  $start, 1, 0, 'L', 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(10, 45);
$pdf->Cell(0, 10, "End Date:", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(40, 45);
$pdf->Cell(0, 10, $end, 0, 0);
//$pdf->Cell(50, 5,  $end, 1, 0, 'L', 0);



$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(25, 65);
$pdf->Cell(0, 10, "CountFTD", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(20, 75);
$pdf->Cell(40, 10, $countFTD, 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(95, 65);
$pdf->Cell(0, 10, "ExtraCPA", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(90, 75);
$pdf->Cell(40, 10, $extraCPA, 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(155, 65);
$pdf->Cell(0, 10, "TotalCPA", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(150, 75);
$pdf->Cell(40, 10, $totalCPA, 1, 0, 'C', 0);


$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(25, 90);
$pdf->Cell(0, 10, "CountSA", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(20, 100);
$pdf->Cell(40, 10,$countSA, 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(95, 90);
$pdf->Cell(0, 10, "SACPA", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(90, 100);
$pdf->Cell(40, 10,$SACPA, 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(145, 90);
$pdf->Cell(0, 10, "Number of clients", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(150, 100);
$pdf->Cell(40, 10,$CountClients, 1, 0, 'C', 0);   

//FOR ITEMS DATA SHOW

$offset = 15;
$height = 115;
$pdf->SetFont("Arial", "B", "12");

$pdf->SetXY(20, 115);
//$pdf->Cell(0, 10, "Date", 0, 0);
$pdf->Cell(40, 15, "Date", 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "12");
$pdf->SetXY(60, 115);
//$pdf->Cell(0, 10, "Amount", 0, 0);
$pdf->Cell(40, 15, "Amount", 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "12");
$pdf->SetXY(100, 115);
//$pdf->Cell(0, 10, "Country", 0, 0);
$pdf->Cell(40, 15, "Country", 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "12");
$pdf->SetXY(140, 115);
//$pdf->Cell(0, 10, "Description", 0, 0);
$pdf->Cell(40, 15, "Description", 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "12");
$pdf->SetXY(180, 115);
//$pdf->Cell(0, 10, "FTD", 0, 0);
$pdf->Cell(20, 15, "FTD", 1, 0, 'C', 0);



if ($numberOfItems > 1) {
    for ($i = 0; $i < $numberOfItems; $i++) {
        $height += $offset;
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(20, $height);
//        $pdf->Cell(0, 10, $time . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['time'] = $_POST["time$i"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(60, $height);
//        $pdf->Cell(0, 10, $amount . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['amount'] =  $_POST["amount$i"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(100, $height);
//        $pdf->Cell(0, 10, $description . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['description'] =  $_POST["description$i"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(140, $height);
//        $pdf->Cell(0, 10, $country . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['country'] = $_POST["country$i"] . '', 1, 0, 'C', 0);
        
        
       $FTD = $_POST["FTD$i"];
       $FTDSes = $_SESSION["FTD"];
        if($FTD == TRUE){
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(180, $height);
        $pdf->Cell(20, 15, $_SESSION["FTD"] = $_POST["FTD$i"]='YES' . '', 1, 0, 'C', 0);
        }else{
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(180, $height);
        $pdf->Cell(20, 15,$_SESSION["FTD"] = $_POST["FTD$i"]='NO' . '', 1, 0, 'C', 0);
        }
        
    }
} elseif ($numberOfItems == 1) {
        $height += $offset;
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(20, $height);
//        $pdf->Cell(0, 10, $time . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['time'] = $_POST["time"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(60, $height);
//        $pdf->Cell(0, 10, $amount . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['amount'] =  $_POST["amount"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(100, $height);
//        $pdf->Cell(0, 10, $description . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['description'] =  $_POST["description"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(140, $height);
//        $pdf->Cell(0, 10, $country . '' . $i, 0, 0);
        $pdf->Cell(40, 15, $_SESSION['country'] = $_POST["country"] . '', 1, 0, 'C', 0);
        
        $FTD = $_POST["FTD"];
       $FTDSes = $_SESSION["FTD"];
        if($FTD == TRUE){
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(180, $height);
        $pdf->Cell(20, 15, $_SESSION["FTD"] = $_POST["FTD"]='YES' . '', 1, 0, 'C', 0);
        }else{
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(180, $height);
        $pdf->Cell(20, 15,$_SESSION["FTD"] = $_POST["FTD"]='NO' . '', 1, 0, 'C', 0);
        }
        
        
}
            
if($numberOfItems == 0){
    $pdf->SetFont("Arial", "", "20");
    $pdf->SetMargins(1, 1);
    $pdf->SetXY(0, 115);
    $pdf->SetTextColor(255, 0, 0);
    $pdf->Cell(0, 0, "You have no items to display!", 0, 0, "C");
}

$pdf->Output();

