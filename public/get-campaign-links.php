<?php 


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$campName = $request->campaignName;
$campLang = $request->language;


$bannerPath = "Campaigns/$campName/$campLang/Banners/";
$banners = scandir($bannerPath);
$bannersArray = array();
for ($i = 0; $i < count($banners); $i++) {
    if($i == 0 || $i == 1){
        continue;
    }
    array_push($bannersArray, $banners[$i]);
}

$lpPath = "Campaigns/$campName/$campLang/LP/";
$lpDir = scandir($lpPath);

for ($i = 0; $i < count($lpDir); $i++) {
    if($i == 0 || $i == 1){
        continue;
    }
    $lp = $lpDir[$i];
}

$mailerPath = "Campaigns/$campName/$campLang/Mailer/";

if(file_exists($mailerPath."index.html")){

$mailers = scandir($mailerPath);

for ($i = 0; $i < count($mailers); $i++) {
    if($i == 0 || $i == 1){
        continue;
    }
    $mailer = $mailers[$i];
}
$data = array("banners"=>$bannersArray,"mailer"=>$mailer,"lp"=>$lp);

}else{
    $data = array("banners"=>$bannersArray,"lp"=>$lp);
}

echo json_encode($data);