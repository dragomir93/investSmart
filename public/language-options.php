<?php

include '../configs/config.inc.php';
$client = new SoapClient(URL);

$languages = $client->GetLanguages()->GetLanguagesResult;

if(!is_array($languages->Language)) {
    $languages->Language = array($languages->Language);
}

echo json_encode($languages->Language);