<?php

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$campaign = $request->campaign;
$language = $request->language;
$bannerToDelete = $request->bannerToDelete;


$bannerLink ="../public/Campaigns/$campaign/$language/Banners/$bannerToDelete".".jpg";

$bannerPath = "../public/Campaigns/$campaign/$language/Banners/";
$banners = scandir($bannerPath);

    if(count($banners) > 3){
        $delete = unlink($bannerLink);
        if(!$delete) {
            $bannerLink ="../public/Campaigns/$campaign/$language/Banners/$bannerToDelete".".jpeg";
            $delete = unlink($bannerLink);
        }

        if (!$delete) {
            $bannerLink ="../public/Campaigns/$campaign/$language/Banners/$bannerToDelete".".gif";
            $delete = unlink($bannerLink);
        }

        if (!$delete) {
            $bannerLink ="../public/Campaigns/$campaign/$language/Banners/$bannerToDelete".".png";
            $delete = unlink($bannerLink);
        }
        
    }else{
        $delete = FALSE;
    }

echo json_encode($delete);
