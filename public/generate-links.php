<?php

session_start();
include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));
$client2 = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));

$token = $_SESSION['TOKEN'];
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$id = $_SESSION['UserID'];

$language = $request->language;
$campaigns = $request->campaignName;
$bannerSize = $request->bannerSize;
$campID = $request->campID;

$linkBanner = 'Campaigns/'.$campaigns.'/'.$language.'/Banners/'.$bannerSize; 

$linkB = "<a href='http://www.stpaffiliates.com/public/Campaigns/$campaigns/$language/LP/index.html?PartnerID=$id&ClickID=$campID'><img src='www.stpaffiliates.com/public/$linkBanner' alt='$bannerSize'/></a>";

$linkBsub = "<a href='http://www.stpaffiliates.com/public/registration.php?refid=$id&ClickID=$campID'><img src='www.stpaffiliates.com/public/$linkBanner' alt='$bannerSize'/></a>";

$lp = "http://www.stpaffiliates.com/public/Campaigns/$campaigns/$language/LP/index.html?PartnerID=$id&ClickID=$campID";

$generateCampaignBanner = $client->InsertPartnerLink(array('token'=>$token,'language'=>$language,'campaign'=>$campaigns,'link'=>$linkB,'type'=>1))->InsertPartnerLinkResult;
$generateCampaignBanner = $client2->InsertPartnerLink(array('token'=>$token,'language'=>$language,'campaign'=>$campaigns,'link'=>$lp,'type'=>3))->InsertPartnerLinkResult;


// LANDING PAGE DOWNLOAD
$strToRepleace = array("USERID","CLICKID");
$parameters = array($id, $campID);
$whereToSave = "TEMP/LandingPage$id.html";
$path = "Campaigns/$campaigns/$language/LP/index.html";

$myFile = file_get_contents($path);

$newFile = str_replace($strToRepleace, $parameters, $myFile);

$tempFile = fopen($whereToSave, "w") or die("Somethnig went wrong! Try again later");
$newLP = fwrite($tempFile, $newFile);
fclose($tempFile);

$tempLocation = $whereToSave;

//MAILER DOWNLOAD
$pathMailer = "Campaigns/$campaigns/$language/Mailer/index.html";
$pathLP = "http://www.stpaffiliates.com/public/Campaigns/$campaigns/$language/LP/index.html";

if(file_exists($pathMailer)){
    $name = "PartnerID";
    $strToRepleaceMailer = array("USERID", "NAME", "PATHLP","CLICKID");
    $parametersMailer = array($id,$name, $pathLP,$campID);
    $whereToSaveMailer = "TEMP/EmailPage$id.html";
    
    $myFileMailer = file_get_contents($pathMailer);

    $newFileMailer = str_replace($strToRepleaceMailer, $parametersMailer, $myFileMailer);

    $tempFileMailer = fopen($whereToSaveMailer, "w") or die("Somethnig went wrong! Try again later");
    $newMailerMailer = fwrite($tempFileMailer, $newFileMailer);
    fclose($tempFileMailer);

    $tempLocationMailer = $whereToSaveMailer;


    //MAILER SUBAFFILIATE DOWNLOAD
    $pathMailerSub = "http://www.stpaffiliates.com/public/registration.php";
    $nameSub = "refid";
    $strToRepleaceMailerSub = array("USERID", "NAME", "PATHLP","CLICKID");
    $parametersMailerSub = array($id,$nameSub, $pathMailerSub,$campID);
    $whereToSaveMailerSub = "TEMP/SubEmailPage$id.html";

    $myFileMailerSub = file_get_contents($pathMailer);

    $newFileMailerSub = str_replace($strToRepleaceMailerSub, $parametersMailerSub, $myFileMailerSub);

    $tempFileMailerSub = fopen($whereToSaveMailerSub, "w") or die("Somethnig went wrong! Try again later");
    $newMailerMailerSub = fwrite($tempFileMailerSub, $newFileMailerSub);
    fclose($tempFileMailerSub);

    $tempLocationMailerSub = $whereToSaveMailerSub;
    
    $mailerExists = TRUE;
}else{
    $tempLocationMailer = FALSE;
    $tempLocationMailerSub = FALSE;
    $mailerExists = FALSE;
}

     
//put data into array to send into json object
$data = array("bannerLink"=>$linkB,"bannerSub"=>$linkBsub,"lp"=>$lp,"lpDownload"=>$tempLocation,"mailerDownload"=>$tempLocationMailer,"mailerSub"=>$tempLocationMailerSub,"mailer"=>$mailerExists);

echo json_encode($data);

