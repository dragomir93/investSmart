<?php

/* Folder List
 * 
  function listFolderFiles($dir) {
  $dirContent = scandir($dir);
  $campaigns = array();
  $banners = array();
  foreach ($dirContent as $item => $campaign) {
  if ($campaign != '.' && $campaign != '..') {
  $banners[] = $campaign;
  return $campaign;
  //            echo '<br>item :<br>';
  //            echo $item;
  //            echo '<br>campaign :<br>';
  //            echo $campaign;
  }
  }
  //    echo '<br>banners :<br>';
  //    echo '<pre>';
  //    print_r($banners);
  //    echo '<pre>';
  }
 * 
 */
/* List directory content */
function listFolderFiles($dir) {
    $dirContent = scandir($dir);
    $campaigns = array();
    $banners = array();
    foreach ($dirContent as $campaign) {
        if ($campaign != '.' && $campaign != '..' && $campaign != "img" && $campaign != "fonts") {
            $banners[] = $campaign;
        }
    }
    return $banners;
}
/* display only value */
function array_values_recursive($ary) {
    $lst = array();
    foreach (array_keys($ary) as $k) {
        $v = $ary[$k];
        if (is_scalar($v)) {
            $lst[] = $v;
        } elseif (is_array($v)) {
            $lst = array_merge($lst, array_values_recursive($v)
            );
        }
    }
    return $lst;
}
/* Text Cut */
function text_cut($text, $length = 200) {
    $text = trim(preg_replace('#[\s\n\r\t]{2,}#', ' ', $text));
    $text_temp = $text;
    while (substr($text, $length, 1) != " ") {
        $length++;
        if ($length > strlen($text)) {
            break;
        }
    }
    $text = substr($text, 0, $length);
    return $text . ( ( $text != '' && strlen($text_temp) > $length ) ? '...' . $more : '');
}
