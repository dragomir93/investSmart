<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">EDIT CAMPAIGN</h3>
            </div>
            <div class="modal-body text-center">
                <form method="post" action="../public/add-new-language.php" class="container" id="editCampaignForm">
                    <div class="row">
                        <div class="col-md-9">
                            <select name="campaignId" id="listCamp">
                                <option disabled selected value='0'>Choose campaign to edit</option>
                            </select>
                        </div>
                        <div class="col-md-3 delete-list">
                            <a href="#" class="btn btn-danger" id="deleteCampaign">DELETE CAMPAIGN</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 delete-list">
                            <p>Delete:</p>
                            <a href="#" class="btn btn-danger" id="deleteLanguage">DELETE LANGUAGE</a>
                        </div>
                        <div class="col-md-2 edit-languages">
                            <p>Languages:</p>
                            <select id="campaignLanguages">
                                <option disabled selected value='0'>Choose language for selected campaign</option>
                            </select>
                        </div>
                        <div class="col-md-2 banners-list">
                            <p>Banners:</p>
                        </div>
                        <div class="col-md-2 mailers-list">
                            <p>Mailers:</p>
                        </div>
                        <div class="col-md-2 lp-list">
                            <p>Langing pages:</p>
                        </div>
                        <div class="col-md-2 edit-list">
                            <p>Edit:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary" id="addNewLanguage" value="Add new language">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>