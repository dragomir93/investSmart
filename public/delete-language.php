<?php
include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$campName = $request->campName;
$campID = $request->campID;
$langName = $request->langName;
$langID = $request->langID;




$language = "../public/Campaigns/$campName/$langName";
$languages = scandir($language);
$campLang = "../public/Campaigns/$campName";
$campLang = scandir($campLang);

if(count($campLang)>3){
    $deleteCampaignLanguage = $client->DeleteCampaignLanguage(array("CampaignId"=>$campID,"LanguageId"=>$langID))->DeleteCampaignLanguageResult;


    function rmrf($dir) {
        foreach (glob($dir) as $file) {
            if (is_dir($file)) { 
                rmrf("$file/*");
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }
    rmrf($language);
    echo json_encode($deleteCampaignLanguage);
} else {
    $oneLanguage = array("Success"=>false,"Message"=>"Can not delete because only one language remain");
    echo json_encode($oneLanguage);
}

