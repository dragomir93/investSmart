<html class="demo-1 no-js">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <link rel="stylesheet" href="intl-tel-input-9.2.0/build/css/intlTelInput.css"> <!-- FOR COUNTRY CALL CODE -->
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/navigationstick-center.js"></script>
        <script src="js/snap.svg-min.js"></script>
        
        <!-- Google login -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="486026532062-81b6dihhele0bj8lt7pnngbiblnh82vq.apps.googleusercontent.com">
        <script src="js/google.js"></script>
        
        <!-- Facebook login -->
        <script src="js/facebook.js"></script>
        
        

        <title>STP Affiliates</title>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>
        <!-- Header -->
        <?php include ('../includes/header.php'); ?>
        <!-- / Header -->
        <!-- Start Menu Main -->
        <?php include ('../includes/menu-main.php'); ?>
        <!-- / Menu Main -->
        <!-- Scrole to top -->
        <i style='display: none;' class="fa fa-angle-double-up backToTop" aria-hidden="true"></i>
        <!-- / Scrole to top -->
        <!-- Start Services First-->
        <!-- Start Currency Line -->
            <div class="currency-line" id="scriptNews">
                <div id="currency-div">
                    <script type="text/javascript">
                        var w = '9999';
                        var s = '3';
                        var mbg = 'transparent';
                        var bs = 'yes';
                        var bc = 'no';
                        var f = 'verdana';
                        var fs = '12px';
                        var fc = 'FFFFFF';
                        var lc = 'FFFFFF';
                        var lhc = 'fe9a00';
                        var vc = 'FFFFFF';

                        var ccHost = (("https:" == document.location.protocol) ? "https://www." : "http://www.");
                        document.write(unescape("%3Cscript src='" + ccHost + "currency.me.uk/remote/CUK-LFOREXRTICKER-1.php' type='text/javascript'%3E%3C/script%3E"));
                    </script>
                </div>
            </div>
            <!-- / Currency Line -->
        <div class="header-bg">
            <!-- Start Carousel Caption -->
            <div class="animated fadeInDown">
                <h2>Enjoy Up to $1000 CPA</h2>
                <h3>For Our Partners and Affiliates</h3>
                <p>Join StockSTP Affiliate program and enjoy the highest commissions and conversion rates industry-wide</p>
                <div class="header-buttons">
                    <a href="../public/registration.php">Register</a>
                    <a href="../public/login.php">Login</a>
                </div>
                <img class="rotate" src="../public/img/rotate.png" alt="rotate" width="138" height="25">
            </div>
            <!-- / Carousel Caption -->
        </div>
        <div class="newsLink">
            <a href="../public/news.php" class="newsAnimation flash">
                <h1>Why STP Affiliates <i>&#63;</i></h1>
            </a>
        </div>
        <!-- Start Why STP Affiliates -->
        <div class="home-bottom-box container">
            <div class="row">
                <div class="col-sm-4 text-center why-stp">
                    <div class="box-img-wrapp">
                        <a href="../public/commission-inner.php">
                            <img src="../public/img/icon1.png" alt="commissions">
                        </a>
                    </div>
                    <h4 id="testimonial-icon" class="testimonial"><a href="../public/commission-inner.php">up to &#36;1000 CPA high revenue share &#37; special hybrid plans</a></h4>
                    <a href="../public/commission-inner.php"><span>See commisions HERE</span></a>
                </div>
                <div class="col-sm-4 text-center why-stp">
                    <div class="box-img-wrapp">
                        <a href="../public/contact-us.php">
                            <img src="../public/img/icon2.png" alt="contact">
                        </a>
                    </div>
                    <h4 id="testimonial-icon" class="testimonial"><a href="../public/contact-us.php">personal support by dedicated affiliate manager</a></h4>
                    <a href="../public/contact-us.php"><span style="letter-spacing: 2px;">Contact us now</span></a>
                </div>
                <div class="col-sm-4 text-center why-stp">
                    <div class="box-img-wrapp">
                        <a href="https://www.stockstp.com/">
                            <img src="../public/img/icon3.png" alt="learn">
                        </a>
                    </div>
                    <h4 class="testimonial"><a href="https://www.stockstp.com/">promote a leading regulated broker with cutting edge technology</a></h4>
                    <a href="https://www.stockstp.com/"><span>Learn about StockSTP</span></a>
                </div>
            </div>
        </div>
        <!-- / Why STP Affiliates -->
        <!-- Start Slider -->
        <div id="carousel-example-generic" class="carousel slide testimonials-slider" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="../public/img/slide1.jpg" alt="slide1">
                    <div class="container">
                        <div class="carousel-caption">
                            "I joined STP Affiliates last year just to try it out and was surprised how much effort their team has put into analyzing my 
                            traffic and optimizing campaigns so I can increase my profits."
                            <br>
                            <br><span>Lorenza Bianchi – Italy</span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="../public/img/slide2.jpg" alt="slide2">
                    <div class="carousel-caption">
                        "STP affiliates has the best online reporting system that lets me see which campaigns are preforming best and keep track of my conversions and commissions"
                        <br>
                        <br><span>Otto Anderson - Sweden</span>
                    </div>
                </div>
                <div class="item">
                    <img src="../public/img/slide3.jpg" alt="slide2">
                    <div class="carousel-caption">
                        "Working as a full-time affiliate and managing over 20 Arabic websites isn't easy but thanks to STP affiliates team I get custom creatives in 
                        Arabic and my clients are are sold in their native language."
                        <br>
                        <br><span>Hassan Mansour - Egypt</span>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
       
        <!-- / News Slider -->
        <!-- Start Collaboration -->
        <div class="container-fluid collaboration">
            
            <div class="row">
                <div class="col-sm-12 footer-add">
                    <h2 class="">NUMBERS SPEAK FOR THEMSELVES</h2>
                    <div class="collaboration-paragraf">
                        <p style="font-family: 'Montserrat', sans-serif; color: #F9A754; font-weight: 700; letter-spacing: 1px;">$16,744,059 <span style=" font-family: 'Montserrat', sans-serif; color: #5b5957; opacity: .8;"> Total Commissions Paid To Affiliates 2016.</span></p>
                        <p style="font-family: 'Montserrat', sans-serif; color: #F9A754; font-weight: 700; letter-spacing: 1px;">10,000 + <span style=" font-family: 'Montserrat', sans-serif; color: #5b5957; opacity: .8;"> Happy Affiliates</span></p>
                        <p style="font-family: 'Montserrat', sans-serif; color: #F9A754; font-weight: 700; letter-spacing: 1px;">1.7 Million <span style=" font-family: 'Montserrat', sans-serif; color: #5b5957; opacity: .8;"> Happy Traders</span></p>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 info">
                    <article class="revealOnScroll" data-animation="fadeInLeft">
                        <h2>ENJOY LIMITLESS HELP AND SUPPORT BY PROFESSIONALS</h2>
                        <p>Our dynamic team of professionals have years of combined experience in affiliate marketing, SEO, SEM, Media Buying, and Social Media. Their sole duty is to help, guide and assist every affiliate at STP affiliates to increase his/her profits.</p>
                    </article>
                    <article class="revealOnScroll" data-animation="fadeInRight">
                        <h2>INCREASE YOUR CLIENT CONVERSION RATIO</h2>
                        <p>Every single lead referred to StockSTP.com is contacted by our professional sales team within minutes of registration and often in the lead's native language, insuring the best possible conversion from your leads.</p>
                    </article>
                    <article class="revealOnScroll" data-animation="fadeInRight">
                        <h2>PROMOTE A TRUSTED EUROPEAN LICENSED AND REGULATED BROKER</h2>
                        <p>Our partner broker Stock STP is operated by Leverate Financial Services Ltd. – a Cyprus Investment Firm (CIF) licensed and regulated by the Cyprus Securities and Exchange Commission (CySEC) under license number 160/11.</p>
                    </article>               
                </div>
                
                <div class="col-md-4 col-md-push-4 info info-second">
                    <article class="revealOnScroll" data-animation="fadeInRight">
                        <h2>GAIN UNIQUE INSIGHTS VIA REAL TIME REPORTING</h2>
                        <p>STP Affiliates offers accurate real time reporting detailing clicks, registrations and conversion, which allows affiliates to track their traffic and identify any issues that could be effecting their success.  </p>
                    </article>
                    <article class="revealOnScroll" data-animation="fadeInLeft">
                        <h2>UTILIZE A LARGE COLLECTIONS OF MARKETING TOOLS</h2>
                        <p>In addition to hundreds of new banners, mailers, landing pages and other marketing tools, STP Affiliates offers custom creatives, made especially to fit your needs.</p>
                    </article>
                    <article class="revealOnScroll" data-animation="fadeInLeft">
                        <h2>EARN THE HIGHEST COMMISSIONS IN THE INDUSTRY</h2><p>Commission plans at STP Affiliates are among the highest rates in the industry. Our challenge stands if you find a competitor offering higher commission rates, let us know and we will outmatch them.</p>
                    </article>
                </div>
                
                <div class="col-md-4 col-md-pull-4 affiliate-img person revealOnScroll" data-animation="fadeIn">
                    <img class="affiliate-person " src="../public/img/footer-img.jpg" alt="affiliate-world">
                </div>
            </div>
        </div>
        <!-- / Collaboration -->
        <footer class="footer">
            <p class="revealOnScroll" data-animation="fadeInLeft">SEE THE BRILLIANT OPPORTUNITY IN EACH NEW DAY</p> 
            <p class="revealOnScroll" data-animation="fadeInRight">with our partner's program and realize your potential with most dynamic and creative team</p>
            <ul>
                <li class="revealOnScroll" data-animation="fadeInLeft"><a href="../public/registration.php">Register</a></li>
                <li class="revealOnScroll" data-animation="fadeInRight"><a href="../public/login.php">Sign Up</a></li>
            </ul>
        </footer>
        <?php include('../includes/footer.php'); ?>
        <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <!-- main -->
        <script src="intl-tel-input-9.2.0/build/js/intlTelInput.js"></script><!-- COUNTRY CODE -->
        <script src="intl-tel-input-9.2.0/build/js/utils.js" ></script><!-- COUNTRY CODE initialization-->
        <script src="intl-tel-input-9.2.0/examples/js/defaultCountryIp.js"></script><!-- COUNTRY CODE ip -->
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
        <script src="js/main.js" type="text/javascript"></script>
        <script>
            $(".activeHome span").css("color", "#ff9900");
        </script>
    </body>
</html>
