<html>
    <?php
    error_reporting(0);
    //WEBSERVICE CONNECTING
        include '../configs/config.inc.php';
    ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display' rel='stylesheet' type='text/css'/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Oswald:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/registrationLandingPage.css"/>
        <script src="js/snap.svg-min.js"></script>
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
        <title>Registration Result</title>
    </head>
    <body class="registration-message">
    
       
        <!-- Message depending on web service response -->
        
    
       
   
     
        
        <?php
        
        
            if ($_POST['kepcaInput'] == $_POST['captcha']) {
               
                $client = new SoapClient(URL);
                $obj = new stdClass();

                $obj->FirstName = $_POST["FirstName"];
                $obj->LastName = $_POST["LastName"];
                $obj->Email = $_POST["Email"];
                $obj->Password = $_POST["Password"];
                $obj->Mobile = $_POST["mobilePhone"];
                $obj->Phone = $_POST['phone-stable'];
                $obj->Country = $_POST["Country"];
                $obj->CityRegion = $_POST["City"];
                $obj->Address = $_POST["Address"];
                $obj->Zip = $_POST["ZIP"];
                $obj->Company = $_POST["companyName"];
                $obj->WebsiteUrl = $_POST["website"];
                $obj->PartnerType = $_POST["partnership"];
                //$obj->CustomCommission = $_POST["customCommision"];
                $obj->NumberOfClients = $_POST["noClients"];
                //$obj->DepositAmount = $_POST["averageDeposit"];
                $obj->Comment = $_POST["comment"];

               $parameter = array("input" => $obj);
               $rezultat = $client->RegisterPartner($parameter)->RegisterPartnerResult;
               
                $error = $rezultat->Message;
                $success = $rezultat->Success;
                
                if($success == true){
                 
                    header("Location:message.html");
                
                }else{
                    
                 header("Location:error.php?errorMssg=".urlencode($error));
                }
                    
                
           
            
          
                
          
                
            }else if ($_POST['kepcaInput'] != $_POST['captcha']){ 
        
                header("Location:registration.php");
                
            
       } ?>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>


