<?php

session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);
 
 
 
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$start = new DateTime();
$start -> setDate(2016,1,1);
$formattedStart = $start->format('Y-m-d');
$end = date("Y-m-d", strtotime('+1 day'));

$reportInput = $client->GetPartnerAccounts(array("PartnerId" => $request))->GetPartnerAccountsResult;
$countClients = $reportInput->ClientCount;
if(!is_array($reportInput->TradeAccounts->string)) {
    $reportInput->TradeAccounts->string = array($reportInput->TradeAccounts->string);
} 

$tradeAccounts = $reportInput->TradeAccounts->string;

if($tradeAccounts !=0) {
    $conn = mysqli_connect("Reports.leveratetech.com", "stockstp", "c7td2UqSjrvCXP6J", "stockstp_real");
    $totalCommission = 0;
    $accounts = array();
    $commissions = array();
    $volumes = array();
    for ($i=0; $i<count($tradeAccounts); $i++) {

        $query = "select sum(volume) from mt4_trades where (cmd=0 or cmd=1) and login=$tradeAccounts[$i]";
        $res = $conn->query($query);

        if ($res != NULL) {

            while ($row = $res->fetch_row()) {
                $itemVolume = $row[0]/100;
                if ($itemVolume == NULL) {
                    $itemVolume = 0;
                }
                if ($itemVolume < 50) {
                    $itemCommission = (5 * $itemVolume);
                } else if ($itemVolume > 49 && $itemVolume < 100) {
                    $itemCommission = (7.5 * $itemVolume);
                } else if ($itemVolume > 99 && $itemVolume < 500) {
                    $itemCommission = (10 * $itemVolume);
                } else {
                    $itemCommission = (12 * $itemVolume);
                }
           }
            $totalCommission = $totalCommission + $itemCommission;
            
            array_push($accounts, $tradeAccounts[$i]);
            array_push($commissions, $itemCommission);
            array_push($volumes, $itemVolume);
            
            

        } else {
            $totalCommission = 0;
            $itemVolume = 0;
            $itemCommission = 0;

        }

    }
    
}

$reports = array("Accounts"=>$accounts, "Comissions"=>$commissions, "Volumes"=>$volumes, "Total"=>$totalCommission, "Clients"=>$countClients);
echo json_encode($reports);


