<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);
    
    $updatePartnerID = $_POST['id'];
    $updatePartnerCommission = $_POST['commission'];
    $updatePartnerFirstName = $_POST['fn'];
    $updatePartnerLastName = $_POST['ln'];
    
    $changeCommission = $client->ChangePartnerComission(array('token'=>$_SESSION['adminToken'],'id'=>$updatePartnerID,'NewComission'=>$updatePartnerCommission))->ChangePartnerComissionResult;
    $updatePartnerMessage = $changeCommission->Message;
    $updatePartnerSuccess = $changeCommission->Success;
    
    if($updatePartnerSuccess==TRUE){
        echo "<div style='color:green; font-size:25px;width:100%;text-align:center;'>UPDATED Commission for: ".$updatePartnerFirstName.' '.$updatePartnerLastName."</div>";
    }
    else {
        echo "<div style='color:red; font-size:25px;width:100%;text-align:center;'>".$updatePartnerMessage."</div>";
    }
?>