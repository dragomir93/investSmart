<?php
session_start();

$language = $_GET['language'];
$campaign = $_GET['campaign'];
?>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="css/create-campaign.css" />
        <link rel="stylesheet" type="text/css" href="css/add-new-language.css" />
        <title>Edit Language</title>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <div class="container">
            
            <form method="post" action="../public/create-campaign-logic.php" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <h3>Language:</h3>
                        <h4><input type="text" name="campaignLanguage" id="language" value="<?php echo $language ?>" readonly></h4>
                    </div>
                    <div class="col-md-6 text-center">
                        <h3>Campaign:</h3>
                        <h4><input type="text" name="campaignName" id="campaign" value="<?php echo $campaign ?>" readonly></h4>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-4 banners-list">
                        <h3>Banners:</h3>
                        
                    </div>
                    
                    <div class="col-md-4 mailers-list">
                        <h3>Mailer:</h3>
                    </div>
                    
                    <div class="col-md-4 lp-list">
                        <h3>Landing page:</h3>
                    </div>
                </div>
                <div class="row banner-upload">
                    
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#" class="btn btn-primary" id="addBanner">Add another banner</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>Add new mailer:</h3>
                        <input type="file" accept="text/html" name="mailer" id="campaignMailer">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>Add new landing page:</h3>
                        <input type="file" accept="text/html" name="landingPage" id="campaignLandingPage">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
                </div>
            </form>
            
        </div>
        <script src="../public/js/create-campaign.js"></script>
        <script src="../public/js/edit-campaign-language.js"></script>
    </body>
</html>
