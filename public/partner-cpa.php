<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);
 
 
 
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$start = new DateTime();
$start -> setDate(2016,1,1);
$formattedStart = $start->format('Y-m-d');
$end = date("Y-m-d", strtotime('+1 day'));

$reports = $client->CalculateCPA(array("PartnerId" => $request, "start" => $formattedStart, "end" => $end))->CalculateCPAResult;


echo json_encode($reports);