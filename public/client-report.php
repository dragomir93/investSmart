<?php

session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


$reports = $client->GetClientReport(array("PartnerId" => $request))->GetClientReportResult;

$items = $reports->Items->DMClientReportItem;


echo json_encode($items);