<?php

include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$language = $client->GetCampaignLanguages(array("CampaignId"=>$request))->GetCampaignLanguagesResult;

if(!is_array($language->DMLanguage)) {
    $language->DMLanguage = array($language->DMLanguage);
}

echo json_encode($language->DMLanguage);
