<?php
include '../configs/config.inc.php';
$client = new SoapClient(URL);
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" href="intl-tel-input-9.2.0/build/css/intlTelInput.css"> <!-- FOR COUNTRY CALL CODE -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/accountDetails.css" /> <!--styling PROFILE page-->
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <title>Registration | StockSTP Affiliate</title>
    </head>
      <!-- Google login -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="486026532062-81b6dihhele0bj8lt7pnngbiblnh82vq.apps.googleusercontent.com">
        <script src="js/google.js"></script>
        
        <!-- Facebook login -->
        <script src="js/facebook.js"></script>
    <body>
         
         <!-- Header -->
        <?php include ('../includes/header.php'); ?>
        <!-- Start Menu Main -->
        <?php include('../includes/menu-main.php'); ?>
        <!-- / Menu Main -->
        <?php include('../includes/inner-img.php'); ?>
        <i style='display: none;' class="fa fa-angle-double-up backToTop" aria-hidden="true"></i>
        <!--END OF HEADER-->
        <div class="container-fluid registration-form-second">
            <div class="basic-data">
                <h3>Basic &#38; Account Information</h3>
            </div>
        </div>
        <div class="container-fluid registration-form">
            <div class="basic-data">
                <h3>Basic information</h3>
            </div>
            <div class="account-data">
                <h3>Account information</h3>
            </div>
        </div>
        <!-- Start Registration Form-->
        <?php
        include('../includes/validation-registrationform.php');
        ?>
        
        <div class="container-fluid registration-form-wrapper">
            <?php include('../includes/registration-form.php'); ?>
        </div>
        

        <?php include('../includes/footer.php'); ?>
        <!-- Country finder -->
        <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
        <script type="text/javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!--        <script type="text/javascript" src="js/InfoShow.js"></script>-->
        <script type="text/javascript" src="js/PassCheck.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script> <!-- Tooltip -->
        <!-- Processing bar -->
        <script src="js/processingAnimation.js" type="text/javascript"></script>
        <!-- Tooltip -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
        <!-- COUNTRY CODE -->
        <script type="text/javascript" src="intl-tel-input-9.2.0/examples/js/isValidNumber.js"></script> <!-- valid number -->
        <!-- keyup -->
        <script src="js/keyup.js" type="text/javascript"></script>
        <!-- main -->
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/registration-validation.js" type="text/javascript"></script>
        <script src="intl-tel-input-9.2.0/build/js/intlTelInput.js"></script><!-- COUNTRY CODE -->
        <script src="intl-tel-input-9.2.0/build/js/utils.js" ></script><!-- COUNTRY CODE initialization-->
        <script src="intl-tel-input-9.2.0/examples/js/defaultCountryIp.js"></script><!-- COUNTRY CODE ip -->
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
        <script type="text/javascript">
                $(document).ready(function () {
                    $(window).scroll(function () {
                        if ($(window).scrollTop() > 500) {
                            $('.backToTop').show(500);
                        } else {
                            $('.backToTop').hide(500);
                        }
                    });
                    $('.backToTop').click(function () {
                        $("html,body").animate({scrollTop: 0}, 1000);
                    });
                });
                /* Back to top change collor on scroll */
                $(window).scroll(function () {
                    var scroll = $(window).scrollTop();

                    if (scroll >= 2500) {
                        $(".fa-angle-double-up").addClass("orange");
                    } else {
                        $(".fa-angle-double-up").removeClass("orange");
                    }
                });
        </script>
        <script type="text/javascript">
            $(".activeJoinNow span").css("color", "#ff9900");
        </script>
        <script type="text/javascript">
//            $(document).ready(function () {
//                $(window).keydown(function (event) {
//                    if (event.keyCode == 13) {
//                        event.preventDefault();
//                        $("#send").click();
//                        return false;
//                    }
//                });
//            });
        </script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                $(".register-form").submit(function () {
                    $('#processingBar').removeAttr('hidden');
                    
            });
            });
            
        </script>
    </body>
</html>
