<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">CREATE CAMPAIGN</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form method="post" action="../public/create-campaign-logic.php" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6 text-center languages">
                                <h3>Choose campaign language:</h3>
                                <select name="languageID" id="languageID">
                                    <option disabled>Choose campaign language</option>
                                </select>
                            </div>
                            <div class="col-md-6 text-center">
                                <h3>Enter campaign name:</h3>
                                <input type="text" name="campaignName" id="campaignName" required pattern="^[A-Za-z0-9 ]*[A-Za-z0-9][A-Za-z0-9 ]*$" placeholder="*required">
                                <input type="hidden" name="addNewLanguage" value="false">
                                
                            </div>
                        </div>
                        <div class="row banner-upload">
                            <div class="col-md-6 text-center">
                                <h3>Choose banner size:</h3>
                                <select name="bannerSize[]" id="bannerSize">
                                    <option>250x250</option>
                                    <option>728x90</option>
                                    <option>160x600</option>
                                    <option>336x280</option>
                                    <option>300x250</option>
                                </select>
                            </div>
                            <div class="col-md-6 text-center">
                                <h3>Upload banner:</h3>
                                <input type="file" accept="image/jpeg" name="banner[]" class="campaign-banner" id="campaignBanner" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="#" class="btn btn-primary" id="addBanner">Add another banner</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3>Upload mailer:</h3>
                                <input type="file" accept="text/html" name="mailer" id="campaignMailer">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3>Upload landing page:</h3>
                                <input type="file" accept="text/html" name="landingPage" id="campaignLandingPage" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">CREATE CAMPAIGN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
