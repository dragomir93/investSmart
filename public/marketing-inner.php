<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/navigationstick-center.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <script src="js/snap.svg-min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" href="intl-tel-input-9.2.0/build/css/intlTelInput.css"> <!-- FOR COUNTRY CALL CODE -->
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <link href="css/about.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
       <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <title>About | StockSTP Affiliate</title>
        
           <!-- Google login -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="486026532062-81b6dihhele0bj8lt7pnngbiblnh82vq.apps.googleusercontent.com">
        <script src="js/google.js"></script>
        
        <!-- Facebook login -->
        <script src="js/facebook.js"></script>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>  
           <!-- Header -->
        <?php include ('../includes/header.php'); ?>
        <!-- Start Menu Main -->
        <?php include('../includes/menu-main.php'); ?>
        <?php include('../includes/inner-img.php'); ?>
        <i style='display: none;' class="fa fa-angle-double-up backToTop" aria-hidden="true"></i>
        <!-- / Menu Main -->
        <!--END OF HEADER-->
        <div class="container-fluid marketing-inn-page">
            <div class="row fadeIn fadeInDuration">
                <div class="col-md-12 aboutContent marketing-inner-line">
                    <h1>Marketing Tools</h1>
                    <p>&Tab;Whether you are a novice or a professional trader, 
                        or if you are a private investor, a web-site owner or blogger, 
                        a corporation o an individual, a financial institution or a bank 
                        – we have a partnership plan that responds your requirements and 
                        accomplishes your goals. With our flexible partner's reward and 
                        commission multipliers you can generate recurring revenue by motivate 
                        other traders to use our professional program. Your income as a partner
                        depends on the number of new clients and the volume of their trades.
                    </p>
                    <div class="our-tools">
                        <h2>OUR TOOLS</h2>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </div>
                </div>
            </div>
            <div class="row make-profit">
                <!-- affiliate-img removed class-->
                <div class="col-sm-6 revealOnScroll" data-animation="fadeInLeft">
                    <img src="../public/img/make-profit.jpg" alt="make-profit">
                </div>
                <div class="col-sm-6 revealOnScroll" data-animation="fadeInRight">
                    <h2><?php echo strtoupper('BANNERS'); ?></h2>
                    <!--                    <h4>Join our partners program and get easy and professional business solutions</h4>
                                        <h4 class="advantage">Why it's better to have a partner?</h4>-->
                    <p>
                        STP affiliate provide for you different kinds and types of banners which will increase your marketing effectivness. 
                        Our banners are produced by our marketing and creative team, who take care of visual effect. 
                        Also we can make specially customized banners and changes to meet your requirement. Banners are available trough your STP Affiliates Account.
                    </p>
                </div>
            </div>
            <div class="row affiliate">
                <div class="col-sm-6 revealOnScroll" data-animation="fadeInLeft">
                    <h2><?php echo ucwords('E-MAILERS'); ?></h2>
                    <p>
                        Our promotional emails will help convert leads to
                        customers in a very efficient way. The content is easy to change and you can personalize the design in the best interest for your marketing moves and strategy.

                    </p>
                </div>
                <div class="col-sm-6 revealOnScroll text-right" data-animation="fadeInRight">
                    <img src="../public/img/affiliate-world.jpg" alt="affiliate-world">
                </div>
            </div>
            <div class="row make-profit">
                <div class="col-sm-6 revealOnScroll" data-animation="fadeInLeft">
                    <img src="../public/img/landing.jpg" alt="make-profit">
                </div>
                <div class="col-sm-6 revealOnScroll" data-animation="fadeInRight">
                    <h2><?php echo strtoupper('LANDING PAGES'); ?></h2>
                    <!--                    <h4>Join our partners program and get easy and professional business solutions</h4>
                                        <h4 class="advantage">Why it's better to have a partner?</h4>-->
                    <p>
                        In your STP Affiliates account you can chose our
                        landing pages with the simply login. Visitors are
                        redirected to landing pages as a second step to our trading platform. Our creative and marketing team use reliable, and safe user experience techniques in
                        designing landing pages that will sure improve your conversion rates. 
                    </p>
                </div>
            </div>
        </div>
        <!-- / Information Box -->
        <?php include('../includes/footer.php'); ?>
        <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
        <!-- / Country finder -->
        <script type="text/javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script><!-- Tooltip -->
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <!--        <script type="text/javascript" src="js/InfoShow.js"></script>-->
        <script type="text/javascript" src="js/PassCheck.js"></script>
        <!-- CAPTCHA 2LINE CODE JS -->
    <!--        [if lt IE 9]><script type='text/javascript' src='js/excanvas.js'></script><![endif]-->
        <script type='text/javascript' src='js/icaptcha.js'></script>
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
        <!-- keyup -->
        <script src="js/keyup.js" type="text/javascript"></script>
        <!-- main -->
        <script src="js/main.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/Date.js"></script>
       <script src="intl-tel-input-9.2.0/build/js/intlTelInput.js"></script><!-- COUNTRY CODE -->
        <script src="intl-tel-input-9.2.0/build/js/utils.js" ></script><!-- COUNTRY CODE initialization-->
        <script src="intl-tel-input-9.2.0/examples/js/defaultCountryIp.js"></script><!-- COUNTRY CODE ip -->
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
        <!--select active page-->
        <script type="text/javascript">
            $(".activeMarketingTools span").css("color", "#ff9900");


            $(document).ready(function () {
                $(window).scroll(function () {
                    if ($(window).scrollTop() > 500) {
                        $('.backToTop').show(500);
                    } else {
                        $('.backToTop').hide(500);
                    }
                });
                $('.backToTop').click(function () {
                    $("html,body").animate({scrollTop: 0}, 1000);
                });
            });
            /* Back to top change collor on scroll */
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();

                if (scroll >= 2500) {
                    $(".fa-angle-double-up").addClass("orange");
                } else {
                    $(".fa-angle-double-up").removeClass("orange");
                }
            });
        </script>
        <script>
            $(".activeNews span").css("color", "#ff9900");
        </script>
    </body>
</html>
