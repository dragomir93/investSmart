<?php

session_start();
include '../configs/config.inc.php';
$client = new SoapClient(URL);


$updateObj = new stdClass();

$updateObj->FirstName = $_POST['FN'];
$updateObj->LastName = $_POST['LN'];
$updateObj->Phone = $_POST['PHONE'];
$updateObj->Mobile = $_POST['MOBILE'];
$updateObj->Country = $_POST['COUNTRY'];
$updateObj->CityRegion = $_POST['CR'];
$updateObj->Address = $_POST['ADDRESS'];
$updateObj->Zip = $_POST['ZIP'];
$updateObj->Company = $_POST['COMPANY'];
$updateObj->WebsiteUrl = $_POST['URL'];
$updateObj->PartnerType = $_POST['PARTNERTYPE'];

$updateData = array('input' => $updateObj);

$updateResult = $client->UpdateUser(array('token' => $_SESSION['token'], $updateData))->UpdateUserResult;
$updateMessage = $updateResult->Message;
$updateSuccess = $updateResult->Success;

if ($updateSuccess == TRUE) {
    // header('Location: accountDetails.php');
    // echo "<div class='updateTrue'>".'UPDATED'."</div>";
    echo "UPDATED <br> $updateSuccess";
} else {
    //header('Location: accountDetails.php');
    // echo "<div class='updateFalse'>".$updateMessage."</div>";
    echo 'NEUSPESNO <br>' . $updateMessage;
    echo '<br>' . $_SESSION['token'];
}
?>