<?php
///OVA STRANICA NAM ZA SADA NE SLUZI NICEMU

session_start();
error_reporting(0);
include '../configs/config.inc.php';

//echo $countCalculateCPAResult = count($arrayCalculateCPARest);
//
//for ($num = 0; $num < $countCalculateCPAResult; $num++) {
//    $reports->string[$num];
//}
//NOT TO ALLOW APPROACH IF THERE IS NO TOKEN
if ($_SESSION['IBFname'] == "" || $_SESSION['username'] == "") {
//    setcookie("IBProfilePassword", '' , time() - 3600,'/');
    header("location: login.php"); //back to login page
}
?>
<html>
    <head>
        <title>Reports</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="shortcut icon" href="img/logo.png"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" href="css/select-jquery.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/calculate-cpa.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <!-- Include JS File Here -->
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
        <script src='js/select-jquery.js'></script>
        <style>
            .top-menu-bg li{
                font-family: 'Oswald', serif;
                font-weight: normal;
            }
            .print-report{
                text-align: center;
            }
            /* Resposnive */
            @media(max-width:990px){
                .parentHeader{
                    font-size: 14px;
                }
            }
        </style>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>  
        <div class="top-header col-md-6">
            <div class="header-logo">
                <a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo-small.png" alt=""/>
                </a>
            </div>
            <?php include('../includes/social-icons.php'); ?>
        </div>
        <div class="account-details col-md-6">
            <p>Hello&#58;</p>
            <h3 style="color: #13869E;"><?php echo $_SESSION['username']; ?></h3>
            <form method="post">
                <button name="LOGOUT" type="submit" class="btn btn-primary">Log Out</button>
            </form>
            <?php
            if (isset($_POST['LOGOUT'])) {
                session_unset();
                session_destroy();
                setcookie("IBProfilePassword", '', time() - 3600, '/');
                header("location: login.php");
            }
            ?>
        </div>
        <!-- Start Menu -->
        <?php include('../includes/menu-admin.php'); ?>
        <img style='display: none;' src="img/arrowUp.png" class="backToTop" alt="arrow"/>
        <!--STARTING CONTENT-->
        <div class="shadowContainer ">
            <!--            <div class="col-sm-3">
                        </div>-->
            <div>
                <?php include('../public/datepicker.php'); ?> 
            </div>
        </div>
        <!-- Start Print Button -->

        <form class="print-report">
            <input class="report-btn" type=button name=print value="Print of save report as a PDF" onclick="printContent('printTable')">
        </form>
        <!--        <form class="print-report">
                    <input class="report-btn" type=button name=print value="Print of save report as a PDF" onClick="window.print()">
                </form>-->
        <!-- / Print Button -->
        <?php include('../includes/footer.php'); ?>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/scrollToTop.js" type="text/javascript"></script>
        <script type="text/javascript">
                $(".activeReports span").css("color", "#ff9900");
        </script>
    </body>
</html>
