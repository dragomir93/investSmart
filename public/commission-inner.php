<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <!--        <link rel="stylesheet" type="text/css" href="css/component.css" />-->
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Fency Box -->
        <link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.6" type="text/css" media="screen" />
        <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
<!--        <script src="js/snap.svg-min.js"></script>-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" href="intl-tel-input-9.2.0/build/css/intlTelInput.css"> <!-- FOR COUNTRY CALL CODE -->
        <link href="css/about.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <title>About | StockSTP Affiliate</title>
        
           <!-- Google login -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="486026532062-81b6dihhele0bj8lt7pnngbiblnh82vq.apps.googleusercontent.com">
        <script src="js/google.js"></script>
        
        <!-- Facebook login -->
        <script src="js/facebook.js"></script>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>  
        <!-- Start Top Header -->
           <!-- Header -->
        <?php include ('../includes/header.php'); ?>
        <!-- Start Menu Main -->
        <?php include('../includes/menu-main.php'); ?>
        <?php include('../includes/inner-img.php'); ?>
        <i style='display: none;' class="fa fa-angle-double-up backToTop" aria-hidden="true"></i>
        <!-- / Menu Main -->
        <!--END OF HEADER-->
        <div class="container commission-wrapper">
            <div class="row fadeIn fadeInDuration">
                <div class="col-md-12 aboutContent marketing-inner-line">
                    <h1>Commission</h1>
                </div>
                <div class="col-md-12 revenue-container">
                    <h2 data-toggle="collapse" data-target="#commission" class="commission">AFFILIATE CPA PLAN<i class="fa fa-chevron-down"></i></h2>
                    <p id="commission" class="collapse">
                        CPA Affiliate program from STP Affiliates is designed for digital marketers who 
                        wish to earn a substantial flat commission fee for every active trader they refer to StockSTP.
                        <br><br>
                        The Cost Per Acquisition (CPA) commission structure guarantees
                        a consistent rate for every new qualified trader whom you refer to Stock STP.
                        <br><br>
                        The more traders you refer, the more money you earn.  Your commission is guaranteed 
                        no matter the trade outcomes. Refer more traders, earn even more profit.
                        <br><br>
                        <a class="fancybox" rel="group" href="img/table.jpg">
                            <img src="img/table.jpg" alt="" />
                        </a>
                    </p>
                    <hr>
                    
                </div>
                <div class="col-md-12 revenue-container">
                    <h2 data-toggle="collapse" data-target="#ib" class="commission">INTRODUCING BROKER plan (REVENUE SHARE)<i class="fa fa-chevron-down"></i></h2>
                    <p id="ib" class="collapse">
                       Introducing brokers IBs earn continuous and instant profits everytime one of their referred clients performs a trade. IBs atStock STP can earn upto $15 per traded lot(approximately 75% Revenue Share).
                        <a class="fancybox" rel="group" href="img/ib.png">
                            <img src="img/ib.png" alt="ibplan" />
                        </a>
                    </p>
                    <hr>
                </div>
                <div class="col-md-12 revenue-container">
                    <h2 data-toggle="collapse" data-target="#hybrid" class="commission">Hybrid Plan<i class="fa fa-chevron-down"></i></h2>
                    <p id="hybrid" class="collapse">The plan offers both immediate reward for your reffered traffic and continuos earnings from your referred client trading.
                        <a class="fancybox" rel="group" href="img/hybrid.png">
                            <img src="../public/img/hybrid.png" alt="hybrid">
                        </a>
                    </p>
                    <hr>
                </div>
                <div class="col-md-12 revenue-container">
                    <h2 data-toggle="collapse" data-target="#sub" class="commission">sub-affiliate<i class="fa fa-chevron-down"></i></h2>
                    <p id="sub" class="collapse">Refer new affiliates to STP Affiliates program and get a whooping 20% of all the profit they
                        earn.
                        <a class="fancybox" rel="group" href="img/marketing.jpg">
                            <img src="../public/img/marketing.jpg" alt="subaffiliate">
                        </a>
                    </p>
                    <hr>
                </div>
                
            </div>
        </div>
        <!-- / Information Box -->
        <?php include('../includes/footer.php'); ?>
          <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
        <!-- / Country finder -->
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
        <script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.6"></script>
        <script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
        <script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
    <!--        [if lt IE 9]><script type='text/javascript' src='js/excanvas.js'></script><![endif]-->
         <script src="intl-tel-input-9.2.0/build/js/intlTelInput.js"></script><!-- COUNTRY CODE -->
        <script src="intl-tel-input-9.2.0/build/js/utils.js" ></script><!-- COUNTRY CODE initialization-->
        <script src="intl-tel-input-9.2.0/examples/js/defaultCountryIp.js"></script><!-- COUNTRY CODE ip -->
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
        <!-- main -->
        <script src="js/main.js" type="text/javascript"></script>
        <!--select active page-->
           <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
        <script type="text/javascript">
            $(".activeCommission span").css("color", "#ff9900");

            $(document).ready(function () {
                $(window).scroll(function () {
                    if ($(window).scrollTop() > 500) {
                        $('.backToTop').show(500);
                    } else {
                        $('.backToTop').hide(500);
                    }
                });
                $('.backToTop').click(function () {
                    $("html,body").animate({scrollTop: 0}, 1000);
                });
            });
            /* Back to top change collor on scroll */
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();

                if (scroll >= 2500) {
                    $(".fa-angle-double-up").addClass("orange");
                } else {
                    $(".fa-angle-double-up").removeClass("orange");
                }
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".fancybox").fancybox();
            });
        </script>
    <script>
            $(".activeAccount span").css("color", "#ff9900");
        </script>
    </body>
</html>
