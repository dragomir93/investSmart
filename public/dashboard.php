<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);
//LOG IN TESTING
if (isset($_POST['adminLogin'])) {
    $_SESSION['adminUsername'] = $_POST['username'];
    $adminPassword = $_POST['password'];

    $adminResponse = $client->AdminLogin(array('username' => $_SESSION['adminUsername'], 'password' => $adminPassword))->AdminLoginResult;

    $_SESSION['adminMessage'] = $adminResponse->Message;
    $_SESSION['adminToken'] = $adminResponse->Token;
    if ($_SESSION['adminToken'] == "") {
        header('location: admin-login.php');
    }
    $adminDetails = $client->GetAdminDetails(array('token' => $_SESSION['adminToken']))->GetAdminDetailsResult;
    $ADMIN = $adminDetails->Admin;
    $READER = $adminDetails->Reader;
    $WRITER = $adminDetails->Writer;
    $adminFirstName = $adminDetails->FirstName;
    $adminLastName = $adminDetails->LastName;
    $adminLoginName = $adminDetails->Username;
}

$adminDetails = $client->GetAdminDetails(array('token' => $_SESSION['adminToken']))->GetAdminDetailsResult;
$ADMIN = $adminDetails->Admin;
$READER = $adminDetails->Reader;
$WRITER = $adminDetails->Writer;
$adminFirstName = $adminDetails->FirstName;
$adminLastName = $adminDetails->LastName;
$adminLoginName = $adminDetails->Username;

$_SESSION['ADMIN'] = $ADMIN;
$_SESSION['READER'] = $READER;
$_SESSION['WRITER'] = $WRITER;

//LOG OUT IF THERE IS NO TOKEN
if ($_SESSION['adminToken'] == "" || $adminFirstName == "") {
    header('location: admin-login.php');
}

//PAGE WILL REFRESH ON SUBMIT AND PENDING REQUESTS WILL ALSO REFRESH
if (isset($_SESSION['counter'])) {
    if ($_SESSION['counter'] == 1) {
        $id = $_POST['ID1'];
        $approveResult = $client->ApprovePartner((array('token' => $_SESSION['adminToken'], 'id' => $id)))->ApprovePartnerResult;
        $approveMessage = $approveResult->Message;
        $approveSuccess = $approveResult->Success;
    } else {
        for ($test = 0; $test <= $_SESSION['counter']; $test++) {
            if (isset($_POST[$test])) {
                $id = $_POST['ID' . $test];
                $approveResult = $client->ApprovePartner((array('token' => $_SESSION['adminToken'], 'id' => $id)))->ApprovePartnerResult;
                $approveMessage = $approveResult->Message;
                $approveSuccess = $approveResult->Success;
            }
        }
    }
    
    
}
?>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="css/create-campaign.css" />
        <style>
            .error {color: #ff0000;}
        </style>
        <title>Dashboard</title>
        <style>
            #login-processing{
                font-size: 15px;
                text-align: center;
            }
            .print-report{
                float: left;
                width: 100%;
                background: orchid;
                height: 60px;
            }
        </style>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>  
        <div class="dashboard-logo">
            <span class="dashboard-title">invest smart</span>
            <span class="dashboard-name">Welcome: <?php
                echo $adminFirstName . '  ' . $adminLastName . ' | ';
                if ($ADMIN == TRUE) {
                    echo '<span style="color:#AB0115;">ADMINISTRATOR</span> |';
                } elseif ($READER == TRUE) {
                    echo '<span style="color:#AB0115;">READ ONLY</span> |';
                } else {
                    echo '<span style="color:#AB0115;">DATA EDIT</span> |';
                }
                ?></span>
            <a href="admin-login.php"><span class="adminLogoutButton"><input type="button" name="adminLogout" value="LogOut"/></span></a>
            <?php
            if (isset($_POST['adminLogout'])) {
                session_destroy();
            }
            ?>
        </div>
        <?php include('../includes/menu-dashboard.php'); ?>
        <div class="mainContainer">
            <!--PENDING REQUESTS CONTAINER-->
            <div class="container-fluid pendingRequestsContainer" >
                <div class="row">
                    <div class="col-lg-12 welcome-text" style="font-size:25px;" >
                        <span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;&nbsp;PENDING REQUESTS
                    </div>
                    <!--PROCESSING BAR-->
                    <div id="login-processing" hidden>
                        <!--                                <div class="loading">-->
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                        <!--                                </div>-->
                        <p id="processingText">Processing... Please wait ...</p>
                    </div>
                    <?php
//                    if (isset($_SESSION['counter'])) {
//                        if ($approveSuccess == TRUE) {
                    echo "<div id='approvedStatusTrue' hidden style='color:green; font-size:20px;margin-top:20px; text-align:center;' id='APPROVED'>APPROVED</div>";
//                        } else {
                    echo "<div id='approvedStatusFalse' hidden style='color:red; font-size:20px; text-align:center;'>" . $approveMessage . "</div>";
//                        }
//                    }
                    ?>
                    <div id="printTable" class="col-lg-12 pendingForm">
                        <?php include('../includes/panding-requests.php'); ?>
                    </div>
                </div>
            </div>
            <!--EDIT PARTNER CONTAINER-->
            <div class="container-fluid editPartnerContainer" > <!-- start of container -->
                <div class="row">
                    <div class="col-lg-12 welcome-text" style="font-size:25px;">
                        <span class="glyphicon glyphicon-floppy-saved" ></span>&nbsp;&nbsp;&nbsp; Edit Partner
                    </div>
                    <div class="col-lg-12 pendingForm">
                        <div id="editPartner">
                            <?php include('../includes/edit-partner.php'); ?>
                        </div>
                        <!--UPDATE RESULT-->
                        <div id="updateResult" >
                        </div>
                        <!--SEARCH RESULT-->
                        <div id="partnerResult">
                        </div>
                    </div>
                    <!--PARTNERS UPDATE-->
                </div>
            </div>
        </div>
        <?php include('../includes/footer.php'); ?>
        <!-- Start Script -->
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/dashboard.js" type="text/javascript"></script>
        <!--ON PAGE LOAD-->
        <script type="text/javascript">
            $(document).ready(function () {
                $('.pendingRequests').addClass('active');
                $('.pendingRequestsContainer').show(500);
                $('.editPartnerContainer').hide();
            });
            $('.pendingRequests').click(function () {
                $('.pendingRequests').addClass('active');
                $('.pendingRequestsContainer').show(500);

                $('.homeDashboard').removeClass('active');
                $('.homeDashboardContainer').hide(500);

                $('.editPartner').removeClass('active');
                $('.editPartnerContainer').hide(500);
            });

            $('.editPartner').click(function () {
                $('.editPartner').addClass('active');
                $('.editPartnerContainer').show(500);

                $('.homeDashboard').removeClass('active');
                $('.homeDashboardContainer').hide(500);

                $('.pendingRequests').removeClass('active');
                $('.pendingRequestsContainer').hide(500);
            });
        </script>
        <!--RETREIVING SEARCH RESULT-->
        <script type="text/javascript">
            $(document).ready(function () {
                
                $("#searchForm").submit(function (event) {
                    event.preventDefault();
                    var res = document.getElementById('updateResult');
                    res.innerHTML = "";
                    var email = document.getElementById('searchEmail').value;
                    var Fname = document.getElementById('searchFirstName').value;
                    var Lname = document.getElementById('searchLastName').value;
                    var country = document.getElementById('searchCountry').value;
                    var partnerType = document.getElementById('searchPartnerType').value;
                    var dataString = 'Email=' + email + '&Fname=' + Fname + '&Lname=' + Lname + '&country=' + country + '&partnerType=' + partnerType;
                    $.ajax({
                        type: "post",
                        url: "editPartner.php",
                        data: dataString,
                        cache: false,
                        success: function (html) {
                            $('#partnerResult').html(html);
                        }
                    });
                    return false;
                });
            });
        </script>
        <!--UPDATE DETAILS-->
        <script type="text/javascript">
            $(document).on('click', function () {
                var x = document.getElementById('clickedID').value;
                $('#editForm' + x).submit(function (e) {
                    e.preventDefault();
                    var id = document.forms['editForm' + x].elements['PartnerID' + x].value;
                    var commission = document.forms['editForm' + x].elements['PartnerCustomCommission' + x].value;
                    var fn = document.forms['editForm' + x].elements['PartnerFirstName' + x].value;
                    var ln = document.forms['editForm' + x].elements['PartnerLastName' + x].value;
                    //var id = document.getElementById('PartnerID'+c).value;
                    //var commission = document.getElementById('PartnerCustomCommission'+c).value;
                    //var fn = document.getElementById('PartnerFirstName'+c).value;
                    //var ln = document.getElementById('PartnerLastName'+c).value;
                    var dataVal = 'id=' + id + '&commission=' + commission + '&fn=' + fn + '&ln=' + ln;
                    $.ajax({
                        type: "post",
                        url: "updatingPartnerCommission.php",
                        data: dataVal,
                        success: function (html) {
                            $('#updateResult').html(html);
                            setTimeout(function () {
                                $("#updateResult").empty();
                            }, 5000);
                        }
                    });
                    return false;
                });
            });
        </script>
        <script type="text/javascript">
            function returnID(id) {
                var clickedID = document.getElementById('clickedID');
                clickedID.value = id;
            }
        </script>
        <script type="text/javascript">
            function toggling(id) {
                $('#DetailsContainer' + id).toggle();
            }
        </script>
        <!--FADE OUT APPROVED MESSAGE AFTER 5 SECONDS-->
        <script type="text/javascript">
            setTimeout(function () {
                $("#APPROVED").fadeOut().empty();
            }, 5000);
        </script>
        <script type="text/javascript">
            //processing approval
            function processBar() {
                $('#login-processing').removeAttr("hidden");
                var success = "<?php echo $approveSuccess; ?>";
                if (success == true) {
                    $('#approvedStatusTrue').removeAttr('hidden');
                } else {
                    $('#approvedStatusFalse').removeAttr('hidden');
                }
            }
        </script>
        <script type="text/javascript">
            
            function calculatePartnerCpa(id) {
                $.post({
                  url: './partner-cpa.php',
                  data: id,
                  success: function(result) {
                      var resultJson = JSON.parse(result);
                      
                      var items = resultJson.Items.CPAItem;
                      
                      if (items != undefined) {
                        if (items.constructor != Array) {
                            var items = Array(items);
                        }
                    }
                      
                      $("#modalCountFTD").text(resultJson.CountFTD);
                      $("#modalTotalCPA").text(resultJson.TotalCPA);
                      $("#modalExtraCPA").text(resultJson.ExtraCPA);
                      $("#modalCountClients").text(resultJson.CountClients);
                      $("#modalCountSA").text(resultJson.CountSA);
                      $("#modalSACPA").text(resultJson.SACPA);
                    
                      

                    for(var item in items) {
                          var itemNumberForDisplay = item;
                          itemNumberForDisplay++
                          
                          var dateTime = items[item].Time;
                          var fields = dateTime.split('T');
                          var date = fields[0].split("-");
                          date.reverse();
                          var formattedDate = date.join('/');
                          var time = fields[1];
                          
                        
                          var answer;
                          if(items[item].FTD == true) {
                              answer = "Yes"
                          } else {
                              answer = "No"
                          }
                          $("#modalReport").append("<tr><td>" + itemNumberForDisplay + "</td>" + "<td>" + formattedDate + "</td>" + "<td>" + time + "</td>" + "<td>" + items[item].Amount + " USD</td>" + "<td>" + items[item].Description + "</td>" + "<td>" + items[item].Country + "</td>"+"<td>" + answer + "</td></tr>");
                         
                      }
                      
                  }
                });
            }
            
            function calculatePartnerIB(id) {
                
                $.post({
                    url: './partner-ib.php',
                    data: id,
                    success: function(result) {
                        var resultJson = JSON.parse(result);
                        
                        $("#totalIBComission").text(resultJson.Total);
                        
                        for (var item in resultJson.Accounts) {
                            
                            var itemNumberForDisplay = item;
                            itemNumberForDisplay++
                            
                            $("#modalIBReport").append("<tr><td>" + itemNumberForDisplay + "</td>" + "<td>" + resultJson.Accounts[item] + "</td>" + "<td>" + resultJson.Comissions[item] + "</td>" + "<td>" + resultJson.Volumes[item] + "</td></tr>");
                        }
                        
                    }
                });
                
            }
            
            function clearModal() {
                $("#modalReport").html('');
                $("#modalIBReport").html('');
                
            }
        </script>
        <script src="js/client-report.js"></script>
        <script src="js/create-campaign.js"></script>
        <script src="js/edit-campaign.js"></script>
    </body>
</html>
