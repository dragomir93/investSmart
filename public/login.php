<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);

if (isset($_COOKIE['IBProfilePassword'])) {
    $send = $client->UserLogin(array("email" => $_COOKIE['IBProfileUsername'], "password" => $_COOKIE['IBProfilePassword']))->UserLoginResult;

    $sendToken = $send->Token;
//    $sendMessage = $send->Message;

    $_SESSION['TOKEN'] = $sendToken;
//    $_SESSION['message'] = $sendMessage;

    if ($_SESSION['TOKEN'] != "") {
        header("location: accountdetails.php");
        setcookie("IBProfileUsername", $_COOKIE['IBProfileUsername'], time() + (90 * 24 * 60 * 60), "/");
        setcookie("IBProfilePassword", $_COOKIE['IBProfilePassword'], time() + (90 * 24 * 60 * 60), "/");
    }
//    else if ($_SESSION['TOKEN'] == "") {
//        header("location: login.php"); //back to login page        
//    }
}
?>
<html class="demo-1 no-js">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!-- jDatepicker CSS -->
         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" href="intl-tel-input-9.2.0/build/css/intlTelInput.css"> <!-- FOR COUNTRY CALL CODE -->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css">
        <!-- Box Effect JS -->
        <script src="js/snap.svg-min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <style>
            .error {color: #ff0000;}
        </style>
        <title>Login</title>
         <!-- Google login -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="486026532062-81b6dihhele0bj8lt7pnngbiblnh82vq.apps.googleusercontent.com">
        <script src="js/google.js"></script>
        
        <!-- Facebook login -->
        <script src="js/facebook.js"></script>
    </head>
    <body class="login-body">
          <!-- Header -->
        <?php include ('../includes/header.php'); ?>
        <?php include('../includes/menu-main.php'); ?>
        <?php include('../includes/inner-img.php'); ?>
        <div class="admin">
            <div class="container login-form animated fadeInLeft">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Login &#58;</h2>
                    <?php include('../includes/login-form.php'); ?>
                </div>
            </div>
            <div class='forgot-pass-container container  ' hidden>
                <?php include('../includes/forgot-password.php'); ?>
            </div>
            <div id="passResponse">
            </div>
        </div>
<!--        <div class="main animated bounceInUp">
            <h2>Introducing Broker &#40;Revshare&#41;</h2>
            <?php include('../includes/broker-information.php'); ?>
        </div>-->
        <?php include('../includes/footer.php'); ?>
    <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
        <script>
            document.forms["login-form"].onsubmit = function () {
                var pass = document.getElementById("userVal").value;
                var rePass = document.getElementById("passVal").value;
                if (pass == "" || rePass == "") {
                    $('#login-processing').removeAttr('hidden');
                } else {
                    $('#login-processing').removeAttr('hidden');
                }
            };
        </script>
        <script src="intl-tel-input-9.2.0/build/js/intlTelInput.js"></script><!-- COUNTRY CODE -->
        <script src="intl-tel-input-9.2.0/build/js/utils.js" ></script><!-- COUNTRY CODE initialization-->
        <script src="intl-tel-input-9.2.0/examples/js/defaultCountryIp.js"></script><!-- COUNTRY CODE ip -->
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
        <script type="text/javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(".activeLogin span").css("color", "#ff9900");
        </script>
        <script type="text/javascript">
            $("#loginButton").click(function () {
                $(".error_msg").attr("hidden", true);
            });
        </script>
        <script type="text/javascript">
            $("#showRequestForm").click(function () {
                $(".forgot-pass-container").toggle(500);
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.forgot-pass').submit(function (e) {
                    e.preventDefault();
                    var mail = document.getElementById('requestMail').value;
                    var data = "mail=" + mail;
                    $.ajax({
                        type: 'POST',
                        url: "sendPassRequest.php",
                        data: data,
                        cache: false,
                        success: function (html) {
                            $('#passResponse').html(html);
                            setTimeout(function () {
                                $("#passResponse").empty();
                            }, 10000);
                        }
                    });
                    $('.requestPass').removeAttr("hidden");
                    return false;
                });
            });
        </script>
    </body>
</html>
