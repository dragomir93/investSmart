<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);

$searchObj = new stdClass();
$searchObj->Email=$_POST['Email'];
$searchObj->FirstName=$_POST['Fname'];
$searchObj->LastName=$_POST['Lname'];
$searchObj->Country=$_POST['country'];
$searchObj->PartnerType=$_POST['partnerType'];
$getSearchResult= $client->GetSearchResult(array('token'=>$_SESSION['adminToken'],'input'=>$searchObj));
$searchResponse= $getSearchResult->GetSearchResultResult;

if($searchResponse->DMSearchResult != NULL) {
    if (!is_array($searchResponse->DMSearchResult)) {
        $searchResponse->DMSearchResult = array($searchResponse->DMSearchResult);
    }   
} else {
    echo "<div style='font-size:25px; color:red; text-align:center; margin-top:20px;'>".'No search result!'."</div>";
}
echo "<div style='color:green;margin-top:20px;margin-bottom:30px;'>".count($searchResponse->DMSearchResult).' Search Result'."</div>";
for($i=0;$i<count($searchResponse->DMSearchResult);$i++) {
    $resultID = $searchResponse->DMSearchResult[$i]->Id;
    $resultEmail = $searchResponse->DMSearchResult[$i]->Email;
    $resultFirstName = $searchResponse->DMSearchResult[$i]->FirstName;
    $resultLastName = $searchResponse->DMSearchResult[$i]->LastName;
    $resultPhone = $searchResponse->DMSearchResult[$i]->Phone;
    $resultMobile = $searchResponse->DMSearchResult[$i]->Mobile;
    $resultPartnerType = $searchResponse->DMSearchResult[$i]->PartnerType;
    $resultCountry = $searchResponse->DMSearchResult[$i]->Country;
    $resultCustomCommission = $searchResponse->DMSearchResult[$i]->CustomComission;
    $d = $searchResponse->DMSearchResult[$i]->RegisteredDate;
    $dateArr = explode("T", $d);
    $date = date_create($dateArr[0]);
    $dateRegistered = date_format($date,"d M Y");

    //GETTING USER PAYMENT DETAILS
    $result = $client->GetPaymentDetailsAdmin(array('token'=>$_SESSION['adminToken'],'id'=>$resultID))->GetPaymentDetailsAdminResult;
    $method = $result->Method;
    $accountNumber = $result->AccountNumber;
    $bankName = $result->BankName;
    $bankAddress = $result->BankAddress;
    $bankCountry = $result->BankCountry;
    $bankState = $result->BankState;
    $swift = $result->Swift;
    $iban = $result->Iban;
    $aba = $result->Aba;
    $correspondence = $result->CorespondenceBank;

    echo "<div class='forms-wrapper'>";
    echo "
    <form method='POST' action=''  >
    <div class='form-group'>
        <label for='ID' class='control-label'>ID</label>
        <div>
            <input type='text' class='form-control' id='' style='border:none;'  value='$resultID' name='PartnerID1'>
        </div>
    </div> 
    <div class='form-group'>
        <label for='dateRegistered' class='control-label'>Date Registered</label>
        <div>
            <input type='text' class='form-control' id='' style='border:none;'  value='$dateRegistered' name='PartnerDateRegistered'>
        </div>
    </div>

    <div class='form-group'>
        <label for='firstName' class='control-label'>First Name</label>
        <div>
            <input type='text' class='form-control' id=''style='border:none;'  value='$resultFirstName' name='PartnerFirstName' readonly>
        </div>
    </div> 
    <div class='form-group'>
        <label for='lastName' class='control-label'>Last Name</label>
        <div>
            <input type='text' class='form-control ' id='' style=border:none;' value='$resultLastName' name='PartnerLastName' readonly>
        </div>
    </div>

    <div class='form-group'>
        <label for='Email' class='control-label'>Email</label>
        <div>
            <input type='email' class='form-control' id='' style='border:none;'  value='$resultEmail' name='PartnerEmail' readonly>
        </div>
    </div>
    <div class='form-group'>
        <label for='partnerPhone' class='control-label'>Phone</label>
        <div>
            <input type='text' class='form-control' id='' style='border:none;' value='$resultPhone' name='PartnerPhone' readonly>
        </div>
    </div>
    <div class='form-group'>
        <label for='partnerMobile' class='control-label'>Mobile</label>
        <div>
            <input type='text' class='form-control' id='' style='border:none;' value='$resultMobile' name='PartnerMobile' readonly>
        </div>
    </div>

    <div class='form-group'>
        <label for='PartnerType' class='control-label'>Partner Type</label>
        <div>
            <input type='text' class='form-control' id='' style='border:none;' value='$resultPartnerType' name='PartnerType' readonly>
        </div>
    </div>
    <div class='form-group'>
        <label for='PartnerCountry' class='control-label'>Country</label>
        <div>
            <input type='text' class='form-control' id='' style='border:none;' value='$resultCountry' name='PartnerCountry' readonly>
        </div>
    </div>
    <div class='form-group'>
        <label for='PartnerCustomCommission' class='control-label'>Custom Commission</label>
        <div>
            <input type='text' class='form-control' id='' style='border:none;' value='$resultCustomCommission' name='PartnerCustomCommission'>
        </div>
    </div>

    <div class='form-group payment-info' style='clear:both; font-size:15px; border-bottom:1px solid gray;margin-top:10px; margin-bottom:10px;padding-bottom:10px;'>
        <a id=$i class='view$i' onClick='toggling(this.id);'><span class='glyphicon glyphicon-zoom-in' ></span>&nbsp;&nbsp;&nbsp; View Payment Details</a>
    </div>

    <div id='DetailsContainer$i' hidden>

        <div class='form-group'>
            <label  class='control-label'>Method</label>
            <div>
                <input type='text' class='form-control' style='border:none;'  value='$method' readonly>
            </div>
        </div> 
        <div class='form-group'>
            <label control-label'>Account Number</label>
            <div>
                <input type='text' class='form-control '  style='border:none;' value='$accountNumber' readonly>
            </div>
        </div>

        <div class='form-group'>
            <label  class='control-label'>Bank Name</label>
            <div>
                <input type='email' class='form-control'  style='border:none;'  value='$bankName' readonly>
            </div>
        </div>
        <div class='form-group'>
            <label  class='control-label'>Bank Address</label>
            <div>
                <input type='text' class='form-control'  style='border:none;' value='$bankAddress' readonly>
            </div>
        </div>
        <div class='form-group'>
            <label  class='control-label'>Bank Country</label>
            <div>
                <input type='text' class='form-control'  style='border:none;' value='$bankCountry' readonly>
            </div>
        </div>
        <div class='form-group'>
            <label  class='control-label'>Bank State</label>
            <div>
                <input type='text' class='form-control'  style='border:none;' value='$bankState' readonly>
            </div>
        </div>
        <div class='form-group'>
            <label  class='control-label'>Swift</label>
            <div>
                <input type='text' class='form-control'  style='border:none;' value='$swift' readonly>
            </div>
        </div>
        <div class='form-group'>
            <label  class='control-label'>Iban</label>
            <div>
                <input type='text' class='form-control'  style='border:none;' value='$iban' readonly>
            </div>
        </div>
        <div class='form-group'>
            <label  class='control-label'>ABA</label>
            <div>
                <input type='text' class='form-control'  style='border:none;' value='$aba' readonly>
            </div>
        </div>
        <div class='form-group'>
            <label  class='control-label'>Correspondence</label>
            <div>
                <input type='text' class='form-control'  style='border:none;' value='$correspondence' readonly>
            </div>
        </div>
    </div>

    <div class='form-group1 register-button-home' style='margin-top:10px; margin-bottom:10px;padding-bottom:10px;'><div>
    <a href=''><input id=''  type='submit' value='UPDATE' class='register btn btn-primary '></a>";
    if($resultPartnerType == 'Junior Partner') {
        echo "<a id='$resultID' onclick='calculatePartnerCpa(this.id);' data-toggle='modal' data-target='#calculateCpaModal' class='btn btn-primary calculate-cpa-btn'>Calculate CPA</a>";
    } else if ($resultPartnerType == 'Senior Partner') {
        echo"<a id='$resultID' onclick='calculatePartnerIB(this.id);' data-toggle='modal' data-target='#calculateIBModal' class='btn btn-primary calculate-cpa-btn'>Calculate IB</a>";
        echo"<a id='$resultID' onclick='getClientReport(this.id)' data-toggle='modal' data-target='#clientReportModal' class='btn btn-primary calculate-cpa-btn'>Client report</a>";
    } else {
        echo"<a id='$resultID' onclick='calculatePartnerCpa(this.id);' data-toggle='modal' data-target='#calculateCpaModal' class='btn btn-primary calculate-cpa-btn'>Calculate CPA</a>";
        echo"<a id='$resultID' onclick='calculatePartnerIB(this.id);' data-toggle='modal' data-target='#calculateIBModal' class='btn btn-primary calculate-cpa-btn'>Calculate IB</a>";
        echo"<a id='$resultID' onclick='getClientReport(this.id)' data-toggle='modal' data-target='#clientReportModal' class='btn btn-primary calculate-cpa-btn'>Client report</a>";
    }
    include("../includes/client-report.html");
    
       echo"<div id='calculateCpaModal' class='modal fade' role='dialog'>
          <div class='modal-dialog'>

            <!-- Modal content-->
            <div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' onclick='clearModal();'>&times;</button>
                    <h4 class='modal-title'>CPA DETAILS FOR SELECTED PARTNER</h4>
                </div>
                <div class='modal-body'>
                    <h5>Overview</h5>
                    <table>
                        <thead>
                            <tr>
                                <th>CountFTD</th>
                                <th>ExtraCPA</th>
                                <th>TotalCPA</th>
                                <th>Number of clients</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                <td><span id='modalCountFTD'></span></td>
                                <td><span id='modalExtraCPA'></span></td>
                                <td><span id='modalTotalCPA'></span></td>
                                <td><span id='modalCountClients'></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <h5>Report</h5>
                    <div class='modal-table-wrapper'>
                        <table>
                            <thead>
                                <tr>
                                    <td>Deposit number</td>
                                    <td>Date of deposit</td>
                                    <td>Time of deposit</td><td>Amount</td>
                                    <td>Description</td>
                                    <td>Country</td>
                                    <td>FTD</td>
                                </tr>
                            </thead>
                            <tbody id='modalReport'>
                            <tbody>
                        </table>
                    </div>
                     <h5>Subaffiliates</h5>
                    <table>
                        <thead>
                            <tr>
                                <th>CountSA</th>
                                <th>SACPA</th>

                            </tr>
                        </thead>
                        <tbody id='modalSubaffiliates'>
                             <tr>
                                <td><span id='modalCountSA'></span></td>
                                <td><span id='modalSACPA'></span></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal' onclick='clearModal();'>Close</button>
                </div>
            </div>

          </div>
        </div>

        <!--- IB MODAL ---->
        <div id='calculateIBModal' class='modal fade' role='dialog'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' onclick='clearModal();'>&times;</button>
                    <h4 class='modal-title'>IB DETAILS FOR SELECTED PARTNER</h4>
                    </div>

                    <div class='modal-body'>
                       <h5>DETAILS</h5>
                        <div class='modal-table-wrapper'>
                            <table>
                                <thead>
                                    <tr>
                                        <td>Order Number</td>
                                        <td>Account</td>
                                        <td>Comission</td>
                                        <td>Volume</td>
                                    </tr>
                                </thead>
                                <tbody id='modalIBReport'>
                                <tbody>
                            </table>
                        </div>
                        <h5>TOTAL</h5>
                        <table>
                            <thead>
                                <tr>
                                    <th>Total Comission</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <tr>
                                    <td><span id='totalIBComission'></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class='modal-footer'>
                        <button type='button' class='btn btn-default' data-dismiss='modal' onclick='clearModal();'>Close</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
</form>";
    echo"<form action='delete.php' method='post' onsubmit=\"return confirm('THIS WILL PERMANENTLY DELETE THIS USER!!! Are you sure you want to continue?');\">";
    echo"<button type='submit' class='register btn btn-primary' name='delete' value='$resultID'>Delete</button>";
    echo"</form></div>";
}
            
?>
