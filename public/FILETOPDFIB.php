<?php

include '../configs/config.inc.php';
$client = new SoapClient(URL);
require('../fpdf/fpdf.php');
session_start();
$lastName = $_POST['fName'];
$firstName = $_POST['lName'];
$start = $_POST['start'];
$end = $_POST['end'];
$TotalComission = $_POST['TotalComission']; 
$CountClientsIB = $_POST['CountClientsIB']; 
$numbOfItemsIB = $_POST['numberOfItemsIB'];


$pdf = new FPDF();

$pdf->AddPage();

//$pdf->SetFillColor(255, 0, 0);
//$pdf->SetTextColor(255);
//$pdf->SetDrawColor(128, 0, 0);
//$pdf->SetLineWidth(.3);

$pdf->SetFont("Arial", "B", "25");
$pdf->SetXY(5, 5);
$pdf->SetTextColor(25, 54, 72);
$pdf->Cell(0, 10, "Profit Report for: $firstName $lastName", 0, 0, "C");

//$pdf->SetFont("Arial", "B", "25");
//$pdf->SetXY(5, 5);
//$pdf->SetTextColor(25, 54, 72);
//$pdf->Cell(0, 10, "Profit Report for: $firstName $lastName", 0, 0, "C");

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(25, 25);
$pdf->SetTextColor(15, 14, 72);

$pdf->Cell(0, 10, "For Period:", 0, 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(10, 35);
$pdf->Cell(0, 10, "Start Date:", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(40, 35);
$pdf->Cell(0, 10, $start, 0, 0);
//$pdf->Cell(50, 5,  $start, 1, 0, 'L', 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(10, 45);
$pdf->Cell(0, 10, "End Date:", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(40, 45);
$pdf->Cell(0, 10, $end, 0, 0);
//$pdf->Cell(50, 5,  $end, 1, 0, 'L', 0);


$sendingToken = $client->GetUserDetails(array("token" => $_SESSION['TOKEN']))->GetUserDetailsResult;
$partnerType = $sendingToken->PartnerType;


    

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(30, 65);
$pdf->Cell(0, 10, "TotalComission", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(20, 75);
$pdf->Cell(70, 10, $TotalComission, 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "16");
$pdf->SetXY(110, 65);
$pdf->Cell(0, 10, "Number of clients", 0, 0);
$pdf->SetFont("Arial", "", "16");
$pdf->SetXY(99, 75);
$pdf->Cell(70, 10, $CountClientsIB, 1, 0, 'C', 0);

    //FOR ITEMS DATA SHOW

$offset = 8;
$height = 105;

$pdf->SetFont("Arial", "B", "12");
$pdf->SetXY(8, 105);
$pdf->Cell(40, 8, "Account", 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "12");
$pdf->SetXY(48, 105);
$pdf->Cell(40, 8, "Comission", 1, 0, 'C', 0);

$pdf->SetFont("Arial", "B", "12");
$pdf->SetXY(88, 105);
$pdf->Cell(40, 8, "Volume", 1, 0, 'C', 0);





if ($numbOfItemsIB > 1) {
    for ($i = 0; $i < $numbOfItemsIB; $i++) {
        
        $height += $offset;
        
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(8, $height);
        $pdf->Cell(40, 8, $_SESSION["account"] = $_POST["account$i"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(48, $height);
        $pdf->Cell(40, 8, $_SESSION['comission'] =  $_POST["comission$i"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(88, $height);
        $pdf->Cell(40, 8, $_SESSION["volume"] =  $_POST["volume$i"] . '', 1, 0, 'C', 0);
     
    }
} elseif ($numbOfItemsIB == 1) {
        $height += $offset;
        
        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(8, $height);
        $pdf->Cell(40, 8, $_SESSION["account"] = $_POST["account"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(48, $height);
        $pdf->Cell(40, 8, $_SESSION['comission'] =  $_POST["comission"] . '', 1, 0, 'C', 0);

        $pdf->SetFont("Arial", "", "8");
        $pdf->SetXY(88, $height);
        $pdf->Cell(40, 8, $_SESSION["volume"] =  $_POST["volume"] . '', 1, 0, 'C', 0);
        
}
    

    
    
if($numbOfItemsIB == 0){
    $pdf->SetFont("Arial", "", "20");
    $pdf->SetMargins(1, 1);
    $pdf->SetXY(0, 115);
    $pdf->SetTextColor(255, 0, 0);
    $pdf->Cell(0, 0, "You have no items to display!", 0, 0, "C");
}

$pdf->Output();


