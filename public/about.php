<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/navigationstick-center.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <script src="js/snap.svg-min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" href="intl-tel-input-9.2.0/build/css/intlTelInput.css"> <!-- FOR COUNTRY CALL CODE -->
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link href="css/about.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <title>About | StockSTP Affiliate</title>
          <!-- Google login -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="486026532062-81b6dihhele0bj8lt7pnngbiblnh82vq.apps.googleusercontent.com">
        <script src="js/google.js"></script>
        
        <!-- Facebook login -->
        <script src="js/facebook.js"></script>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>  
         <!-- Header -->
        <?php include ('../includes/header.php'); ?>
        
        <!-- Start Menu Main -->
        <?php include('../includes/menu-main.php'); ?>
        <?php include('../includes/inner-img.php'); ?>
        <i style='display: none;' class="fa fa-angle-double-up backToTop" aria-hidden="true"></i>
        <!-- / Menu Main -->
        <!--END OF HEADER-->
        <div class="container-fluid">
            <div class="row fadeIn fadeInDuration">
                <div class="aboutContent">
                    <p>STP affiliates is the official affiliate program of the regulated financial trading broker Stock STP. Our Affiliate program offers maximum flexibility when it comes to marketing tools, reporting and commissions.
                        <br><br>
                        Thanks to combination of cutting-edge technology and an experienced customer centered team STP Affiliates can help affiliates optimize their approach and maximize their profits.  
                    </p>
                </div>
                <div class="aboutContent">
                    <h1>Make Productive Partnership</h1>
                    <p>&Tab;Whether you are a novice or a professional trader, 
                        or if you are a private investor, a web-site owner or blogger, 
                        a corporation o an individual, a financial institution or a bank 
                        – we have a partnership plan that responds your requirements and 
                        accomplishes your goals. With our flexible partner's reward and 
                        commission multipliers you can generate recurring revenue by motivate 
                        other traders to use our professional program. Your income as a partner
                        depends on the number of new clients and the volume of their trades. </p>
                </div>
                <div class="aboutContent">
                    <h1>What Commission structure do we offer?</h1>
                    <p>
                        STP affiliates offers flexible commission structures such as CPA (Cost Per acquisition), Revenue Sharing and hybrid models. 
                        Commission are credit instantly, approved in a timely manner and paid directly to the affiliate.
                    </p>
                </div>
                <div class="aboutContent">
                    <h1>Why Stock STP?</h1>
                    <ul>
                        <li>Licensed and regulated by Cyprus Securities and Exchange Commission</li>
                        <li>Advanced MT4 and Sirix platforms</li>
                        <li>Fast NDD no dealing desk execution</li>
                        <li>Free and professional trading education</li>
                        <li>Market insights, analysis and news</li>
                        <li>Sirix social trading – copy top traders</li>
                        <li>Tight spreads from 0 pips, rebates and much more…</li>
                    </ul>
                </div>
                <!--                <div class="aboutContent">
                                    <h1>Make Productive Partnership</h1>
                                    <p>&Tab;Whether you are a novice or a professional trader, 
                                        or if you are a private investor, a web-site owner or blogger, 
                                        a corporation o an individual, a financial institution or a bank 
                                        – we have a partnership plan that responds your requirements and 
                                        accomplishes your goals. With our flexible partner's reward and 
                                        commission multipliers you can generate recurring revenue by motivate 
                                        other traders to use our professional program. Your income as a partner
                                        depends on the number of new clients and the volume of their trades. </p>
                                </div>
                                <div class="aboutContent">
                                    <h1>Partner's comission for each trade</h1>
                                    <p>&Tab;The StockSTP partnership program provides an opportunity 
                                        to get commission reward from your referrals’ trading.<br><br>
                                        StockSTP offers you a partner’s commission for each trade your clients make.
                                        As a beginner, you may use a regular commission reward scheme. To ascribe the 
                                        commission to your account, we apply a multilevel agent model. It permits getting 
                                        income not only from the 1st-level clients, (those brought to us by you personally),
                                        but also from your 2nd and 3rd –level clients (those attracted to this way of trading
                                        by your referrals). You are also offered income multipliers, improving your commission
                                        based on your referrals’ total trading volume.
                                        The partnership program for PAMM accounts is also disposed.
                                        The amount of comission reward for different partnership levels is discussed on an individual basis and is determined in the agreement.
                                        Any StockSTP partner or client can boost their profits by bringing new traders to the company.
                                    </p>
                                </div>-->
            </div>
        </div>
        <!-- / Information Box -->
        <?php include('../includes/footer.php'); ?>
        <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
    <!-- / Country finder -->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script>
    
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script> <!-- Tooltip -->
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
<!--        <script type="text/javascript" src="js/InfoShow.js"></script>-->
    <script type="text/javascript" src="js/PassCheck.js"></script>
    <!-- CAPTCHA 2LINE CODE JS -->
<!--        [if lt IE 9]><script type='text/javascript' src='js/excanvas.js'></script><![endif]-->
    <script type='text/javascript' src='js/icaptcha.js'></script>
    <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder -->
    <!-- keyup -->
    <script src="js/keyup.js" type="text/javascript"></script>
    <!-- main -->
    <script src="js/main.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Date.js"></script>
    <script src="intl-tel-input-9.2.0/build/js/intlTelInput.js"></script><!-- COUNTRY CODE -->
    <script src="intl-tel-input-9.2.0/build/js/utils.js" ></script><!-- COUNTRY CODE initialization-->
    <script src="intl-tel-input-9.2.0/examples/js/defaultCountryIp.js"></script><!-- COUNTRY CODE ip -->
    
    <script type="text/javascript">
        $(".activeAbout span").css("color", "#ff9900");

        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(window).scrollTop() > 500) {
                    $('.backToTop').show(500);
                } else {
                    $('.backToTop').hide(500);
                }
            });
            $('.backToTop').click(function () {
                $("html,body").animate({scrollTop: 0}, 1000);
            });
        });
        /* Back to top change collor on scroll */
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if (scroll >= 2500) {
                $(".fa-angle-double-up").addClass("orange");
            } else {
                $(".fa-angle-double-up").removeClass("orange");
            }
        });
    </script>
</body>
</html>
