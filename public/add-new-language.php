<?php
session_start();
if ($_POST['campaignId'] == "") {
    $_SESSION['createCampError'] = "You did not choose campaign";
    header("Location:../layout/createCampError.php");
}


?>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="css/create-campaign.css" />
        <link rel="stylesheet" type="text/css" href="css/add-new-language.css" />
        <title>Add New Language</title>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <div class="container">
            
            <form method="post" action="../public/create-campaign-logic.php" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 text-center languages">
                        <h3>Enter new language:</h3>
                        <select type="text" name="languageID" id="languages">
                            <option disabled>Choose new language for the selected campaign</option>
                        </select>
                    </div>
                    <div class="col-md-6 text-center">
                        <h3>Adding new language for campaign:</h3>
                        <h4><input type="text" name="campaignName" value="<?php echo $_POST['campaignName']; ?>" readonly></h4>
                        <input type="hidden" name="campaignId" value="<?php echo $_POST['campaignId']; ?>">
                        <input type="hidden" name="addNewLanguage" value="true">
                    </div>
                </div>
                <div class="row banner-upload">
                    <div class="col-md-6 text-center">
                        <h3>Choose banner size:</h3>
                        <select name="bannerSize[]" id="bannerSize">
                            <option>250x250</option>
                            <option>728x90</option>
                            <option>160x600</option>
                            <option>336x280</option>
                            <option>300x250</option>
                        </select>
                    </div>
                    <div class="col-md-6 text-center">
                        <h3>Upload banner:</h3>
                        <input type="file" accept="image/jpeg" name="banner[]" class="campaign-banner" id="campaignBanner" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#" class="btn btn-primary" id="addBanner">Add another banner</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>Upload mailer:</h3>
                        <input type="file" accept="text/html" name="mailer" id="campaignMailer">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>Upload landing page:</h3>
                        <input type="file" accept="text/html" name="landingPage" id="campaignLandingPage" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">CREATE NEW LANGUAGE</button>
                    </div>
                </div>
            </form>
            
        </div>
        <script src="../public/js/create-campaign.js"></script>
        <script src="../public/js/add-new-language.js"></script>
    </body>
</html>
