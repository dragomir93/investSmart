<?php
session_start();
include 'config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));

$partnersLinks = $client->GetPartnerLinks(array('token' => $_SESSION['TOKEN']))->GetPartnerLinksResult;

$data = $partnersLinks->DMTrackingLink;

echo json_encode($data);