<?php
include '../configs/config.inc.php';
$client = new SoapClient(URL);

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$reports = $client->GetCampaignsForLanguage(array("LanguageId" => $request))->GetCampaignsForLanguageResult;

echo json_encode($reports->Campaign);
