$(document).ready(function () {
    var l, content, space;
    for (l = 0, content = languagesDir.length, space = ""; l < content; l++) {
    var languageSelect = languagesDir[l];
    var i, len, text;
    for (i = 0, len = campaigns.length, text = ""; i < len; i++) {
//                    document.write(campaigns[i] + '<br>');
        //       var English = [{
//                            display: campaigns[0], value: campaigns[0]},
//                display: campaigns[i], value: campaigns[i]},
//                        {display: campaigns[1], value: campaigns[1]},
//                        {display: campaigns[2], value: campaigns[2]},
//        ];
        var folderContent = {
            display: campaigns[i], value: campaigns[i]
        };
        console.log(folderContent);
        languageSelect.push(folderContent);
        console.log(languageSelect);
    }}
    var German = [];
    for (i = 0, len = campaigns.length, text = ""; i < len; i++) {
        var folderContent = {
            display: campaigns[i], value: campaigns[i]
        };
        console.log(folderContent);
        German.push(folderContent);
        console.log(German);
    }

    var France = [];
    for (i = 0, len = campaigns.length, text = ""; i < len; i++) {
        var folderContent = {
            display: campaigns[i], value: campaigns[i]
        };
        console.log(folderContent);
        France.push(folderContent);
        console.log(France);
    }
    // Function executes on change of first select option field.
    $("#select-language").change(function () {
        var select = $("#select-language option:selected").val();
        switch (select) {
            case "English":
                campaign(English);
                break;
            case "German":
                campaign(German);
                break;
            case "France":
                campaign(France);
                break;
            default:
                $("#campaign").empty();
                $("#campaign").append("<option>--Select--</option>");
                break;
        }
    });
    // Function To List out Cities in Second Select tags
    function campaign(lang) {
        $("#campaign").empty(); //To reset cities
        $("#campaign").append("<option>--Select--</option>");
        $(lang).each(function (i) { //to list cities
            $("#campaign").append("<option value=\"" + lang[i].value + "\">" + lang[i].display + "</option>")
        });
    }
});

