function date(){
            var date = new Date();
            var daysOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
            var monthsOfYear = ["January","February","March","April","May","June","July","August","September","October","November","December"];
            var theDay = date.getDay();
            var theMonth = date.getMonth();
            var theDate = date.getDate();
            var theYear = date.getFullYear();
            var month = monthsOfYear[theMonth];
            var today = daysOfWeek[theDay];
            $('#date').html(today+" "+theDate+"/"+month+"/"+theYear);
        }