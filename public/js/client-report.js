function getClientReport(id) {
    $("#clientReportModal .modal-body tbody tr").remove();
    $.post({
        url: "../public/client-report.php",
        data: JSON.stringify(id),
        success: function(res) {
            var jsonRes = JSON.parse(res);
            if (jsonRes != undefined) {
                if (jsonRes.constructor != Array) {
                    jsonRes = Array(jsonRes);
                }
            }
            for(var client in jsonRes) {
                var dateTime = jsonRes[client].RegisteredDate;
                var fields = dateTime.split('T');
                var date = fields[0].split("-");
                date.reverse();
                var formattedDate = date.join('/');
                var time = fields[1];
                
                
                var tableRow = "<tr><td>"+jsonRes[client].FirstName+"</td><td>"+jsonRes[client].LastName+"</td><td>"+jsonRes[client].Email+"</td><td>"+jsonRes[client].Country+"</td><td>"+formattedDate+"</td><td>"+jsonRes[client].TradeAccounts+"</td></tr>";
                
                $("#clientReportModal .modal-body tbody").append(tableRow);
            }
        }
    });
    
}