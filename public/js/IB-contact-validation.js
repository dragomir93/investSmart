function validateForm(id) {
     if ($("#" + id).val() == null || $("#" + id).val() == "") {
        var div = $("#" + id).closest("div");
        div.removeClass("has-success");
        $("#glyp" + id).remove();
        div.addClass("has-error has-feedback");
        div.append('<span id="glyp' + id + '" style="margin-top:10px;" class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
        return false;
    }
    else {
        var div = $("#" + id).closest("div");
        div.removeClass("has-error");
        $("#glyp" + id).remove();
        div.addClass("has-success has-feedback");
        div.append('<span id="glyp' + id + '" style="margin-top:10px;" class="glyphicon glyphicon-ok form-control-feedback removeGlyph" aria-hidden="true"></span>');
        return true;
    }
}
$('#sendMail').click(function () {
    validateForm("subject");
    validateForm("message");
});
$('#subject').keyup(function(){
    validateForm("subject");
});
$('#message').keyup(function(){
    validateForm("message");
});

