function test(id){
    var website = document.getElementById('website').value;
    var phone = document.getElementById('phone-stable').value;
    
    if (id == "email") {
//        $('#email').keyup(function(){
        var mail = document.getElementById(id).value;
       // var emailFilter = /^([a-z]{3,})+@(([a-z])+\.)+([a-z]{2,10})+$/;
        var emailFilter = /^([a-z0-9]{0,})+(?:.[a-z0-9]{1,})+@([a-z]{1,}\.[a-z]{1,})+$/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.removeClass("has-error");
            return false;
        }
        else if (!emailFilter.test(mail)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
//        });
    }
     else if(id == "Fname"){
        var name = document.getElementById(id).value;
        var nameFilter = /^[A-Z][a-z]+/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else if (!nameFilter.test(name)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(id == "Lname"){
        var name = document.getElementById(id).value;
        var nameFilter = /^[A-Z][a-z]+/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else if (!nameFilter.test(name)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(id == "companyName"){
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(id == "noClients"){
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            div.addClass("has-success has-feedback");
            return true;
        }
    }
    else if(id == "passVal"){
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(id == "phone"){
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(id == "RepassVal"){
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(id == "CountryList"){
        if ($("." + id).val() == null || $("." + id).val() == "") {
            var div = $("." + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("." + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(id == "partnershipType"){
        if ($("." + id).val() == null || $("." + id).val() == "") {
            var div = $("." + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("." + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    else if(website !=""){
        if (id=="website"){
            var web = document.getElementById(id).value;
        if (web == null || web == "") {
            
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
        }
    }
    else if(phone !=""){
        if (id=="phone-stable"){
        var tel = document.getElementById(id).value;
        var phoneFilter = /\+\(\d{1,4}\)\d{8,}/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else if (!phoneFilter.test(tel)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            div.addClass("has-error has-feedback");
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            return true;
        }
    }
    }
    
}
$('#submit').click(function(){
    test("email");
    test("Fname");
    test("Lname");
    test("companyName");
    test("noClients");
    test("passVal");
    test("phone");
    test("RepassVal");
    test("CountryList");
    test("partnershipType");
    test("website");
    test("phone-stable");
});

$(document).ready(function(){
    $('#email').keyup(function(){
    test("email");
    });
    $('#Fname').keyup(function(){
    test("Fname");
    });
    $('#Lname').keyup(function(){
    test("Lname");
    });
    $('#companyName').keyup(function(){
    test("companyName");
    });
    $('#noClients').keyup(function(){
    test("noClients");
    });
    $('#passVal').keyup(function(){
    test("passVal");
    });
    $('#phone').keyup(function(){
    test("phone");
    });
    $('#RepassVal').keyup(function(){
    test("RepassVal");
    });
    $('#CountryList').keyup(function(){
    test("CountryList");
    });
    $('#partnershipType').keyup(function(){
    test("partnershipType");
    });
    $('#website').keyup(function(){
    test("website");
    });
    $('#phone-stable').keyup(function(){
    test("phone-stable");
    });
});

var pass = document.getElementById('passVal');
var rePass = document.getElementById('RepassVal');
function validatePassword(){
  if(pass.value != rePass.value) {
    rePass.setCustomValidity("Passwords Don't Match");
  } else {
    rePass.setCustomValidity('');
  }
}
pass.onchange = validatePassword;
rePass.onkeyup = validatePassword;