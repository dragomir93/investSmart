$(document).ready(function() {
    
    var campaign = $("#campaign").val();
    var language = $("#language").val();
    $(".banners-list p").remove();
    $(".mailers-list p").remove();
    $(".lp-list p").remove();
    
    var data = {
        "campaignName": campaign,
        "language": language
    }
    
    $.post({
            url: "../public/get-campaign-links.php",
            data: JSON.stringify(data),
            success: function(res) {
                var jsonData = JSON.parse(res);
                for (var banner in jsonData.banners) {
                    var bannerList = "<p id='"+banner+"'>"+jsonData.banners[banner]+"<a href='#' class='removeBanner' id='button_"+banner+"'>Remove</a></p>";
                    $(".banners-list").append(bannerList);
                }
                
                if (jsonData.mailer != undefined) {
                    $(".mailers-list").append("<p>Yes</p>");
                } else {
                    $(".mailers-list").append("<p>No</p>");
                }
                
                if (jsonData.lp != undefined) {
                    $(".lp-list").append("<p>Yes</p>");
                } else {
                    $(".lp-list").append("<p>No</p>");
                }
            }
        });
    
    $(document).on("click", ".removeBanner", function() {
    
        var id = $(this).attr('id');
        var arrId = id.split("_");
        var buttonId = arrId[1];
        
        var banner = $("#"+buttonId).text();
        var bannerArr = banner.split(".");
        var bannerToDelete = bannerArr[0];
        
        var deleteBannerData = {
            "campaign": campaign,
            "language": language,
            "bannerToDelete": bannerToDelete
        }
        $.post({
            url: "../public/delete-banner.php",
            data: JSON.stringify(deleteBannerData),
            success: function(res) {
                var jsonRes = JSON.parse(res);
                console.log(jsonRes);
                if (jsonRes == true) {
                    $("#"+buttonId).remove();
                } else {
                    $("#errorMessage").remove();
                    $(".banners-list").append("<p id='errorMessage'>Error deleting banner or minimum banners reached. Please refresh and try again.</p>");
                }
                
            }
            
        });
    });
});