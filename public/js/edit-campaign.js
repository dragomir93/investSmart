$(document).ready(function() {
    
    $("#editCampaignBtn").click(function() {
        
        $.get("../public/campaign-list.php", function(res) {
            
            var jsonCampaignes = JSON.parse(res);
            $("#listCamp option:not(:first-child)").remove();
            if(jsonCampaignes.constructor != Array) {
                jsonCampaignes = Array(jsonCampaignes);
            }
            for (var campaign in jsonCampaignes) {
                var campOption = "<option value='"+jsonCampaignes[campaign].Id+"'>"+jsonCampaignes[campaign].Name+"</option>";
                $("#listCamp").append(campOption);
            }
        });
    });
    
    $("#listCamp").change(function() {
        $("#campaignLanguages option").remove();
        $("#campaignLanguages").append("<option disabled selected value='0'>Choose language for selected campaign</option>");
        $("#editCampaignForm #campaignName").remove();
        var name = $("#listCamp option:selected").text();
        var campaignID = $("#listCamp option:selected").val();
        $(".banners-list p:not(:first-child)").remove();
        $(".mailers-list p:not(:first-child)").remove();
        $(".lp-list p:not(:first-child)").remove();
        $(".edit-list a").remove();
        var data = {
            "campaignID": campaignID,
            "campaignName": name
        }
        
        var input = "<input type='hidden' name='campaignName' id='campaignName' value='"+name+"'>";
        $("#editCampaignForm").append(input);
        
        $.post({
            url: "../public/get-campaign-languages.php",
            data: JSON.stringify(campaignID),
            success: function(res) {
                
                var jsonLanguages = JSON.parse(res);
                
                if (jsonLanguages.constructor != Array) {
                    jsonLanguages = Array(jsonLanguages); 
                }
                
                $("#campaignLanguages option:not(:first-child)").remove();
                for (var lang in jsonLanguages) {
                    
                    var languageOption = "<option value='"+jsonLanguages[lang].Id+"'>"+jsonLanguages[lang].Name+"</option>";
                    $("#campaignLanguages").append(languageOption);
                }
                
            }
        });
        
    });
    
    
    $("#campaignLanguages").change(function() {
        
        var name = $("#listCamp option:selected").text();
        var campaignID = $("#listCamp option:selected").val();
        var language = $("#campaignLanguages option:selected").text();
        $(".banners-list p:not(:first-child)").remove();
        $(".mailers-list p:not(:first-child)").remove();
        $(".lp-list p:not(:first-child)").remove();
        $(".edit-list a").remove();
        var data = {
            "campaignID": campaignID,
            "campaignName": name,
            "language": language
        }
        
        $.post({
            url: "../public/get-campaign-links.php",
            data: JSON.stringify(data),
            success: function(res) {
                var jsonData = JSON.parse(res);
                
                for (var banner in jsonData.banners) {
                    var bannerList = "<p>"+jsonData.banners[banner]+"</p>";
                    $(".banners-list").append(bannerList);
                }
                
                if (jsonData.mailer != undefined) {
                    $(".mailers-list").append("<p>Yes</p>");
                } else {
                    $(".mailers-list").append("<p>No</p>");
                }
                
                if (jsonData.lp != undefined) {
                    $(".lp-list").append("<p>Yes</p>");
                } else {
                    $(".lp-list").append("<p>No</p>");
                }
                
                var editLanguageLink = "<a href='../public/edit-campaign-language.php?campaign="+name+"&language="+language+"' class='btn btn-primary'>Edit this language</a>";
                $(".edit-list").append(editLanguageLink);
            }
        });
        
    });
    
    $("#deleteCampaign").click(function() {
       
        var campID = $("#listCamp option:selected").val();
        var campName = $("#listCamp option:selected").text();
        
        if (campID == "0") {
            alert("PLEASE SELECT CAMPAIGN YOU WANT TO DELETE");
        } else {
            var confirmDelete = confirm("Are you sure you want to delete "+campName+" campaign?");
        }
        
        if(confirmDelete) {
            var data = {
                "campID": campID,
                "campName": campName
            }
            $.post({
                url: '../public/delete-campaign.php',
                data: JSON.stringify(data),
                success: function(res) {
                    var jsonRes = JSON.parse(res);
                    
                    if (jsonRes.Success) {
                        alert("Campaign "+campName+" deleted successfully! Page will now refresh!");
                        location.reload();
                    } else {
                        alert(jsonRes.Message);
                    }
                }
            });
        }
        
        
    });
    
    $("#deleteLanguage").click(function() {
        
        var langID = $("#campaignLanguages option:selected").val();
        var langName = $("#campaignLanguages option:selected").text();
        var campID = $("#listCamp option:selected").val();
        var campName = $("#listCamp option:selected").text();
        
        if(langID == "0") {
            alert("PLEASE, CHOOSE LANGUAGE FIRST!");
        } else {
            var confirmDelete = confirm("Are you sure you want to delete "+langName+" language for the "+campName+" ?");
        }
        
        if(confirmDelete) {
            var data = {
                "langID": langID,
                "langName": langName,
                "campID": campID,
                "campName": campName
            }
            
            $.post({
                url: "../public/delete-language.php",
                data: JSON.stringify(data),
                success: function(res) {
                    var jsonRes = JSON.parse(res);
                    console.log(res);
                    if (jsonRes.Success) {
                        alert("Language "+langName+" for "+campName+" campaign deleted successfully! Page will now refresh!");
                        location.reload();
                    } else {
                        alert(jsonRes.Message);
                    }
                }
            });
        }
        
    });
    
});
