$(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 500) {
            $('.backToTop').show(500);
        } else {
            $('.backToTop').hide(500);
        }
    });
    $('.backToTop').click(function () {
        $("html,body").animate({scrollTop: 0}, 1000);
    });
});
