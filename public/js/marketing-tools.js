$(document).ready(function() {
    
    var campaignId;
    var campaignText;
    var language;
    
    $.get("./language-options.php", function(res) {
        
        var jsonRes = JSON.parse(res);
        
        for(var i=0; i<jsonRes.length; i++) {
            
            var langName = jsonRes[i].name;
            var langId = jsonRes[i].id;
            
            var option = "<option value='"+langId+"'>"+langName+"</option>";
            $("#selectLanguage").append(option);
        }
    });
    
    $("#selectLanguage").change(function() {
        
        $("#banner-preview img").remove();
        $("#mailer-preview iframe").remove();
        $("#lp-preview iframe").remove();
        $(".marketing-tools-thumbnails, .generate-links-button").css("display","none");
        $(".banner-links, .mailer-links, .lp-links").css("display", "none");
        
        var campaignLang = $("#selectLanguage option:selected").text();
        var languageId = $("#selectLanguage option:selected").val();
        
        $("#selectCampaign option:not(:first-child)").remove();
        $("#selectCampaign option:first-child").attr("selected", true);
        
        $.post({
            url: "./campaign-options.php",
            data: languageId,
            success: function(res) {
                var jsonCampaignes = JSON.parse(res);
                if(jsonCampaignes.constructor != Array) {
                    jsonCampaignes = Array(jsonCampaignes);
                }
                for(var j=0; j<jsonCampaignes.length; j++) {
                    var campaignOptionText = jsonCampaignes[j].name;
                    var campaignOptionId = jsonCampaignes[j].id;
                    var campaignOption = "<option value='"+campaignOptionId+"'>"+campaignOptionText+"</option>";
                    $("#selectCampaign").append(campaignOption);
                }
            }
        });
        
        $(".select-campaign").css("display","block");
        
       
        language = campaignLang;
    });
    
    $("#selectCampaign").change(function() {
        
        $(".banner-links, .mailer-links, .lp-links").css("display", "none");
        
        clickID = $("#selectCampaign").val();
        campaignId = clickID;
        
        textID = $("#selectCampaign option:selected").text();
        campaignText = textID;
        

        var data = {
            "lang": language,
            "camp": campaignText
        };
        
        var sendData = JSON.stringify(data);

        
        $.post({
            url: "./obrada.php",
            data: sendData,
            success: function(res) {
                var resJson = JSON.parse(res);

                var banners = resJson.banners;
                var mailers = resJson.mailers;
                $("#bannerSize option").remove();
                for (var k=0; k<banners.length; k++) {
                    if (k==0 || k==1) {
                        continue;
                    } else {
                        var bannerSize = "<option>"+banners[k]+"</option>";
                        $("#bannerSize").append(bannerSize);
                    }
                }
                
                if (mailers == true) {
                    $("#mailer-preview").removeAttr("hidden");
                } else {
                    $("#mailer-preview").attr("hidden", "true");
                }
                var bannerImgURL = "<img src='Campaigns/" + campaignText + "/" + language + "/Banners/"+banners[2]+"' alt='" + campaignText + " banner'>";
                var mailerImgURL = "<iframe src='Campaigns/" + campaignText + "/" + language + "/Mailer/index.html'></iframe>";
                var lpImgURL = "<iframe src='Campaigns/" + campaignText + "/" + language + "/LP/index.html'></iframe>";
                
                $("#banner-preview img").remove();
                $("#mailer-preview iframe").remove();
                $("#lp-preview iframe").remove();
                $("#banner-img").append(bannerImgURL);
                $("#mailer-img").append(mailerImgURL);
                $("#lp-img").append(lpImgURL);
                $(".marketing-tools-thumbnails, .generate-links-button").css("display","block");

            }
            
        });
        
    });
    
    $("#bannerSize").change(function() {
        
        $(".banner-links, .mailer-links, .lp-links").css("display", "none");
        $("#bannerClient").val("");
        $("#bannerSub").val("");
        $("#lpLink").val("");
        
        var sizeValue = $("#bannerSize").val();
        var bannerImgURL = "<img src='Campaigns/" + campaignText + "/" + language + "/Banners/"+sizeValue+"' alt='" + campaignText + " banner'>";
        $("#banner-img img").remove();
        $("#banner-img").append(bannerImgURL);
        
    });
    
    $("#generateLinks").click(function() {
        
        $("#loading").css("display", "block");
        
        var chosenLang = $("#selectLanguage option:selected").text();
        var chosenCamp = $("#selectCampaign option:selected").text();
        var chosenBannerSize = $("#bannerSize option:selected").text();
        
        var generateLinks = {
            "language": chosenLang,
            "campID": clickID,
            "campaignName": chosenCamp,
            "bannerSize": chosenBannerSize
        }
        
        var sendGenerateLinks = JSON.stringify(generateLinks);
        
        $.post({
            url: "./generate-links.php",
            data: sendGenerateLinks,
            success: function(res) {
                
                var linksJson = JSON.parse(res);
                if (linksJson.mailer == true) {
                    $(".mailer-links").removeAttr("hidden");
                }
                $("#bannerClient").val("");
                $("#bannerSub").val("");
                $("#lpLink").val("");
                $("#bannerClient").val(linksJson.bannerLink);
                $("#bannerSub").val(linksJson.bannerSub);
                $("#lpLink").val(linksJson.lp);
                
                $("#downloadLP a").remove();
                $("#downloadMailer a").remove();
                $("#downloadSubMailer a").remove();
                var downloadLP = "<a href='"+linksJson.lpDownload+"' download>DOWNLOAD LANDING PAGE</a>";
                var downloadMailer = "<a href='"+linksJson.mailerDownload+"' download>DOWNLOAD MAILER FOR CLIENTS</a>";
                var downloadSubMailer = "<a href='"+linksJson.mailerSub+"' download>DOWNLOAD MAILER FOR SUBAFFILIATES</a>";
                if (linksJson.mailer == true) {
                    $("#downloadMailer").append(downloadMailer);
                    $("#downloadSubMailer").append(downloadSubMailer);   
                }
                $("#downloadLP").append(downloadLP);
                $(".banner-links, .mailer-links, .lp-links").css("display", "block");
                $("#loading").css("display", "none");
            }
        });
    });
    
    $("#linkHistoryTab").click(function() {
        
        $.get("./link-history.php", function(res) {
            
            $("#linkHistoryTable tr").remove();
            var jsonLinkHistory = JSON.parse(res);
            
            for (var item in jsonLinkHistory) {
                var count = item;
                count++;
                var tableRow = jsonLinkHistory[item];
                
                if (tableRow.type == 0) {
                    tableRow.type = "Not defined";
                } else if (tableRow.type == 1) {
                    tableRow.type = "Banner";
                } else {
                    tableRow.type = "Landing Page";
                }
                var textAreaHtml = "<textarea>"+tableRow.link+"</textarea>";
                var tableHtml = "<tr><td>"+count+"</td><td>"+tableRow.campaign+"</td><td>"+tableRow.language+"</td><td>"+textAreaHtml+"</td><td>"+tableRow.type+"</td></tr>";
                $("#linkHistoryTable").append(tableHtml);
            }
            
        });
        
    });
    
});