//$(function() {
//    var owl = $('.owl-carousel'),
//        owlOptions = {
//            loop: false,
//            margin: 10,
//            responsive: {
//                0: {
//                    items: 2
//                }
//            }
//        };
//
//    if ( $(window).width() < 854 ) {
//        var owlActive = owl.owlCarousel(owlOptions);
//    } else {
//        owl.addClass('off');
//    }
//
//    $(window).resize(function() {
//        if ( $(window).width() < 854 ) {
//            if ( $('.owl-carousel').hasClass('off') ) {
//                var owlActive = owl.owlCarousel(owlOptions);
//                owl.removeClass('off');
//            }
//        } else {
//            if ( !$('.owl-carousel').hasClass('off') ) {
//                owl.addClass('off').trigger('destroy.owl.carousel');
//                owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
//            }
//        }
//    });
//});
//$(".view2").owlCarousel({
//    autoPlay: false
//});
$("#owl-demo").owlCarousel({
//    autoPlay: 10000, //Set AutoPlay to 10 seconds
    items: 1,
    navigation: false,
    stopOnHover: true,
    responsiveClass: true,
    autoPlay: false,
    rewindNav: true,
    rewindSpeed: 0,
    mouseDrag: true
});
$(window).resize(function () {
    if ($(window).width() < 1257) {
//        $('#owl-demo').owlCarousel({
//            autoPlay: false,
//            touchDrag: false,
//            mouseDrag: false
//        });
        $('.registration-form').attr('hidden');
        $('.registration-form-second').removeAttr('hidden');

    } else {
//        $("#owl-demo").owlCarousel({
//            autoPlay: false, //Set AutoPlay to 10 seconds
//            items: 1,
//            navigation: false,
//            stopOnHover: true,
//            responsiveClass: true
//        });
        $('.registration-form').removeAttr('hidden');
        $('.registration-form-second').attr('hidden');
    }
});

/*Animation Boxes */
$("figure").hover(function () {
    $(this).stop(true, false).animate({height: "200px"});
}, function () {
    $(this).stop(true, false).animate({height: "200px"});
});
/* Accordion */
jQuery(document).ready(function () {
    function close_accordion_section() {
        jQuery('.accordion .accordion-section-title').removeClass('active');
        jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    jQuery('.accordion-section-title').click(function (e) {
// Grab current anchor value
        var currentAttrValue = jQuery(this).attr('href');
        if (jQuery(e.target).is('.active')) {
            close_accordion_section();
        } else {
            close_accordion_section();
            // Add active class to section title
            jQuery(this).addClass('active');
            // Open up the hidden content panel
            jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
        }

        e.preventDefault();
    });
});
/* excanvas */
document.createElement("canvas").getContext || function () {
    function a() {
        return this.context_ || (this.context_ = new S(this))
    }
    function l(e, t) {
        var n = f.call(arguments, 2);
        return function () {
            return e.apply(t, n.concat(f.call(arguments)))
        }
    }
    function h(e) {
        var t = e.srcElement;
        switch (e.propertyName) {
            case"width":
                t.style.width = t.attributes.width.nodeValue + "px";
                t.getContext().clearRect();
                break;
            case"height":
                t.style.height = t.attributes.height.nodeValue + "px";
                t.getContext().clearRect();
                break
        }
    }
    function p(e) {
        var t = e.srcElement;
        if (t.firstChild) {
            t.firstChild.style.width = t.clientWidth + "px";
            t.firstChild.style.height = t.clientHeight + "px"
        }
    }
    function g() {
        return[[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    }
    function y(e, t) {
        var n = g(), r = 0;
        for (; r < 3; r++) {
            var i = 0;
            for (; i < 3; i++) {
                var s = 0, o = 0;
                for (; o < 3; o++)
                    s += e[r][o] * t[o][i];
                n[r][i] = s
            }
        }
        return n
    }
    function b(e, t) {
        t.fillStyle = e.fillStyle;
        t.lineCap = e.lineCap;
        t.lineJoin = e.lineJoin;
        t.lineWidth = e.lineWidth;
        t.miterLimit = e.miterLimit;
        t.shadowBlur = e.shadowBlur;
        t.shadowColor = e.shadowColor;
        t.shadowOffsetX = e.shadowOffsetX;
        t.shadowOffsetY = e.shadowOffsetY;
        t.strokeStyle = e.strokeStyle;
        t.globalAlpha = e.globalAlpha;
        t.arcScaleX_ = e.arcScaleX_;
        t.arcScaleY_ = e.arcScaleY_;
        t.lineScale_ = e.lineScale_
    }
    function w(e) {
        var t, n = 1;
        e = String(e);
        if (e.substring(0, 3) == "rgb") {
            var r = e.indexOf("(", 3), i = e.indexOf(")", r + 1), s = e.substring(r + 1, i).split(",");
            t = "#";
            var o = 0;
            for (; o < 3; o++)
                t += d[Number(s[o])];
            if (s.length == 4 && e.substr(3, 1) == "a")
                n = s[3]
        } else
            t = e;
        return{color: t, alpha: n}
    }
    function E(e) {
        switch (e) {
            case"butt":
                return"flat";
            case"round":
                return"round";
            case"square":
            default:
                return"square"
        }
    }
    function S(e) {
        this.m_ = g();
        this.mStack_ = [];
        this.aStack_ = [];
        this.currentPath_ = [];
        this.fillStyle = this.strokeStyle = "#000";
        this.lineWidth = 1;
        this.lineJoin = "miter";
        this.lineCap = "butt";
        this.miterLimit = o * 1;
        this.globalAlpha = 1;
        this.canvas = e;
        var t = e.ownerDocument.createElement("div");
        t.style.width = e.clientWidth + "px";
        t.style.height = e.clientHeight + "px";
        t.style.overflow = "hidden";
        t.style.position = "absolute";
        e.appendChild(t);
        this.element_ = t;
        this.lineScale_ = this.arcScaleY_ = this.arcScaleX_ = 1
    }
    function T(e, t, n, r) {
        e.currentPath_.push({type: "bezierCurveTo", cp1x: t.x, cp1y: t.y, cp2x: n.x, cp2y: n.y, x: r.x, y: r.y});
        e.currentX_ = r.x;
        e.currentY_ = r.y
    }
    function N(e) {
        var t = 0;
        for (; t < 3; t++) {
            var n = 0;
            for (; n < 2; n++)
                if (!isFinite(e[t][n]) || isNaN(e[t][n]))
                    return false
        }
        return true
    }
    function C(e, t, n) {
        if (!!N(t)) {
            e.m_ = t;
            if (n)
                e.lineScale_ = s(i(t[0][0] * t[1][1] - t[0][1] * t[1][0]))
        }
    }
    function k(e) {
        this.type_ = e;
        this.r1_ = this.y1_ = this.x1_ = this.r0_ = this.y0_ = this.x0_ = 0;
        this.colors_ = []
    }
    function L() {}
    var e = Math, t = e.round, n = e.sin, r = e.cos, i = e.abs, s = e.sqrt, o = 10, u = o / 2;
    var f = Array.prototype.slice;
    var c = {init: function (e) {
            if (/MSIE/.test(navigator.userAgent) && !window.opera) {
                var t = e || document;
                t.createElement("canvas");
                t.attachEvent("onreadystatechange", l(this.init_, this, t))
            }
        }, init_: function (e) {
            e.namespaces.g_vml_ || e.namespaces.add("g_vml_", "urn:schemas-microsoft-com:vml", "#default#VML");
            e.namespaces.g_o_ || e.namespaces.add("g_o_", "urn:schemas-microsoft-com:office:office", "#default#VML");
            if (!e.styleSheets.ex_canvas_) {
                var t = e.createStyleSheet();
                t.owningElement.id = "ex_canvas_";
                t.cssText = "canvas{display:inline-block;overflow:hidden;text-align:left;width:300px;height:150px}g_vml_\\:*{behavior:url(#default#VML)}g_o_\\:*{behavior:url(#default#VML)}"
            }
            var n = e.getElementsByTagName("canvas"), r = 0;
            for (; r < n.length; r++)
                this.initElement(n[r])
        }, initElement: function (e) {
            if (!e.getContext) {
                e.getContext = a;
                e.innerHTML = "";
                e.attachEvent("onpropertychange", h);
                e.attachEvent("onresize", p);
                var t = e.attributes;
                if (t.width && t.width.specified)
                    e.style.width = t.width.nodeValue + "px";
                else
                    e.width = e.clientWidth;
                if (t.height && t.height.specified)
                    e.style.height = t.height.nodeValue + "px";
                else
                    e.height = e.clientHeight
            }
            return e
        }};
    c.init();
    var d = [], v = 0;
    for (; v < 16; v++) {
        var m = 0;
        for (; m < 16; m++)
            d[v * 16 + m] = v.toString(16) + m.toString(16)
    }
    var x = S.prototype;
    x.clearRect = function () {
        this.element_.innerHTML = ""
    };
    x.beginPath = function () {
        this.currentPath_ = []
    };
    x.moveTo = function (e, t) {
        var n = this.getCoords_(e, t);
        this.currentPath_.push({type: "moveTo", x: n.x, y: n.y});
        this.currentX_ = n.x;
        this.currentY_ = n.y
    };
    x.lineTo = function (e, t) {
        var n = this.getCoords_(e, t);
        this.currentPath_.push({type: "lineTo", x: n.x, y: n.y});
        this.currentX_ = n.x;
        this.currentY_ = n.y
    };
    x.bezierCurveTo = function (e, t, n, r, i, s) {
        var o = this.getCoords_(i, s), u = this.getCoords_(e, t), a = this.getCoords_(n, r);
        T(this, u, a, o)
    };
    x.quadraticCurveTo = function (e, t, n, r) {
        var i = this.getCoords_(e, t), s = this.getCoords_(n, r), o = {x: this.currentX_ + .6666666666666666 * (i.x - this.currentX_), y: this.currentY_ + .6666666666666666 * (i.y - this.currentY_)};
        T(this, o, {x: o.x + (s.x - this.currentX_) / 3, y: o.y + (s.y - this.currentY_) / 3}, s)
    };
    x.arc = function (e, t, i, s, a, f) {
        i *= o;
        var l = f ? "at" : "wa", c = e + r(s) * i - u, h = t + n(s) * i - u, p = e + r(a) * i - u, d = t + n(a) * i - u;
        if (c == p && !f)
            c += .125;
        var v = this.getCoords_(e, t), m = this.getCoords_(c, h), g = this.getCoords_(p, d);
        this.currentPath_.push({type: l, x: v.x, y: v.y, radius: i, xStart: m.x, yStart: m.y, xEnd: g.x, yEnd: g.y})
    };
    x.rect = function (e, t, n, r) {
        this.moveTo(e, t);
        this.lineTo(e + n, t);
        this.lineTo(e + n, t + r);
        this.lineTo(e, t + r);
        this.closePath()
    };
    x.strokeRect = function (e, t, n, r) {
        var i = this.currentPath_;
        this.beginPath();
        this.moveTo(e, t);
        this.lineTo(e + n, t);
        this.lineTo(e + n, t + r);
        this.lineTo(e, t + r);
        this.closePath();
        this.stroke();
        this.currentPath_ = i
    };
    x.fillRect = function (e, t, n, r) {
        var i = this.currentPath_;
        this.beginPath();
        this.moveTo(e, t);
        this.lineTo(e + n, t);
        this.lineTo(e + n, t + r);
        this.lineTo(e, t + r);
        this.closePath();
        this.fill();
        this.currentPath_ = i
    };
    x.createLinearGradient = function (e, t, n, r) {
        var i = new k("gradient");
        i.x0_ = e;
        i.y0_ = t;
        i.x1_ = n;
        i.y1_ = r;
        return i
    };
    x.createRadialGradient = function (e, t, n, r, i, s) {
        var o = new k("gradientradial");
        o.x0_ = e;
        o.y0_ = t;
        o.r0_ = n;
        o.x1_ = r;
        o.y1_ = i;
        o.r1_ = s;
        return o
    };
    x.drawImage = function (n) {
        var r, i, s, u, a, f, l, c, h = n.runtimeStyle.width, p = n.runtimeStyle.height;
        n.runtimeStyle.width = "auto";
        n.runtimeStyle.height = "auto";
        var d = n.width, v = n.height;
        n.runtimeStyle.width = h;
        n.runtimeStyle.height = p;
        if (arguments.length == 3) {
            r = arguments[1];
            i = arguments[2];
            a = f = 0;
            l = s = d;
            c = u = v
        } else if (arguments.length == 5) {
            r = arguments[1];
            i = arguments[2];
            s = arguments[3];
            u = arguments[4];
            a = f = 0;
            l = d;
            c = v
        } else if (arguments.length == 9) {
            a = arguments[1];
            f = arguments[2];
            l = arguments[3];
            c = arguments[4];
            r = arguments[5];
            i = arguments[6];
            s = arguments[7];
            u = arguments[8]
        } else
            throw Error("Invalid number of arguments");
        var m = this.getCoords_(r, i), g = [];
        g.push(" <g_vml_:group", ' coordsize="', o * 10, ",", o * 10, '"', ' coordorigin="0,0"', ' style="width:', 10, "px;height:", 10, "px;position:absolute;");
        if (this.m_[0][0] != 1 || this.m_[0][1]) {
            var y = [];
            y.push("M11=", this.m_[0][0], ",", "M12=", this.m_[1][0], ",", "M21=", this.m_[0][1], ",", "M22=", this.m_[1][1], ",", "Dx=", t(m.x / o), ",", "Dy=", t(m.y / o), "");
            var b = m, w = this.getCoords_(r + s, i), E = this.getCoords_(r, i + u), S = this.getCoords_(r + s, i + u);
            b.x = e.max(b.x, w.x, E.x, S.x);
            b.y = e.max(b.y, w.y, E.y, S.y);
            g.push("padding:0 ", t(b.x / o), "px ", t(b.y / o), "px 0;filter:progid:DXImageTransform.Microsoft.Matrix(", y.join(""), ", sizingmethod='clip');")
        } else
            g.push("top:", t(m.y / o), "px;left:", t(m.x / o), "px;");
        g.push(' ">', '<g_vml_:image src="', n.src, '"', ' style="width:', o * s, "px;", " height:", o * u, 'px;"', ' cropleft="', a / d, '"', ' croptop="', f / v, '"', ' cropright="', (d - a - l) / d, '"', ' cropbottom="', (v - f - c) / v, '"', " />", "</g_vml_:group>");
        this.element_.insertAdjacentHTML("BeforeEnd", g.join(""))
    };
    x.stroke = function (n) {
        var r = [], i = w(n ? this.fillStyle : this.strokeStyle), s = i.color, u = i.alpha * this.globalAlpha;
        r.push("<g_vml_:shape", ' filled="', !!n, '"', ' style="position:absolute;width:', 10, "px;height:", 10, 'px;"', ' coordorigin="0 0" coordsize="', o * 10, " ", o * 10, '"', ' stroked="', !n, '"', ' path="');
        var a = {x: null, y: null}, f = {x: null, y: null}, l = 0;
        for (; l < this.currentPath_.length; l++) {
            var c = this.currentPath_[l];
            switch (c.type) {
                case"moveTo":
                    r.push(" m ", t(c.x), ",", t(c.y));
                    break;
                case"lineTo":
                    r.push(" l ", t(c.x), ",", t(c.y));
                    break;
                case"close":
                    r.push(" x ");
                    c = null;
                    break;
                case"bezierCurveTo":
                    r.push(" c ", t(c.cp1x), ",", t(c.cp1y), ",", t(c.cp2x), ",", t(c.cp2y), ",", t(c.x), ",", t(c.y));
                    break;
                case"at":
                case"wa":
                    r.push(" ", c.type, " ", t(c.x - this.arcScaleX_ * c.radius), ",", t(c.y - this.arcScaleY_ * c.radius), " ", t(c.x + this.arcScaleX_ * c.radius), ",", t(c.y + this.arcScaleY_ * c.radius), " ", t(c.xStart), ",", t(c.yStart), " ", t(c.xEnd), ",", t(c.yEnd));
                    break
            }
            if (c) {
                if (a.x == null || c.x < a.x)
                    a.x = c.x;
                if (f.x == null || c.x > f.x)
                    f.x = c.x;
                if (a.y == null || c.y < a.y)
                    a.y = c.y;
                if (f.y == null || c.y > f.y)
                    f.y = c.y
            }
        }
        r.push(' ">');
        if (n)
            if (typeof this.fillStyle == "object") {
                var h = this.fillStyle, p = 0, d = {x: 0, y: 0}, v = 0, m = 1;
                if (h.type_ == "gradient") {
                    var g = h.x1_ / this.arcScaleX_, y = h.y1_ / this.arcScaleY_, b = this.getCoords_(h.x0_ / this.arcScaleX_, h.y0_ / this.arcScaleY_), S = this.getCoords_(g, y);
                    p = Math.atan2(S.x - b.x, S.y - b.y) * 180 / Math.PI;
                    if (p < 0)
                        p += 360;
                    if (p < 1e-6)
                        p = 0
                } else {
                    var b = this.getCoords_(h.x0_, h.y0_), x = f.x - a.x, T = f.y - a.y;
                    d = {x: (b.x - a.x) / x, y: (b.y - a.y) / T};
                    x /= this.arcScaleX_ * o;
                    T /= this.arcScaleY_ * o;
                    var N = e.max(x, T);
                    v = 2 * h.r0_ / N;
                    m = 2 * h.r1_ / N - v
                }
                var C = h.colors_;
                C.sort(function (e, t) {
                    return e.offset - t.offset
                });
                var k = C.length, L = C[0].color, A = C[k - 1].color, O = C[0].alpha * this.globalAlpha, M = C[k - 1].alpha * this.globalAlpha, _ = [], l = 0;
                for (; l < k; l++) {
                    var D = C[l];
                    _.push(D.offset * m + v + " " + D.color)
                }
                r.push('<g_vml_:fill type="', h.type_, '"', ' method="none" focus="100%"', ' color="', L, '"', ' color2="', A, '"', ' colors="', _.join(","), '"', ' opacity="', M, '"', ' g_o_:opacity2="', O, '"', ' angle="', p, '"', ' focusposition="', d.x, ",", d.y, '" />')
            } else
                r.push('<g_vml_:fill color="', s, '" opacity="', u, '" />');
        else {
            var H = this.lineScale_ * this.lineWidth;
            if (H < 1)
                u *= H;
            r.push("<g_vml_:stroke", ' opacity="', u, '"', ' joinstyle="', this.lineJoin, '"', ' miterlimit="', this.miterLimit, '"', ' endcap="', E(this.lineCap), '"', ' weight="', H, 'px"', ' color="', s, '" />')
        }
        r.push("</g_vml_:shape>");
        this.element_.insertAdjacentHTML("beforeEnd", r.join(""))
    };
    x.fill = function () {
        this.stroke(true)
    };
    x.closePath = function () {
        this.currentPath_.push({type: "close"})
    };
    x.getCoords_ = function (e, t) {
        var n = this.m_;
        return{x: o * (e * n[0][0] + t * n[1][0] + n[2][0]) - u, y: o * (e * n[0][1] + t * n[1][1] + n[2][1]) - u}
    };
    x.save = function () {
        var e = {};
        b(this, e);
        this.aStack_.push(e);
        this.mStack_.push(this.m_);
        this.m_ = y(g(), this.m_)
    };
    x.restore = function () {
        b(this.aStack_.pop(), this);
        this.m_ = this.mStack_.pop()
    };
    x.translate = function (e, t) {
        C(this, y([[1, 0, 0], [0, 1, 0], [e, t, 1]], this.m_), false)
    };
    x.rotate = function (e) {
        var t = r(e), i = n(e);
        C(this, y([[t, i, 0], [-i, t, 0], [0, 0, 1]], this.m_), false)
    };
    x.scale = function (e, t) {
        this.arcScaleX_ *= e;
        this.arcScaleY_ *= t;
        C(this, y([[e, 0, 0], [0, t, 0], [0, 0, 1]], this.m_), true)
    };
    x.transform = function (e, t, n, r, i, s) {
        C(this, y([[e, t, 0], [n, r, 0], [i, s, 1]], this.m_), true)
    };
    x.setTransform = function (e, t, n, r, i, s) {
        C(this, [[e, t, 0], [n, r, 0], [i, s, 1]], true)
    };
    x.clip = function () {};
    x.arcTo = function () {};
    x.createPattern = function () {
        return new L
    };
    k.prototype.addColorStop = function (e, t) {
        t = w(t);
        this.colors_.push({offset: e, color: t.color, alpha: t.alpha})
    };
    G_vmlCanvasManager = c;
    CanvasRenderingContext2D = S;
    CanvasGradient = k;
    CanvasPattern = L
}()

/* icapthca */
/*!
 * Internet Captcha
 * version: 2 (20 Mqy 2013)
 * Examples at http://www.icaptcha.com
 * License: CREATIVE COMMONS Attribution-NonCommercial 3.0 Unported
 * Copyright 2013 David Arroni Castellanos
 */

$("#submit").attr('disabled', true);
//$("#captchaStatusFalse").attr('hidden',true);
$("#captchaStatusTrue").attr('hidden', true);
function checking_icaptcha() {
    if (window.XMLHttpRequest) {
        icaptchaXML = new XMLHttpRequest
    } else {
        icaptchaXML = new ActiveXObject("Microsoft.XMLHTTP")
    }
    icaptchaXML.open("POST", "validate." + icaptchae);
    icaptchaXML.onreadystatechange = icaptcha_validate;
    icaptchaXML.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    icaptchaXML.send("code=" + document.getElementById("code").value)
}
function icaptcha_validate() {
    if (icaptchaXML.readyState == 4) {
        var e = icaptchaXML.responseText;
        if (e == "ok=True") {
            $("#submit").removeAttr('disabled');
            document.getElementById("code").value = "";
            $("#captchaStatusTrue").removeAttr('hidden'); // mozda id #captchaStatusTrue treba u css da ima zelenu boju slova
            $("#captchaStatusFalse").attr('hidden', true);
            
        } else {
            loadicaptcha();
            document.getElementById("code").value = "";
            $("#submit").attr('disabled', true);
            $("#captchaStatusFalse").removeAttr('hidden');
            $("#captchaStatusTrue").attr('hidden', true);
        }
    }
}
function loadicaptcha() {
    if (window.XMLHttpRequest) {
        icaptchaXML = new XMLHttpRequest
    } else {
        icaptchaXML = new ActiveXObject("Microsoft.XMLHTTP")
    }
    icaptchaXML.open("POST", "icaptcha." + icaptchae);
    icaptchaXML.onreadystatechange = readicaptcha;
    icaptchaXML.send()
}
function readicaptcha() {
    if (icaptchaXML.readyState == 4) {
        var e;
        var t;
        var n = new Array(5);
        var r = document.getElementById("icaptcha");
        var i = document.getElementById("icaptchaId");
        var s = r.offsetWidth;
        var o = r.offsetHeight;
        i.height = o;
        i.width = s;
        icaptchactx.clearRect(0, 0, s, o);
        var u = icaptchaXML.responseText;
        var a = u.length / 12;
        for (e = 0; e < a; e++) {
            icaptchactx.beginPath();
            icaptchactx.lineWidth = icaptchas;
            icaptchactx.strokeStyle = icaptchac;
            var f = icaptchag - 5;
            var l = icaptchag - 35;
            var c = s / (240 + 2 * icaptchag);
            var h = o / (60 + 2 * icaptchag);
            for (t = 0; t < 6; t++) {
                n[t] = parseInt(u.substr(e * 12 + t * 2, 2), 16)
            }
            icaptchactx.moveTo((n[0] + f) * c, (n[1] + l) * h);
            icaptchactx.quadraticCurveTo((n[4] + f) * c, (n[5] + l) * h, (n[2] + f) * c, (n[3] + l) * h);
            icaptchactx.stroke()
        }
    }
}
function icaptcha(e, t, n, r, i) {
    icaptchas = e;
    icaptchac = t;
    icaptchat = n;
    icaptchag = r;
    icaptchae = i;
    var i = document.getElementById("icaptcha");
    var n = document.createElement("canvas");
    n.id = "icaptchaId";
    i.appendChild(n);
    if (typeof G_vmlCanvasManager != "undefined") {
        G_vmlCanvasManager.initElement(n)
    }
    icaptchactx = n.getContext("2d");
    loadicaptcha()
}
/* modernizr-custom */
/*! modernizr 3.3.1 (Custom Build) | MIT *
 * https://modernizr.com/download/?-bgpositionxy-bgsizecover-boxshadow-checked-cssanimations-csstransitions-cssvalid-flexboxtweener-inputtypes-placeholder-textshadow-setclasses !*/
!function (e, t, n) {
    function i(e, t) {
        return typeof e === t
    }
    function r() {
        var e, t, n, r, o, s, a;
        for (var l in x)
            if (x.hasOwnProperty(l)) {
                if (e = [], t = x[l], t.name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length))
                    for (n = 0; n < t.options.aliases.length; n++)
                        e.push(t.options.aliases[n].toLowerCase());
                for (r = i(t.fn, "function")?t.fn():t.fn, o = 0; o < e.length; o++)
                    s = e[o], a = s.split("."), 1 === a.length ? Modernizr[a[0]] = r : (!Modernizr[a[0]] || Modernizr[a[0]]instanceof Boolean || (Modernizr[a[0]] = new Boolean(Modernizr[a[0]])), Modernizr[a[0]][a[1]] = r), v.push((r ? "" : "no-") + a.join("-"))
            }
    }
    function o(e) {
        var t = C.className, n = Modernizr._config.classPrefix || "";
        if (w && (t = t.baseVal), Modernizr._config.enableJSClass) {
            var i = new RegExp("(^|\\s)" + n + "no-js(\\s|$)");
            t = t.replace(i, "$1" + n + "js$2")
        }
        Modernizr._config.enableClasses && (t += " " + n + e.join(" " + n), w ? C.className.baseVal = t : C.className = t)
    }
    function s() {
        return"function" != typeof t.createElement ? t.createElement(arguments[0]) : w ? t.createElementNS.call(t, "http://www.w3.org/2000/svg", arguments[0]) : t.createElement.apply(t, arguments)
    }
    function a() {
        var e = t.body;
        return e || (e = s(w ? "svg" : "body"), e.fake = !0), e
    }
    function l(e, n, i, r) {
        var o, l, d, u, f = "modernizr", p = s("div"), c = a();
        if (parseInt(i, 10))
            for (; i--; )
                d = s("div"), d.id = r ? r[i] : f + (i + 1), p.appendChild(d);
        return o = s("style"), o.type = "text/css", o.id = "s" + f, (c.fake ? c : p).appendChild(o), c.appendChild(p), o.styleSheet ? o.styleSheet.cssText = e : o.appendChild(t.createTextNode(e)), p.id = f, c.fake && (c.style.background = "", c.style.overflow = "hidden", u = C.style.overflow, C.style.overflow = "hidden", C.appendChild(c)), l = n(p, e), c.fake ? (c.parentNode.removeChild(c), C.style.overflow = u, C.offsetHeight) : p.parentNode.removeChild(p), !!l
    }
    function d(e, t) {
        return!!~("" + e).indexOf(t)
    }
    function u(e) {
        return e.replace(/([a-z])-([a-z])/g, function (e, t, n) {
            return t + n.toUpperCase()
        }).replace(/^-/, "")
    }
    function f(e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    }
    function p(e, t, n) {
        var r;
        for (var o in e)
            if (e[o]in t)
                return n === !1 ? e[o] : (r = t[e[o]], i(r, "function") ? f(r, n || t) : r);
        return!1
    }
    function c(e) {
        return e.replace(/([A-Z])/g, function (e, t) {
            return"-" + t.toLowerCase()
        }).replace(/^ms-/, "-ms-")
    }
    function m(t, i) {
        var r = t.length;
        if ("CSS"in e && "supports"in e.CSS) {
            for (; r--; )
                if (e.CSS.supports(c(t[r]), i))
                    return!0;
            return!1
        }
        if ("CSSSupportsRule"in e) {
            for (var o = []; r--; )
                o.push("(" + c(t[r]) + ":" + i + ")");
            return o = o.join(" or "), l("@supports (" + o + ") { #modernizr { position: absolute; } }", function (e) {
                return"absolute" == getComputedStyle(e, null).position
            })
        }
        return n
    }
    function h(e, t, r, o) {
        function a() {
            f && (delete N.style, delete N.modElem)
        }
        if (o = i(o, "undefined") ? !1 : o, !i(r, "undefined")) {
            var l = m(e, r);
            if (!i(l, "undefined"))
                return l
        }
        for (var f, p, c, h, y, g = ["modernizr", "tspan", "samp"]; !N.style && g.length; )
            f = !0, N.modElem = s(g.shift()), N.style = N.modElem.style;
        for (c = e.length, p = 0; c > p; p++)
            if (h = e[p], y = N.style[h], d(h, "-") && (h = u(h)), N.style[h] !== n) {
                if (o || i(r, "undefined"))
                    return a(), "pfx" == t ? h : !0;
                try {
                    N.style[h] = r
                } catch (v) {
                }
                if (N.style[h] != y)
                    return a(), "pfx" == t ? h : !0
            }
        return a(), !1
    }
    function y(e, t, n, r, o) {
        var s = e.charAt(0).toUpperCase() + e.slice(1), a = (e + " " + P.join(s + " ") + s).split(" ");
        return i(t, "string") || i(t, "undefined") ? h(a, t, r, o) : (a = (e + " " + A.join(s + " ") + s).split(" "), p(a, t, n))
    }
    function g(e, t, i) {
        return y(e, n, n, t, i)
    }
    var v = [], x = [], b = {_version: "3.3.1", _config: {classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0}, _q: [], on: function (e, t) {
            var n = this;
            setTimeout(function () {
                t(n[e])
            }, 0)
        }, addTest: function (e, t, n) {
            x.push({name: e, fn: t, options: n})
        }, addAsyncTest: function (e) {
            x.push({name: null, fn: e})
        }}, Modernizr = function () {};
    Modernizr.prototype = b, Modernizr = new Modernizr;
    var C = t.documentElement, w = "svg" === C.nodeName.toLowerCase(), S = s("input"), k = "search tel url email datetime date month week time datetime-local number range color".split(" "), T = {};
    Modernizr.inputtypes = function (e) {
        for (var i, r, o, s = e.length, a = "1)", l = 0; s > l; l++)
            S.setAttribute("type", i = e[l]), o = "text" !== S.type && "style"in S, o && (S.value = a, S.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(i) && S.style.WebkitAppearance !== n ? (C.appendChild(S), r = t.defaultView, o = r.getComputedStyle && "textfield" !== r.getComputedStyle(S, null).WebkitAppearance && 0 !== S.offsetHeight, C.removeChild(S)) : /^(search|tel)$/.test(i) || (o = /^(url|email)$/.test(i) ? S.checkValidity && S.checkValidity() === !1 : S.value != a)), T[e[l]] = !!o;
        return T
    }(k);
    var _ = b.testStyles = l;
    Modernizr.addTest("checked", function () {
        return _("#modernizr {position:absolute} #modernizr input {margin-left:10px} #modernizr :checked {margin-left:20px;display:block}", function (e) {
            var t = s("input");
            return t.setAttribute("type", "checkbox"), t.setAttribute("checked", "checked"), e.appendChild(t), 20 === t.offsetLeft
        })
    }), Modernizr.addTest("cssvalid", function () {
        return _("#modernizr input{height:0;border:0;padding:0;margin:0;width:10px} #modernizr input:valid{width:50px}", function (e) {
            var t = s("input");
            return e.appendChild(t), t.clientWidth > 10
        })
    });
    var z = "Moz O ms Webkit", P = b._config.usePrefixes ? z.split(" ") : [];
    b._cssomPrefixes = P;
    var A = b._config.usePrefixes ? z.toLowerCase().split(" ") : [];
    b._domPrefixes = A;
    var E = {elem: s("modernizr")};
    Modernizr._q.push(function () {
        delete E.elem
    });
    var N = {style: E.elem.style};
    Modernizr._q.unshift(function () {
        delete N.style
    });
    var j = b.testProp = function (e, t, i) {
        return h([e], n, t, i)
    };
    Modernizr.addTest("textshadow", j("textShadow", "1px 1px")), b.testAllProps = y, b.testAllProps = g, Modernizr.addTest("cssanimations", g("animationName", "a", !0)), Modernizr.addTest("bgpositionxy", function () {
        return g("backgroundPositionX", "3px", !0) && g("backgroundPositionY", "5px", !0)
    }), Modernizr.addTest("bgsizecover", g("backgroundSize", "cover")), Modernizr.addTest("boxshadow", g("boxShadow", "1px 1px", !0)), Modernizr.addTest("flexboxtweener", g("flexAlign", "end", !0)), Modernizr.addTest("csstransitions", g("transition", "all", !0)), Modernizr.addTest("placeholder", "placeholder"in s("input") && "placeholder"in s("textarea")), r(), o(v), delete b.addTest, delete b.addAsyncTest;
    for (var L = 0; L < Modernizr._q.length; L++)
        Modernizr._q[L]();
    e.Modernizr = Modernizr
}(window, document);
/* Slideshow */
//news slideshow 1
var TitleClass = document.getElementsByClassName("titles"); //input klasa naslova
var DescriptionClass = document.getElementsByClassName("descs"); //input klasa opisa


var startT = document.getElementById("Title").innerHTML = TitleClass[0].value; //pocetni naslov
var startD = document.getElementById("Desc").innerHTML = DescriptionClass[0].value; //pocetni opis

var counter = 1;
var title = document.getElementById("Title"); //uzimanje polja za ispis
var desc = document.getElementById("Desc");
setInterval(Display, 5000);
function Display() {
    title.innerHTML = TitleClass[counter].value; //upis vrednosti u polje
    desc.innerHTML = DescriptionClass[counter].value;
    counter++;
    if (counter >= TitleClass.length) {
        counter = 0;
    }//restart brojaca na 0
}



//slideshow first

$("#slideshow > div:gt(0)").hide();
setInterval(function () {
    $('#slideshow > div:first')
            .fadeOut(2000)
            .next()
            .fadeIn(1000)
            .end()
            .appendTo('#slideshow');
}, 5000);
//news slideshow 2
var TitleClass2 = document.getElementsByClassName("newsTitle2"); //input klasa naslova
var DescriptionClass2 = document.getElementsByClassName("newsDesc2"); //input klasa opisa


var startT2 = document.getElementById("Title2").innerHTML = TitleClass2[0].value; //pocetni naslov
var startD2 = document.getElementById("Desc2").innerHTML = DescriptionClass2[0].value; //pocetni opis

var counter2 = 1;
var title2 = document.getElementById("Title2"); //uzimanje polja za ispis
var desc2 = document.getElementById("Desc2");
setInterval(Display2, 5000);
function Display2() {
    title2.innerHTML = TitleClass2[counter2].value; //upis vrednosti u polje
    desc2.innerHTML = DescriptionClass2[counter2].value;
    counter2++;
    if (counter2 >= TitleClass2.length) {
        counter2 = 0;
    }//restart brojaca na 0
}
//slideshow second
$(".slideImg > div:gt(0)").hide();
setInterval(function () {
    $('.slideImg > div:first')
            .fadeOut(2000)
            .next()
            .fadeIn(1000)
            .end()
            .appendTo('.slideImg');
}, 5000);
/* Back to top */
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 500) {
            $('.backToTop').show(500);
        } else {
            $('.backToTop').hide(500);
        }
    });
    $('.backToTop').click(function () {
        $("html,body").animate({scrollTop: 0}, 1000);
    });
});
/* Back to top change collor on scroll */
$(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 2500) {
        $(".fa-angle-double-up").addClass("orange");
    } else {
        $(".fa-angle-double-up").removeClass("orange");
    }
});
