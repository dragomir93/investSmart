$(document).ready(function() {
    
    $("#languageID option:not(:first-child)").remove();
    $(".languages input").remove();
    $.get("../public/language-options.php", function(res) {

        var jsonLanguages = JSON.parse(res);
        if (jsonLanguages.constructor != Array) {
            jsonLanguages = Array(jsonLanguages);
        }
        for (var i=0; i<jsonLanguages.length; i++) {
            var languageOption = "<option value='"+jsonLanguages[i].id+"'>"+jsonLanguages[i].name+"</option>";
            $("#languageID").append(languageOption);
        }
        var languageText = $("#languages option:selected").text();
        var hiddenInput = "<input type='hidden' name='campaignLanguage' value='"+languageText+"'>";
        $(".languages").append(hiddenInput);

    }); 
    
    $("#languages option:not(:first-child)").remove();
    $.get("../public/language-options.php", function(res) {
        
        var jsonLanguages = JSON.parse(res);

        for (var i=0; i<jsonLanguages.length; i++) {
            var languageOption = "<option value='"+jsonLanguages[i].id+"'>"+jsonLanguages[i].name+"</option>";
            $("#languages").append(languageOption);   
        }
    });
    
     $("#languages").change(function() {
        $(".languages input").remove();
        var languageText = $("#languages option:selected").text();
         console.log(languageText);
        var hiddenInput = "<input type='hidden' name='campaignLanguage' value='"+languageText+"'>";
        $(".languages").append(hiddenInput);
    });
});