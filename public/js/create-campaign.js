$(document).ready(function() {
    
    var bannerCount = 0;
    
    $("#addBanner").click(function() {
        bannerCount++;
        var bannerUpload = "<div class='row'><div class='col-md-6 text-center'><h3>Choose banner size:</h3><select name='bannerSize[]' id='bannerSize"+bannerCount+"'><option>250x250</option><option>728x90</option><option>160x600</option><option>336x280</option><option>300x250</option></select></div><div class='col-md-6 text-center'><h3>Upload banner:</h3><input type='file' accept='image/jpeg' name='banner[]' class='campaign-banner'></div></div>";
        $(bannerUpload).insertAfter(".banner-upload");
    });
    
    $("#createCampaignBtn").click(function() {
        
        $("#languageID option:not(:first-child)").remove();
        $(".languages input").remove();
        $.get("../public/language-options.php", function(res) {

            var jsonLanguages = JSON.parse(res);
            
            for (var i=0; i<jsonLanguages.length; i++) {
                var languageOption = "<option value='"+jsonLanguages[i].id+"'>"+jsonLanguages[i].name+"</option>";
                $("#languageID").append(languageOption);
            }
            var languageText = $("#languageID option:selected").text();
            var hiddenInput = "<input type='hidden' name='campaignLanguage' value='"+languageText+"'>";
            $(".languages").append(hiddenInput);
            
        }); 
    });
    
    $("#languageID").change(function() {
        $(".languages input").remove();
        var languageText = $("#languageID option:selected").text();
        var hiddenInput = "<input type='hidden' name='campaignLanguage' value='"+languageText+"'>";
        $(".languages").append(hiddenInput);
    });
});