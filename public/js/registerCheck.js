function validateForm(id) {
    var subject =$('#subject').val();
if (id == "email") {
        var mail = document.getElementById(id).value;
        var emailFilter = /^([a-z0-9]{1,})+(?:.[a-z0-9]{1,})+@([a-z]{1,}\.[a-z]{1,})+$/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            return false;
        }
        else if (!emailFilter.test(mail)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            return true;
        }
    }
        else if(id == "name"){
        var name = document.getElementById(id).value;
        var nameFilter = /^[A-Z][a-z]+$/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            return false;
        }
        else if (!nameFilter.test(name)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            return true;
        }
    }
    else if(id == "surname"){
        var lname = document.getElementById(id).value;
        var nameFilter = /^[A-Z][a-z]+$/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            return false;
        }
        else if (!nameFilter.test(lname)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
            return true;
        }
    }
    else if(id == "countrySelector"){
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
        var div = $("#" + id).closest("div");
        div.removeClass("has-success");
        div.addClass("has-error has-feedback");
        return false;
    }
    else {
        var div = $("#" + id).closest("div");
        div.removeClass("has-error");
        div.addClass("has-success has-feedback");
        return true;
    }
    }
    else if ($("#" + id).val() == null || $("#" + id).val() == "") {
        var div = $("#" + id).closest("div");
        div.removeClass("has-success");
        div.addClass("has-error has-feedback");
        return false;
    }
    else {
        var div = $("#" + id).closest("div");
        div.removeClass("has-error");
        div.removeClass("has-success");
        return true;
    }
}
$('#submit').click(function () {
    validateForm("name");
    validateForm("surname");
    validateForm("email");
    validateForm("countrySelector");
    validateForm("passVal");
    validateForm("RepassVal");
});
$('#name').keyup(function(){
    validateForm("name");
});
$('#surname').keyup(function(){
    validateForm("surname");
});
$('#email').keyup(function(){
    validateForm("email");
});
$('#countrySelector').change(function(){
    validateForm("countrySelector");
});
$('#passVal').keyup(function(){
        validateForm("passVal");
});
$('#RepassVal').keyup(function(){
        validateForm("RepassVal");
});

var pass = document.getElementById('passVal');
var rePass = document.getElementById('RepassVal');
function validatePassword(){
  if(pass.value != rePass.value) {
    rePass.setCustomValidity("Passwords Don't Match");
  } else {
    rePass.setCustomValidity('');
  }
}
pass.onchange = validatePassword;
rePass.onkeyup = validatePassword;