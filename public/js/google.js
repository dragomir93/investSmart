function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    $("#Fname").val(profile.getGivenName());
    $("#Lname").val(profile.getFamilyName());
    $("#email").val(profile.getEmail());
    
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    $("#Fname").val("");
    $("#Lname").val("");
    $("#email").val("");
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
}