function validateForm(id) {
    var subject =$('#subject').val();
if (id == "email") {
        var mail = document.getElementById(id).value;
       // var emailFilter = /^([a-z]{3,})+@(([a-z])+\.)+([a-z]{2,10})+$/;
        var emailFilter = /^([a-z0-9]{1,})+(?:.[a-z0-9]{1,})+@([a-z]{1,}\.[a-z]{1,})+$/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else if (!emailFilter.test(mail)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return true;
        }
    }
        else if(id == "Fname"){
        var name = document.getElementById(id).value;
        var nameFilter = /^[A-Z][a-z]+$/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else if (!nameFilter.test(name)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return true;
        }
    }
    else if(id == "Lname"){
        var name = document.getElementById(id).value;
        var nameFilter = /^[A-Z][a-z]+$/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else if (!nameFilter.test(name)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return true;
        }
    }
    else if (id == "phoneNo") {
        var phone = document.getElementById(id).value;
        var phoneFilter = /\+\(\d{1,4}\)\d{8,}/;
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else if (!phoneFilter.test(phone)) {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return true;
        }
    }
    else if (id == "message") {
        if ($("#" + id).val() == null || $("#" + id).val() == "") {
            var div = $("#" + id).closest("div");
            div.removeClass("has-success");
            $("#glyp" + id).remove();
            div.addClass("has-error has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return false;
        }
        else {
            var div = $("#" + id).closest("div");
            div.removeClass("has-error");
            $("#glyp" + id).remove();
            div.addClass("has-success has-feedback");
            div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback removeGlyph" aria-hidden="true"></span>');
            return true;
        }
    }
    else if ($("#" + id).val() == null || $("#" + id).val() == "") {
        var div = $("#" + id).closest("div");
        div.removeClass("has-success");
        //$("#glyp" + id).remove();
        div.addClass("has-error has-feedback");
       // div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
        return false;
    }
    else {
        var div = $("#" + id).closest("div");
        div.removeClass("has-error");
       // $("#glyp" + id).remove();
        div.addClass("has-success has-feedback");
       // div.append('<span id="glyp' + id + '"  class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
        return true;
    }
}
$('#sendMail').click(function () {
    validateForm("Fname");
    validateForm("Lname");
    validateForm("email");
    validateForm("phoneNo");
    validateForm("subject");
    validateForm("message");
    
});
$('#Fname').keyup(function(){
    validateForm("Fname");
});
$('#Lname').keyup(function(){
    validateForm("Lname");
});
$('#email').keyup(function(){
    validateForm("email");
});
$('#phoneNo').keyup(function(){
    validateForm("phoneNo");
});
$('#subject').change(function(){
    validateForm("subject");
});
$('#message').keyup(function(){
    validateForm("message");
});