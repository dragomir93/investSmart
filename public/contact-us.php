<?php error_reporting(0); ?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/defaults.css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- Tooltip -->
        <link rel="stylesheet" href="intl-tel-input-9.2.0/build/css/intlTelInput.css"> <!-- FOR COUNTRY CALL CODE -->
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <script src="js/snap.svg-min.js"></script>
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/contact-us-Style.css"/>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <style>
            .error {color: #ff0000;}
            .intro {
                color: red;
            }
        </style>
        <title>Contact Us | StockSTP Affiliate</title>
          <!-- Google login -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="486026532062-81b6dihhele0bj8lt7pnngbiblnh82vq.apps.googleusercontent.com">
        <script src="js/google.js"></script>
        
        <!-- Facebook login -->
        <script src="js/facebook.js"></script>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>  
           <!-- Header -->
        <?php include ('../includes/header.php'); ?>
        <!-- Start Menu Main -->
        <div class="contact-menu">
            <?php include('../includes/menu-main.php'); ?>
        </div>
        <?php include('../includes/inner-img.php'); ?>
        <!-- / Menu Main -->
        <!--END OF HEADER-->
        <!--PICTURE-->
<!--        <div class="bottleMessage" >
        </div>-->
        <!--INFO BAR-->
        <div class="container-fluid contactUsInfoBar animated fadeInDown" >
            <div class="row">
                <div class="col-md-6 ReachUs" >
                    <span class="glyphicon glyphicon-bookmark "></span>&nbsp; REACH US
                </div>

                <div class="col-md-6 emailUs" >
                    <span class=" glyphicon glyphicon-envelope "></span>&nbsp; MAIL US
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row content animated fadeInLeft">
                <!--REACH US TEXT-->
                <div class="mailUsInfo col-md-6 text-center">
                    <p>
                        Please use the contact form if You have any questions or requests,
                        concerning our services.<br>
                        We will respond to Your message within 24 hours.
                    </p>
                    <p>
                        <b>La Prosperidad</b> <br>
                        Inomenon Ethnon 48, Larnaca Cyprus
                    </p>
                    <br>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.5575863700674!2d33.60606191554031!3d34.91755047908029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14e09d4fa4469a3b%3A0xd221f4471adc671e!2sGuricon+Limited!5e0!3m2!1ssr!2srs!4v1488534927171" width="100%" height="360" frameborder="0" style="border:0;" allowfullscreen class="map"></iframe>
                </div>
                <!-- USER INPUT FORM -->
                <div class="formContainer col-md-6 text-center">
                    <form class="form-horizontal  " id="visitorContact" >
                        <div class="form-group">
                            <label for="Name" class="control-label col-lg-2">First Name:</label>
                            <input type="text" class="form-control " id="Fname" name="fName" placeholder="Enter Your First Name..." required pattern="[A-Z][a-z]+" title="John">
                        </div>
                        <div class="form-group">
                            <label for="Name" class="col-lg-2 control-label  ">Last Name:</label>
                                <input type="text" class="form-control " id="Lname" name="lName" placeholder="Enter Your Last Name..." required pattern="[A-Z][a-z]+" title="Smith">
                        </div>
                        <div class="form-group">
                            <label for="Email" class="col-lg-2 control-label">Email:</label>
                            <input type="email" class="form-control " id="email" name="Email" placeholder="Enter Your Email Address..." required pattern="([a-z0-9]{1,})+(?:.[a-z0-9]{1,})+@([a-z]{1,}\.[a-z]{1,})+$" title="example@yahoo.com">
                        </div>
                        <div class="form-group">
                            <label for="Phone" class="col-lg-2 control-label ">Phone:</label>
                            <input type="tel" class="form-control " id="phoneNo" name="Phone" placeholder="+(000)0000000" required title="+(381)646644555" pattern="\+\(\d{1,4}\)\d{8,}">
                        </div>
                        <div class="form-group">
                            <label for="Subject" class="col-lg-2  control-label">Subject:</label>
                            <select name="Subject" class="form-control lists" id="subject" required>
                                <option value="" >Select from the following subject</option>
                                <optgroup label="Report a problem">
                                    <option value="I didn't get the activation email">I didn't get the activation email</option>
                                    <option value="Problem while singing up">Problem while singing up</option>
                                    <option value="Other Problem">Other Problem</option>
                                </optgroup>
                                <optgroup label="Intrested in">
                                    <option value="Working with you as an Affiliate">Working with you as an Affiliate</option>
                                    <option value="Working with you as a Money Manager">Working with you as a Money Manager</option>
                                    <option value="Other">Other</option>
                                <optgroup>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Message" class="col-lg-2 control-label">Message:</label>
                            <textarea class="form-control " id="message" rows="6" placeholder="Enter your message..." required></textarea>
                        </div>
                        <div class="form-group">

                            <div class="col-lg-2">
                                <button type="submit" id="sendMail" class="btn btn-primary">Send Message</button>
                                <div class="sendingMailAnimation" hidden>
                                    <i class="fa fa-spinner" ></i>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div id="mailResult" style="text-align: center;margin-bottom: 50px;">

                    </div>
                </div>
            </div>
        </div> 
        <!-- / Information Box -->
        <?php include('../includes/footer.php'); ?>
        <!-- Country finder -->
        <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;

            function jsonpCallback(data) {

                country = data.address.country;

                p.text = country;
                p.value = country;
            }
        </script>
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <script type="text/javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/contact-us-validation.js" type="text/javascript"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script> <!-- Tooltip -->
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder --> 
        <script src="intl-tel-input-9.2.0/build/js/intlTelInput.js"></script><!-- COUNTRY CODE -->
        <script src="intl-tel-input-9.2.0/build/js/utils.js" ></script><!-- COUNTRY CODE initialization-->
        <script src="intl-tel-input-9.2.0/examples/js/defaultCountryIp.js"></script><!-- COUNTRY CODE ip -->
        <!--select active page-->
        <script type="text/javascript">
            $(".activeContact span").css("color", "#ff9900");
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#visitorContact").submit(function (e) {
                    e.preventDefault();
                    $(".sendingMailAnimation").removeAttr("hidden");
                     $("#mailResult").empty();
                    var Fname = document.getElementById('Fname').value;
                    var Lname = document.getElementById('Lname').value;
                    var email = document.getElementById('email').value;
                    var phone = document.getElementById('phoneNo').value;
                    var subject = $('.lists').val();
                    var message = document.getElementById('message').value;
                    var data = "Fname=" + Fname + "&Lname=" + Lname + "&email=" + email + "&phone=" + phone + "&subject=" + subject + "&message=" + message;
                    $.ajax({
                        type: 'post',
                        url: "mailingVisitors.php",
                        data: data,
                        cache: false,
                        success: function (html) {
                            $("#mailResult").html(html);
                        }
                    });
                    return false;
                });
            });
        </script>
    </body>
</html>
