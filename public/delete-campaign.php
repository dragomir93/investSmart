<?php
include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$campName = $request->campName;
$campID = $request->campID;

$deleteCampaign = $client->DeleteCampaign(array("id"=>$campID))->DeleteCampaignResult;



$Campaign = "../public/Campaigns/$campName";
function rmrf($dir) {
    foreach (glob($dir) as $file) {
        if (is_dir($file)) { 
            rmrf("$file/*");
            rmdir($file);
        } else {
            unlink($file);
        }
    }
}
rmrf($Campaign);

echo json_encode($deleteCampaign);