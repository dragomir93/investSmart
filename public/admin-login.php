<?php
session_start();
error_reporting(0);
?>
<html class="demo-1 no-js">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <!-- Box Effect CSS -->
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <!-- jDatepicker CSS -->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css">
        <!-- Box Effect JS -->
        <script src="js/snap.svg-min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <style>
            #login-processing{
                font-size: 20px;
                text-align: center;
            }
        </style>
        <title>Administrator Login</title>
    </head>
    <body>
         <?php include_once("analyticstracking.php") ?>  
        <!-- Start Top Header -->
        <div class="top-header">
            <div class="header-logo">
                <a href="../public/index.php">
                    <img class="header-logo" src="img/logo.png" alt=""/>
                    <!--<span>invest smart</span>-->
                </a>
            </div>
            
            <!-- Start Social -->
            <?php include('../includes/social-icons.php'); ?>
            <!-- / Social -->
        </div>
        <div class="admin-login">
            <div class="container login-form">
                <div class="row">
                    <div class="login-adm col-md-12">
                        <h2>Login &#58;</h2>
                        <?php include('../includes/login-form-admin.php'); ?>
                </div>
                </div>
            </div>
        </div>
        <!-- / Information Box -->
        <?php include('../includes/footer.php'); ?>
        <!-- Country finder -->
        <script>
            document.forms["login-form"].onsubmit = function () {
                var pass = document.getElementById("userVal").value;
                var rePass = document.getElementById("passVal").value;

                if (pass == "" || rePass == "") {
                    $('#login-processing').removeAttr('hidden');
                } else {
                    $('#login-processing').removeAttr('hidden');
                }
            };
        </script>
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
<!--        <script src="js/processingAnimation.js" type="text/javascript"></script>-->
        <script src="js/main.js" type="text/javascript"></script>
        <script type="text/javascript">
            $("#adminLoginButton").click(function () {
                $(".error_msg").attr("hidden", true);
            });
        </script>
    </body>
</html>
