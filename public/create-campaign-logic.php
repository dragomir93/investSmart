<?php
clearstatcache();
error_reporting(0);
session_start();
include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));
$client2 = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));
$client3 = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));

$campLanguage = filter_input(INPUT_POST, 'campaignLanguage',FILTER_SANITIZE_STRING);
$campName = $_POST['campaignName'];
$bannerSize = $_POST['bannerSize'];
$landingPage = $_FILES['landingPage'];
$languageID = $_POST['languageID'];
$campaignID = $_POST['campaignId'];
$addNewLanguage = $_POST['addNewLanguage'];

if(isset($_FILES['mailer'])){
    $mailer = $_FILES['mailer'];
}
if($_FILES['landingPage']['name'] != ""){
    if ($_FILES["landingPage"]['error'] != 0) {
        die("Upload failed with error code " . $_FILES['file']['error'][$i]);
    }
}

for ($i = 0; $i < count($_FILES['banner']); $i++) {
    if ($_FILES['banner']['error'][$i] != 0) {
        die("Upload failed with error code " . $_FILES['file']['error'][$i]);
    }
}




if(preg_match('/^[A-Za-z0-9 ]*[A-Za-z0-9][A-Za-z0-9 ]*$/', $campName) && preg_match('/^[a-zA-Z]*$/', $campLanguage)){
    //create campaign name folder
    $CampNew = str_replace(' ', '', $campName);
    $pathCamp = "Campaigns/$CampNew/";
    

    
    //create camp Language folder
    
    $pathLan = "Campaigns/$CampNew/$campLanguage";
    $fileType = $_FILES["banner"]["type"];
    
    if($addNewLanguage == "true"){
        if(file_exists($pathLan)){
            $_SESSION['createCampError'] = "Language already exists";
            $error = include("../layout/createCampError.php"); 
            die($error);
        }
    } else if ($addNewLanguage == "false"){ 
        if(file_exists($pathLan)){
            $_SESSION['createCampError'] = "Language or Campaign already exists";
            $error = include("../layout/createCampError.php"); 
            die($error);
        }   

        if(file_exists($pathCamp)){
            $_SESSION['createCampError'] = "Language or Campaign already exists";
            $error = include("../layout/createCampError.php"); 
            die($error);

        }
    }

    for ($j=0; $j< count($_FILES["banner"]["tmp_name"]); $j++) {
        
        $bannerSizeArr = explode("x", $bannerSize[$j]);
        
        $bannerWidth = $bannerSizeArr[0];
        $bannerHeight = $bannerSizeArr[1];
        
        $info = getimagesize($_FILES['banner']['tmp_name'][$j]);
        
        if ($info === FALSE) {
            
            $_SESSION['createCampError'] = "Unable to determine image type of uploaded file";
            $error = include("../layout/createCampError.php"); 
            die($error);
            
        } else if (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
            
           $_SESSION['createCampError'] = "Banner not a gif/jpg/jpeg/png format";
            $error = include("../layout/createCampError.php"); 
            die($error);
            
        } else {

            
            mkdir($pathCamp, 0777, true);
            mkdir($pathLan, 0777, true);
            
            if (isset($_FILES["banner"])) {
                mkdir($pathLan."/Banners",0777, true);
            }
        }
        
        move_uploaded_file($_FILES["banner"]["tmp_name"][$j], "$pathLan/Banners/" . $bannerSize[$j] .".jpg");
        
        $filename = "$pathLan/Banners/" . $bannerSize[$j] .".jpg";
        list($width, $height) = getimagesize($filename);
        
        $newwidth = $bannerWidth;
        $newheight = $bannerHeight;
        
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        $source = imagecreatefromjpeg($filename);
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        
        imagejpeg($thumb, "$pathLan/Banners/" . $bannerSize[$j] .".jpg");
        
        
    }
    
    //SAVE LP
    
    mkdir($pathCamp, 0777, true);
    mkdir($pathLan, 0777, true);
    mkdir($pathLan."/LP",0777, true);
    move_uploaded_file($_FILES["landingPage"]["tmp_name"], "$pathLan/LP/index.html");
    
    if (isset($mailer)){
        mkdir($pathCamp, 0777, true);
        mkdir($pathLan, 0777, true);
        mkdir($pathLan."/Mailer",0777, true);
        move_uploaded_file($_FILES["mailer"]["tmp_name"], "$pathLan/Mailer/index.html"); 
    }
    
    header("Location:../layout/success.html");
  
    
  
   $campaigns = $client3->GetCampaigns()->GetCampaignsResult;

   if(!is_array($campaigns->DMCampaign)){
       $campaigns->DMCampaign = array($campaigns->DMCampaign);
   }
    for ($i=0; $i<count($campaigns->DMCampaign); $i++) {
        
        if($campaigns->DMCampaign[$i]->Name == $campName) {
            $haveCamp = true;
            break;
        } else {
            $haveCamp = false;
        }
        
    }
    if ($haveCamp) {
        $addCampaignLanguage = $client2->AddCampaignLanguage(array("CampaignId"=>$campaignID,"LanguageId"=>$languageID))->AddCampaignLanguageResult;
    } else {
        $addCampaign = $client->AddCampaign(array("name"=>$CampNew,"LanguageId"=>$languageID))->SaveCampaignResult;
    }
    
}else{
    header("Location:dashboard.php");
}



