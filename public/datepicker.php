<?php
$start = date("Y/m/d");
$end = new DateTime("+1 months");
?>
<form method="post" class="chooseDateForm">
    <label>Start date:&nbsp;</label>
    <input class="form-control report-input startDateChoose" type="date" id="datetimepicker1" name="date1" >
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
    <label style="margin-top: 20px;">End date:&nbsp;</label>
    <input class="form-control report-input endDateChoose"  type="date" id="datetimepicker2" name="date2" >
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
    <input class="report-btn getReportButton" type="submit" name="getReport" value="Get Report">
</form>
<?php
if (isset($_POST['getReport'])) {

    $start = $_POST['date1'];
    $end = $_POST['date2'];


//prosledjivanje podataka u web servise funkcija web servise CPAReport CalculateCPA(int PartnerId, DateTime start, DateTime end)
    $reports = $client->CalculateCPA(array("Partnerid" => $_SESSION['UserID'], "start" => $start, "end" => $end))->CalculateCPAResult;

    $countFTD = $reports->CountFTD;
    $totalCPA = $reports->TotalCPA;
    $extraCPA = $reports->ExtraCPA;
    $message = $reports->Message;


    if ($message != "") {
        echo "<div class='messageText'><span class='fa fa-exclamation-circle'></span>&nbsp;&nbsp;&nbsp;  $message!</div>";
    }

    $items = $reports->Items->CPAItem;
    if (count($items) < 1) {
        echo "<div class='noItems'> <span class='glyphicon glyphicon-warning-sign'></span>&nbsp;&nbsp;&nbsp; You have no items to display! </div>";
        $countFTD = 0;
        $totalCPA = 0;
        $extraCPA = 0;
    } else {
        ?>
        <table id="printTable" class="reportTable">
            <tr class="headerTable">
                <th>DATE</th>
                <th>AMOUNT</th>
                <th>DESCRIPTION</th>
            </tr>

            <?php
            for ($i = 0; $i < count($items); $i++) {
                $date = explode('T', $items[$i]->Time);
                $formated = explode('-', $date[0]);

                echo '<tr>';
                echo "<td>" . $formated[2] . "/" . $formated[1] . "/" . $formated[0] . "</td>";
                echo "<td>" . $items[$i]->Amount . "</td>";
                echo "<td>" . $items[$i]->Description . "</td>";
                echo '</tr>';
            }
        }


    } else {
        echo "<div class='noItems'> <span class='glyphicon glyphicon-warning-sign'></span>&nbsp;&nbsp;&nbsp; You have no items to display! </div>";
        $countFTD = 0;
        $totalCPA = 0;
        $extraCPA = 0;
    }
    ?>
</table>
<div class="parentDivReport">
    <div class="parentHeader">
        <p>CountFTD</p>
        <p class="extraCPA">ExtraCPA</p>
        <p>TotaltCPA</p>
    </div>
    <div class="data">
        <div class="countFTD">
            <?php echo $countFTD; ?>
        </div>
        <div >
            <?php echo $extraCPA; ?>
        </div>
        <div class="totalCPA">
            <?php echo $totalCPA; ?>
        </div>
    </div>
</div>     
