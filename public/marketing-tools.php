<?php
session_start();
error_reporting(0);
include('functions/functions.php');
$id = $_SESSION['UserID'];
include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));
//NOT TO ALLOW APPROACH IF THERE IS NO TOKEN
if ($_SESSION['IBFname'] == "" || $_SESSION['username'] == "") {
//    setcookie("IBProfilePassword", '' , time() - 3600,'/');
    header("location: login.php"); //back to login page
}
?>
<html>
    <head>
        <title>Marketing Tools</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" href="css/select-jquery.css"/>
        <link rel="stylesheet" type="text/css" href="css/marketing-toolsStyle.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <!-- Include JS File Here -->
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
        <script src="js/bootstrap.min.js"></script>
        <script src='js/select-jquery.js'></script>
    </head>
    <body class="marketing-tools">
        <?php include_once("analyticstracking.php") ?>  
        <div class="top-header">
            <div class="header-logo">
                <?php
                if (isset($_COOKIE['IBProfilePassword'])) {
                    echo '<a href="../public/accountdetails.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                } else {
                    echo '<a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                }
                ?>
<!--                <a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo-small.png" alt="logo"/>
                </a>-->
            </div>
            <?php include('../includes/social-icons.php'); ?>
        </div>
        <div class="container-fluid account-details">
            <div class="row">
                <div class="col-lg-3 col-lg-push-9 text-right login-info">
                    <p>Hello&#58;</p>
                    <h3 style="color: #13869E;"><?php echo $_SESSION['username']; ?></h3>
                    <form method="post" action="logout.php">
                        <button name="LOGOUT" type="submit" class="btn btn-primary">Log Out</button>
                    </form>
                </div>
                <!-- Start Menu -->
                <?php include('../includes/menu-admin.php'); ?>
            </div>
        </div>
        <i style='display: none;' class="fa fa-angle-double-up backToTop" aria-hidden="true"></i>
        
        <div class="container-fluid shadowContainer">
            <div class="container marketing-tools-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">CHOOSE CAMPAIGN</a></li>
                            <li><a data-toggle="tab" id="linkHistoryTab" href="#menu1">YOUR CAMPAIGNS</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-6 marketing-tools-language">
                                        <h4>Choose campaign language</h4>
                                        <select class="select-language" id="selectLanguage" name="languages">
                                            <option value="" disabled selected></option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 select-campaign">
                                        <h4>Choose campaign</h4>
                                        <select class="select-language" id="selectCampaign" name="campaigns">
                                            <option value="default" disabled selected></option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row marketing-tools-thumbnails">
                                    <hr>
                                    <div class="col-md-4" id="banner-preview">
                                        <p>Choose banner size:</p>
                                        <select id="bannerSize" class="select-language">
                                        </select>
                                        <hr>
                                        <h4>BANNER</h4>
                                        <div id="banner-img"></div>
                                        <div class="banner-links">
                                            <hr>
                                            <h5>HTML FOR CLIENTS</h5>
                                            <textarea id="bannerClient"></textarea>
                                            <hr>
                                            <h5>HTML FOR SUBAFFILIATES</h5>
                                            <textarea id="bannerSub"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="mailer-preview" hidden>
                                        <h4>MAILER</h4>
                                        <div id="mailer-img"></div>
                                        <div class="mailer-links" hidden>
                                            <hr>
                                            <button id="downloadMailer" class="btn btn-primary"></button>
                                            <button id="downloadSubMailer" class="btn btn-primary"></button>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="lp-preview">
                                        <h4>LANDING PAGE</h4>
                                        <div id="lp-img"></div>
                                        <div class="lp-links">
                                            <hr>
                                            <h5>URL</h5>
                                            <textarea id="lpLink"></textarea>
                                            <button id="downloadLP" class="btn btn-primary"></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row generate-links-button">
                                    <hr>
                                    <div class="col-md-12 text-center">
                                        <button id="generateLinks" class="btn">GENERATE LINKS</button>
                                        <p id="loading">PLEASE WAIT WHILE WE CREATE LINKS FOR YOU...</p>
                                    </div>
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Campaign name</th>
                                            <th>Language</th>
                                            <th>Link</th>
                                            <th>Type</th>
                                        </tr>
                                    </thead>
                                    <tbody id="linkHistoryTable">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        
        
        
        
        <script type="text/javascript">
            $(".activeTools span").css("color", "#ff9900");
        </script>
        <script src="js/marketing-tools.js"></script>
        
    </body>
</html>
