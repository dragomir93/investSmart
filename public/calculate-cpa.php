<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));
$client2 = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));
$client3 = new SoapClient(URL,array('cache_wsdl' => WSDL_CACHE_NONE));

//NOT TO ALLOW APPROACH IF THERE IS NO TOKEN
if ($_SESSION['IBFname'] == "" || $_SESSION['username'] == "") {
//    setcookie("IBProfilePassword", '' , time() - 3600,'/');
    header("location: login.php"); //back to login page
}
?>
<html>
    <head>
        <title>Reports</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="shortcut icon" href="img/logo.png"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" href="css/select-jquery.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/calculate-cpa.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <link href="css/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
        <script src='js/select-jquery.js'></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function(){
                $('#datepicker').datepicker({
                    inline: true,
                    showOtherMonths: true,
                    dateFormat: 'yy-mm-dd',
                    dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    firstDay: 1
			     });
                $('#datepicker1').datepicker({
                    inline: true,
                    showOtherMonths: true,
                    dateFormat: 'yy-mm-dd',
                    dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    firstDay: 1
                });
            });    
        </script>
        <style>
            .top-menu-bg li{
                font-family: 'Oswald', serif;
                font-weight: normal;
            }
        </style>
        
    </head>
    <body>
        <?php include_once("analyticstracking.php") ?>  
        <div class="top-header">
            <div class="header-logo">
                <?php
                if (isset($_COOKIE['IBProfilePassword'])) {
                    echo '<a href="../public/accountdetails.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                } else {
                    echo '<a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                }
                ?>
                <!--                <a href="../public/index.php">
                                    <img class="header-logo" src="../public/img/logo-small.png" alt=""/>
                                </a>-->
            </div>
            <?php include('../includes/social-icons.php'); ?>
        </div>
        <div class="container-fluid account-details">
            <div class="row">
                <div class="col-lg-3 col-lg-push-9 text-right login-info">
                    <p>Hello&#58;</p>
                    <h3 style="color: #13869E;"><?php echo $_SESSION['username']; ?></h3>
                    <form method="post" action="logout.php">
                        <button name="LOGOUT" type="submit" class="btn btn-primary">Log Out</button>
                    </form>
                </div>
                <!-- Start Menu -->
                <?php include('../includes/menu-admin.php'); ?>
            </div>
        </div>
        
        <img style='display: none;' src="img/arrowUp.png" class="backToTop" alt="arrow"/>
        <!--STARTING CONTENT-->
        <div class="container-fluid">
            <div class="row shadowContainer">
                <div id="printTable" class="col-md-12">
                    <?php
                        $start = date("Y/m/d");
                        $end = new DateTime("+1 months");
                    ?>
                    <form method="post" class="chooseDateForm">
                        <label>Start date:&nbsp;</label>
                        <input class="form-control report-input startDateChoose" type="text" id="datepicker" name="date1" >
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        <label style="margin-top: 20px;">End date:&nbsp;</label> 
                        <input class="form-control report-input endDateChoose"  type="text" id="datepicker1" name="date2" >
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        <input class="report-btn getReportButton" type="submit" name="getReport" value="Get Report">
                    </form>
                    <?php
                        if (isset($_POST['getReport'])) {

                            $start = $_POST['date1'];
                            $end = $_POST['date2'];
                            $_SESSION['startDate'] = $_POST['date1'];
                            $_SESSION['endDate'] = $_POST['date2'];
                          
                            $sendingToken = $client->GetUserDetails(array("token" => $_SESSION['TOKEN']))->GetUserDetailsResult;
                            $partnerType = $sendingToken->PartnerType;
                            
                            if($partnerType == 'Junior Partner'){
                             //prosledjivanje podataka u web servise funkcija web servise CPAReport CalculateCPA(int PartnerId, DateTime start, DateTime end)
                            $reports = $client->CalculateCPA(array("PartnerId" => $_SESSION['UserID'], "start" => $start, "end" => $end))->CalculateCPAResult;
                            
                            // here we can give the value to the variables from web services from $reports which presents the value taken from $client ($client = new SoapClient(URL);)
                            //example: $countFTD will get value from web services for data CountFTD
                            $countFTD = $reports->CountFTD;
                            $_SESSION['countFTD'] = $countFTD;

                            $totalCPA = $reports->TotalCPA;
                            $_SESSION['totalCPA'] = $totalCPA;

                            $extraCPA = $reports->ExtraCPA;
                            $_SESSION['extraCPA'] = $extraCPA;
                            
                            $countSA = $reports->CountSA;
                            $_SESSION['countSA'] = $countSA;
                            
                            $SACPA = $reports->SACPA;
                            $_SESSION['SACPA'] = $SACPA;
                            
                            $CountClients = $reports->CountClients;
                            $_SESSION['CountClients'] = $CountClients;
                            
                            
                            
                            $message = $reports->Message;
                            $_SESSION['message'] = $message;

                            if ($message != "") {
                                echo "<div class='messageText'><span class='fa fa-exclamation-circle'></span>&nbsp;&nbsp;&nbsp;  $message!</div>";
                            }
                            
                            $items = $reports->Items->CPAItem;
                            
                             if (isset($items)) { ?>
                                <table id="printTable" class="reportTable">
                                    <tr class="headerTable">
                                        <th>DATE</th>
                                        <th>AMOUNT</th>
                                        <th>DESCRIPTION</th>
                                        <th>COUNTRY</th>
                                        <th>FTD</th>
                                    </tr>
                                    <?php
                                    if (count($items) == 1) {
                                        
                                        $date = explode('T', $items->Time);
                                        $formated = explode('-', $date[0]);
                                  
                                    
                                        echo '<tr>';
                                        echo "<td>" . $formated[2] . "/" . $formated[1] . "/" . $formated[0] . "</td>";

                                        echo "<td>" . $items->Amount . "</td>";
                                        $_SESSION['amount'] = $items->Amount;

                                        echo "<td>" . $items->Description . "</td>";
                                        $_SESSION['description'] = $items->Description;

                                        echo "<td>" . $items->Country . "</td>";
                                        $_SESSION['country'] = $items->Country;


                                        if($items->FTD == TRUE) {
                                            echo "<td>YES</td>";
                                        $_SESSION['FTD'] = $items->FTD;
                                        } else {
                                            echo "<td>NO</td>";
                                        }


                                        echo '</tr></table>';
                                        
                                       
                                   ?>
                                            <div class="parentDivReport">
                                                <div class="parentHeader">
                                                    <p>CountFTD</p>
                                                    <p class="extraCPA">ExtraCPA</p>
                                                    <p>TotaltCPA</p>
                                                    <p>Number of clients</p>
                                                </div>
                                                <div class="data">
                                                    <div class="countFTD">
                                                        <?php echo $countFTD; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $extraCPA; ?>
                                                    </div>
                                                    <div class="totalCPA">
                                                        <?php echo $totalCPA; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $CountClients; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <table id="printTable" class="reportTable">        
                                                <tr class="headerTable">          
                                                    <th>Number of subaffiliates</th>
                                                    <th>Subaffiliates CPA</th>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $countSA; ?></td>
                                                    <td><?php echo $SACPA; ?></td>
                                                </tr>
                                            </table>
                                     
                                
                                        <form class="print-report" method="post" action="FILETOPDF.php" target="_blank">
                                            <input type="text" name="fName" value="<?php echo $_SESSION['IBFname']; ?>" style="display: none;">
                                            <input type="text" name="lName" value="<?php echo $_SESSION['IBLname']; ?>" style="display: none;">
                                            <input type="text" name="countFTD" value="<?php echo $countFTD; ?>" style="display: none;">
                                            <input type="text" name="extraCPA" value="<?php echo $extraCPA; ?>" style="display: none;">
                                            <input type="text" name="totalCPA" value="<?php echo $totalCPA; ?>" style="display: none;">
                                            <input type="text" name="countSA" value="<?php echo $countSA; ?>" style="display: none;">
                                            <input type="text" name="SACPA" value="<?php echo $SACPA; ?>" style="display: none;">
                                            <input type="text" name="CountClients" value="<?php echo $CountClients; ?>" style="display: none;">
                                            <input type="text" name="start" value="<?php echo $_SESSION['startDate']; ?>" style="display: none;">
                                            <input type="text" name="end" value="<?php echo $_SESSION['endDate']; ?>" style="display: none;">
                                            <input type="text" name="numberOfItems" value="<?php echo count($items); ?>" style="display: none;">
                                            <?php
                                                echo "<input type='hidden' name='time' value='" . $items->Time . "' >";
                                                $_SESSION['time'] = $items->Time;
                                                echo "<input type='hidden' name='amount' value='" . $items->Amount . "' >";
                                                $_SESSION['amount'] = $items->Amount;
                                                echo "<input type='hidden' name='description' value='" . $items->Description . "' >";
                                                $_SESSION['description'] = $items->Description;
                                                echo "<input type='hidden' name='country' value='" . $items->Country . "' >";
                                                $_SESSION['country'] = $items->Country;
                                                echo "<input type='hidden' name='FTD' value='" . $items->FTD . "' >";
                                                $_SESSION['FTD'] = $items->FTD;
                                                $numbOfItems = $_SESSION['numberOfItems'];
                                            ?>
                                            <input class="report-btn PDFreport" type="submit" name="printToPDF" value="Get CPA PDF Report">
                                        </form> 
                                    <?php } else {
                                        for ($i = 0; $i < count($items); $i++) {
                                            $date = explode('T', $items[$i]->Time);
                                            $formated = explode('-', $date[0]);


                                            echo '<tr>';
                                            echo "<td>" . $formated[2] . "/" . $formated[1] . "/" . $formated[0] . "</td>";

                                            echo "<td>" . $items[$i]->Amount . "</td>";
                                            $_SESSION['amount'] = $items[$i]->Amount;

                                            echo "<td>" . $items[$i]->Description . "</td>";
                                            $_SESSION['description'] = $items[$i]->Description;

                                            echo "<td>" . $items[$i]->Country . "</td>";
                                            $_SESSION['country'] = $items[$i]->Country;


                                            if($items[$i]->FTD == TRUE){
                                            echo "<td>YES</td>";
                                            $_SESSION['FTD'] = $items[$i]->FTD;
                                            }else{
                                                echo "<td>NO</td>";
                                            }


                                            echo '</tr>';
                                        } 
                                        echo "</table>";
                                     ?>
                                            <div class="parentDivReport">
                                                <div class="parentHeader">
                                                    <p>CountFTD</p>
                                                    <p class="extraCPA">ExtraCPA</p>
                                                    <p>TotaltCPA</p>
                                                    <p>Number of clients</p>
                                                </div>
                                                <div class="data">
                                                    <div class="countFTD">
                                                        <?php echo $countFTD; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $extraCPA; ?>
                                                    </div>
                                                    <div class="totalCPA">
                                                        <?php echo $totalCPA; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $CountClients; ?>
                                                    </div>
                                                </div>
                                            </div> 
                                                     
                                            <table id="printTable" class="reportTable">        
                                                <tr class="headerTable">          
                                                    <th>Number of subaffiliates</th>
                                                    <th>Subaffiliates CPA</th>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $countSA;?></td>
                                                    <td><?php echo $SACPA;?></td>
                                                </tr>
                                            </table>
                                
                                       
                                        <form class="print-report" method="post" action="FILETOPDF.php" target="_blank">
                                            <input type="text" name="fName" value="<?php echo $_SESSION['IBFname']; ?>" style="display: none;">
                                            <input type="text" name="lName" value="<?php echo $_SESSION['IBLname']; ?>" style="display: none;">
                                            <input type="text" name="countFTD" value="<?php echo $countFTD; ?>" style="display: none;">
                                            <input type="text" name="extraCPA" value="<?php echo $extraCPA; ?>" style="display: none;">
                                            <input type="text" name="totalCPA" value="<?php echo $totalCPA; ?>" style="display: none;">
                                            <input type="text" name="countSA" value="<?php echo $countSA; ?>" style="display: none;">
                                            <input type="text" name="SACPA" value="<?php echo $SACPA; ?>" style="display: none;">
                                            <input type="text" name="CountClients" value="<?php echo $CountClients; ?>" style="display: none;">
                                            <input type="text" name="start" value="<?php echo $_SESSION['startDate']; ?>" style="display: none;">
                                            <input type="text" name="end" value="<?php echo $_SESSION['endDate']; ?>" style="display: none;">
                                            <input type="text" name="numberOfItems" value="<?php echo count($items); ?>" style="display: none;">
                                            <?php for ($i = 0; $i < count($items); $i++) {

                                                echo "<input type='hidden' name='time$i' value='" . $items[$i]->Time . "' >";
                                                $_SESSION['time'] = $items[$i]->Time;
                                                echo "<input type='hidden' name='amount$i' value='" . $items[$i]->Amount . "' >";
                                                $_SESSION['amount'] = $items[$i]->Amount;
                                                echo "<input type='hidden' name='description$i' value='" . $items[$i]->Description . "' >";
                                                $_SESSION['description'] = $items[$i]->Description;
                                                echo "<input type='hidden' name='country$i' value='" . $items[$i]->Country . "' >";
                                                $_SESSION['country'] = $items[$i]->Country;
                                                echo "<input type='hidden' name='FTD$i' value='" . $items[$i]->FTD . "' >";
                                                $_SESSION['FTD'] = $items[$i]->FTD;
                                                //var_dump($items[$i]->Country);
                                                $numbOfItems = $_SESSION['numberOfItems'];
                                            } ?>

                                            <input class="report-btn PDFreport" type="submit" name="printToPDF" value="Get CPA PDF Report">
                                        </form><?php
                                
                                    }
                            } else {?>
                                <div class="parentDivReport">
                                    <div class="parentHeader">
                                        <p>CountFTD</p>
                                        <p class="extraCPA">ExtraCPA</p>
                                        <p>TotaltCPA</p>
                                        <p>Number of clients</p>
                                    </div>
                                    <div class="data">
                                        <div class="countFTD">
                                            <?php echo $countFTD; ?>
                                        </div>
                                        <div>
                                            <?php echo $extraCPA; ?>
                                        </div>
                                        <div class="totalCPA">
                                            <?php echo $totalCPA; ?>
                                        </div>
                                        <div>
                                            <?php echo $CountClients; ?>
                                        </div>
                                    </div>
                                </div> 
                            <?php }
                            
                            } else if($partnerType == 'Senior Partner') {
                              
                                $reportInput = $client3->GetPartnerAccounts(array("PartnerId" =>314))->GetPartnerAccountsResult;
                                $countClients = $reportInput->ClientCount;
                                $_SESSION['countClients'] = $countClients;
                                
                                
                                if(!is_array($reportInput->TradeAccounts->string)) {
                                    $reportInput->TradeAccounts->string = array($reportInput->TradeAccounts->string);
                                } 
                                
                                $tradeAccounts = $reportInput->TradeAccounts->string;
                                
                                if($tradeAccounts !=0) {
                                    
                                    $conn = mysqli_connect("Reports.leveratetech.com", "stockstp", "c7td2UqSjrvCXP6J", "stockstp_real");
                                
                                    echo "<table id='printTable' class='reportTable'>";
                                        echo "<thead class='headerTable'>";
                                            echo "<th>#</th>";
                                            echo "<th>Account</th>";
                                            echo "<th>Volume</th>";
                                            echo "<th>Commission</th>";
                                        echo "</thead>";
                                        echo "<tbody>";

                                        $totalCommission = 0;
                                        $_SESSION['account'] = array();
                                        $_SESSION['comission'] = array();
                                        $_SESSION['volume'] = array();


                                        for ($i=0; $i<count($tradeAccounts); $i++) {

                                            $query = "select sum(volume) from mt4_trades where (cmd=0 or cmd=1) and close_time>='$start' and close_time<='$end' and login=$tradeAccounts[$i]";
                                            $res = $conn->query($query);
                                            
                                            if ($res != NULL) {
                                                
                                                while ($row = $res->fetch_row()) {
                                                    $itemVolume = $row[0]/100;
                                                    if ($itemVolume == NULL) {
                                                        $itemVolume = 0;
                                                    }
                                                    if ($itemVolume < 50) {
                                                        $itemCommission = (5 * $itemVolume);
                                                    } else if ($itemVolume > 49 && $itemVolume < 100) {
                                                        $itemCommission = (7.5 * $itemVolume);
                                                    } else if ($itemVolume > 99 && $itemVolume < 500) {
                                                        $itemCommission = (10 * $itemVolume);
                                                    } else {
                                                        $itemCommission = (12 * $itemVolume);
                                                    }
                                               }
                                                $totalCommission = $totalCommission + $itemCommission;
                                                echo "<tr><td>" . ($i+1) . "</td><td>$tradeAccounts[$i]</td><td>$itemVolume</td><td>$itemCommission</td></tr>";

                                                array_push($_SESSION['account'], $tradeAccounts[$i]);
                                                array_push($_SESSION['comission'], $itemCommission);
                                                array_push($_SESSION['volume'], $itemVolume);

                                            } else {
                                                $totalCommission = 0;
                                                $itemVolume = 0;
                                                $itemCommission = 0;
                                                $_SESSION['account'] = $tradeAccounts;
                                                $_SESSION['comission'] = $itemCommission;
                                                $_SESSION['volume'] = $itemVolume;
                                                
                                            }
                                            
                                        }
                                        echo "</tbody>";
                                    echo "</table>";

                                    echo "<table class='reportTable'>";
                                        echo "<thead class='headerTable blue-table'>";
                                            echo "<th>Number of clients</th>";
                                            echo "<th>Total Commission</th>";
                                        echo "</thead>";
                                        echo "<tbody class='data'>";
                                            echo "<tr><td>$countClients</td><td>$totalCommission</td></tr>";
                                        echo "</tbody>";
                                    echo "</table>";

                                    $_SESSION['countClientsIB'] = $countClients;

                                    ?>
                                    <form class="print-report" method="post" action="FILETOPDFIB.php" target="_blank">
                                        <input type="text" name="fName" value="<?php echo $_SESSION['IBFname']; ?>" style="display: none;">
                                        <input type="text" name="lName" value="<?php echo $_SESSION['IBLname']; ?>" style="display: none;">
                                        <input type="text" name="TotalComission" value="<?php echo $totalCommission; ?>" style="display: none;">
                                        <input type="text" name="CountClientsIB" value="<?php echo $countClients; ?>" style="display: none;">
                                        <input type="text" name="start" value="<?php echo $_SESSION['startDate']; ?>" style="display: none;">
                                        <input type="text" name="end" value="<?php echo $_SESSION['endDate']; ?>" style="display: none;">
                                        <?php
                                            
                                            if(count($tradeAccounts)==1 && $tradeAccounts[0] == NULL) {
                                                $numberOfItems = 0;
                                            } else {
                                                $numberOfItems = count($tradeAccounts);
                                            }
                                    
                                        ?>
                                        <input type="text" name="numberOfItemsIB" value="<?php echo $numberOfItems; ?>" style="display: none;">
                                        
                                        <?php
                                                 for ($i = 0; $i < count($_SESSION['account']); $i++) {
                                                 
                                                        echo "<input type='hidden' name='account$i' value='" . $_SESSION['account'][$i] . "' >";
                                                        echo "<input type='hidden' name='comission$i' value='" . $_SESSION['comission'][$i] . "' >";
                                                        echo "<input type='hidden' name='volume$i' value='" . $_SESSION['volume'][$i] . "' >";  
                                                    
                                                    
                                                   
                                                } 
                                        ?>
                                        <input class="report-btn PDFreport" type="submit" name="printToPDF" value="Get PDF IB Report">
                                    </form>
                    
                              
                                
                                <?php
                              
                          }   
                                 
                            } else if($partnerType == 'Hybrid Partner'){
                             //prosledjivanje podataka u web servise funkcija web servise CPAReport CalculateCPA(int PartnerId, DateTime start, DateTime end)
                            $reports = $client->CalculateCPA(array("PartnerId" => $_SESSION['UserID'], "start" => $start, "end" => $end))->CalculateCPAResult;
                            
                            // here we can give the value to the variables from web services from $reports which presents the value taken from $client ($client = new SoapClient(URL);)
                            //example: $countFTD will get value from web services for data CountFTD
                            $countFTD = $reports->CountFTD;
                            $_SESSION['countFTD'] = $countFTD;

                            $totalCPA = $reports->TotalCPA;
                            $_SESSION['totalCPA'] = $totalCPA;

                            $extraCPA = $reports->ExtraCPA;
                            $_SESSION['extraCPA'] = $extraCPA;
                            
                            $countSA = $reports->CountSA;
                            $_SESSION['countSA'] = $countSA;
                            
                            $SACPA = $reports->SACPA;
                            $_SESSION['SACPA'] = $SACPA;
                            
                            $CountClients = $reports->CountClients;
                            $_SESSION['CountClients'] = $CountClients;
                            
                            
                            
                            $message = $reports->Message;
                            $_SESSION['message'] = $message;

                            if ($message != "") {
                                echo "<div class='messageText'><span class='fa fa-exclamation-circle'></span>&nbsp;&nbsp;&nbsp;  $message!</div>";
                            }
                            
                            $items = $reports->Items->CPAItem;
                            
                             if (isset($items)) { ?>
                                <table id="printTable" class="reportTable">
                                    <tr class="headerTable">
                                        <th>DATE</th>
                                        <th>AMOUNT</th>
                                        <th>DESCRIPTION</th>
                                        <th>COUNTRY</th>
                                        <th>FTD</th>
                                    </tr>
                                    <?php
                                    if (count($items) == 1) {
                                        
                                        $date = explode('T', $items->Time);
                                        $formated = explode('-', $date[0]);
                                  
                                    
                                        echo '<tr>';
                                        echo "<td>" . $formated[2] . "/" . $formated[1] . "/" . $formated[0] . "</td>";

                                        echo "<td>" . $items->Amount . "</td>";
                                        $_SESSION['amount'] = $items->Amount;

                                        echo "<td>" . $items->Description . "</td>";
                                        $_SESSION['description'] = $items->Description;

                                        echo "<td>" . $items->Country . "</td>";
                                        $_SESSION['country'] = $items->Country;


                                        if($items->FTD == TRUE) {
                                            echo "<td>YES</td>";
                                        $_SESSION['FTD'] = $items->FTD;
                                        } else {
                                            echo "<td>NO</td>";
                                        }


                                        echo '</tr></table>';
                                        
                                       
                                   ?>
                                            <div class="parentDivReport">
                                                <div class="parentHeader">
                                                    <p>CountFTD</p>
                                                    <p class="extraCPA">ExtraCPA</p>
                                                    <p>TotaltCPA</p>
                                                    <p>Number of clients</p>
                                                </div>
                                                <div class="data">
                                                    <div class="countFTD">
                                                        <?php echo $countFTD; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $extraCPA; ?>
                                                    </div>
                                                    <div class="totalCPA">
                                                        <?php echo $totalCPA; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $CountClients; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <table id="printTable" class="reportTable">        
                                                <tr class="headerTable">          
                                                    <th>Number of subaffiliates</th>
                                                    <th>Subaffiliates CPA</th>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $countSA; ?></td>
                                                    <td><?php echo $SACPA; ?></td>
                                                </tr>
                                            </table>
                                     
                                
                                        <form class="print-report" method="post" action="FILETOPDF.php" target="_blank">
                                            <input type="text" name="fName" value="<?php echo $_SESSION['IBFname']; ?>" style="display: none;">
                                            <input type="text" name="lName" value="<?php echo $_SESSION['IBLname']; ?>" style="display: none;">
                                            <input type="text" name="countFTD" value="<?php echo $countFTD; ?>" style="display: none;">
                                            <input type="text" name="extraCPA" value="<?php echo $extraCPA; ?>" style="display: none;">
                                            <input type="text" name="totalCPA" value="<?php echo $totalCPA; ?>" style="display: none;">
                                            <input type="text" name="countSA" value="<?php echo $countSA; ?>" style="display: none;">
                                            <input type="text" name="SACPA" value="<?php echo $SACPA; ?>" style="display: none;">
                                            <input type="text" name="CountClients" value="<?php echo $CountClients; ?>" style="display: none;">
                                            <input type="text" name="start" value="<?php echo $_SESSION['startDate']; ?>" style="display: none;">
                                            <input type="text" name="end" value="<?php echo $_SESSION['endDate']; ?>" style="display: none;">
                                            <input type="text" name="numberOfItems" value="<?php echo count($items); ?>" style="display: none;">
                                            <?php
                                                echo "<input type='hidden' name='time' value='" . $items->Time . "' >";
                                                $_SESSION['time'] = $items->Time;
                                                echo "<input type='hidden' name='amount' value='" . $items->Amount . "' >";
                                                $_SESSION['amount'] = $items->Amount;
                                                echo "<input type='hidden' name='description' value='" . $items->Description . "' >";
                                                $_SESSION['description'] = $items->Description;
                                                echo "<input type='hidden' name='country' value='" . $items->Country . "' >";
                                                $_SESSION['country'] = $items->Country;
                                                echo "<input type='hidden' name='FTD' value='" . $items->FTD . "' >";
                                                $_SESSION['FTD'] = $items->FTD;
                                                $numbOfItems = $_SESSION['numberOfItems'];
                                            ?>
                                            <input class="report-btn PDFreport" type="submit" name="printToPDF" value="Get PDF Report">
                                        </form> 
                                    <?php } else {
                                        for ($i = 0; $i < count($items); $i++) {
                                            $date = explode('T', $items[$i]->Time);
                                            $formated = explode('-', $date[0]);


                                            echo '<tr>';
                                            echo "<td>" . $formated[2] . "/" . $formated[1] . "/" . $formated[0] . "</td>";

                                            echo "<td>" . $items[$i]->Amount . "</td>";
                                            $_SESSION['amount'] = $items[$i]->Amount;

                                            echo "<td>" . $items[$i]->Description . "</td>";
                                            $_SESSION['description'] = $items[$i]->Description;

                                            echo "<td>" . $items[$i]->Country . "</td>";
                                            $_SESSION['country'] = $items[$i]->Country;


                                            if($items[$i]->FTD == TRUE){
                                            echo "<td>YES</td>";
                                            $_SESSION['FTD'] = $items[$i]->FTD;
                                            }else{
                                                echo "<td>NO</td>";
                                            }


                                            echo '</tr>';
                                        } 
                                        echo "</table>";
                                     ?>
                                            <div class="parentDivReport">
                                                <div class="parentHeader">
                                                    <p>CountFTD</p>
                                                    <p class="extraCPA">ExtraCPA</p>
                                                    <p>TotaltCPA</p>
                                                    <p>Number of clients</p>
                                                </div>
                                                <div class="data">
                                                    <div class="countFTD">
                                                        <?php echo $countFTD; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $extraCPA; ?>
                                                    </div>
                                                    <div class="totalCPA">
                                                        <?php echo $totalCPA; ?>
                                                    </div>
                                                    <div>
                                                        <?php echo $CountClients; ?>
                                                    </div>
                                                </div>
                                            </div> 
                                                     
                                            <table id="printTable" class="reportTable">        
                                                <tr class="headerTable">          
                                                    <th>Number of subaffiliates</th>
                                                    <th>Subaffiliates CPA</th>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $countSA;?></td>
                                                    <td><?php echo $SACPA;?></td>
                                                </tr>
                                            </table>
                                
                                       
                                        <form class="print-report" method="post" action="FILETOPDF.php" target="_blank">
                                            <input type="text" name="fName" value="<?php echo $_SESSION['IBFname']; ?>" style="display: none;">
                                            <input type="text" name="lName" value="<?php echo $_SESSION['IBLname']; ?>" style="display: none;">
                                            <input type="text" name="countFTD" value="<?php echo $countFTD; ?>" style="display: none;">
                                            <input type="text" name="extraCPA" value="<?php echo $extraCPA; ?>" style="display: none;">
                                            <input type="text" name="totalCPA" value="<?php echo $totalCPA; ?>" style="display: none;">
                                            <input type="text" name="countSA" value="<?php echo $countSA; ?>" style="display: none;">
                                            <input type="text" name="SACPA" value="<?php echo $SACPA; ?>" style="display: none;">
                                            <input type="text" name="CountClients" value="<?php echo $CountClients; ?>" style="display: none;">
                                            <input type="text" name="start" value="<?php echo $_SESSION['startDate']; ?>" style="display: none;">
                                            <input type="text" name="end" value="<?php echo $_SESSION['endDate']; ?>" style="display: none;">
                                            <input type="text" name="numberOfItems" value="<?php echo count($items); ?>" style="display: none;">
                                            <?php for ($i = 0; $i < count($items); $i++) {

                                                echo "<input type='hidden' name='time$i' value='" . $items[$i]->Time . "' >";
                                                $_SESSION['time'] = $items[$i]->Time;
                                                echo "<input type='hidden' name='amount$i' value='" . $items[$i]->Amount . "' >";
                                                $_SESSION['amount'] = $items[$i]->Amount;
                                                echo "<input type='hidden' name='description$i' value='" . $items[$i]->Description . "' >";
                                                $_SESSION['description'] = $items[$i]->Description;
                                                echo "<input type='hidden' name='country$i' value='" . $items[$i]->Country . "' >";
                                                $_SESSION['country'] = $items[$i]->Country;
                                                echo "<input type='hidden' name='FTD$i' value='" . $items[$i]->FTD . "' >";
                                                $_SESSION['FTD'] = $items[$i]->FTD;
                                                //var_dump($items[$i]->Country);
                                                $numbOfItems = $_SESSION['numberOfItems'];
                                            } ?>

                                            <input class="report-btn PDFreport" type="submit" name="printToPDF" value="Get CPA Report">
                                        </form><?php
                                
                                    }
                            } else {?>
                                <div class="parentDivReport">
                                    <div class="parentHeader">
                                        <p>CountFTD</p>
                                        <p class="extraCPA">ExtraCPA</p>
                                        <p>TotaltCPA</p>
                                        <p>Number of clients</p>
                                    </div>
                                    <div class="data">
                                        <div class="countFTD">
                                            <?php echo $countFTD; ?>
                                        </div>
                                        <div>
                                            <?php echo $extraCPA; ?>
                                        </div>
                                        <div class="totalCPA">
                                            <?php echo $totalCPA; ?>
                                        </div>
                                        <div>
                                            <?php echo $CountClients; ?>
                                        </div>
                                    </div>
                                </div> 
                        <?php }   
                        
                             
                                $reportInput = $client3->GetPartnerAccounts(array("PartnerId" => $_SESSION['UserID']))->GetPartnerAccountsResult;
                                $countClients = $reportInput->ClientCount;
                                $_SESSION['countClients'] = $countClients;
                                
                                
                                if(!is_array($reportInput->TradeAccounts->string)) {
                                    $reportInput->TradeAccounts->string = array($reportInput->TradeAccounts->string);
                                } 
                                
                                $tradeAccounts = $reportInput->TradeAccounts->string;
                                
                                if($tradeAccounts !=0) {
                                    
                                    $conn = mysqli_connect("Reports.leveratetech.com", "stockstp", "c7td2UqSjrvCXP6J", "stockstp_real");
                                
                                    echo "<table id='printTable' class='reportTable'>";
                                        echo "<thead class='headerTable'>";
                                            echo "<th>#</th>";
                                            echo "<th>Account</th>";
                                            echo "<th>Volume</th>";
                                            echo "<th>Commission</th>";
                                        echo "</thead>";
                                        echo "<tbody>";

                                        $totalCommission = 0;
                                        $_SESSION['account'] = array();
                                        $_SESSION['comission'] = array();
                                        $_SESSION['volume'] = array();


                                        for ($i=0; $i<count($tradeAccounts); $i++) {

                                            $query = "select sum(volume) from mt4_trades where (cmd=0 or cmd=1) and close_time>='$start' and close_time<='$end' and login=$tradeAccounts[$i]";
                                            $res = $conn->query($query);
                                            
                                            if ($res != NULL) {
                                                
                                                while ($row = $res->fetch_row()) {
                                                    $itemVolume = $row[0]/100;
                                                    if ($itemVolume == NULL) {
                                                        $itemVolume = 0;
                                                    }
                                                    if ($itemVolume < 50) {
                                                        $itemCommission = (5 * $itemVolume);
                                                    } else if ($itemVolume > 49 && $itemVolume < 100) {
                                                        $itemCommission = (7.5 * $itemVolume);
                                                    } else if ($itemVolume > 99 && $itemVolume < 500) {
                                                        $itemCommission = (10 * $itemVolume);
                                                    } else {
                                                        $itemCommission = (12 * $itemVolume);
                                                    }
                                               }
                                                $totalCommission = $totalCommission + $itemCommission;
                                                echo "<tr><td>" . ($i+1) . "</td><td>$tradeAccounts[$i]</td><td>$itemVolume</td><td>$itemCommission</td></tr>";

                                                array_push($_SESSION['account'], $tradeAccounts[$i]);
                                                array_push($_SESSION['comission'], $itemCommission);
                                                array_push($_SESSION['volume'], $itemVolume);

                                            } else {
                                                $totalCommission = 0;
                                                $itemVolume = 0;
                                                $itemCommission = 0;
                                                $_SESSION['account'] = $tradeAccounts;
                                                $_SESSION['comission'] = $itemCommission;
                                                $_SESSION['volume'] = $itemVolume;
                                                
                                            }
                                            
                                        }
                                        echo "</tbody>";
                                    echo "</table>";

                                    echo "<table class='reportTable'>";
                                        echo "<thead class='headerTable blue-table'>";
                                            echo "<th>Number of clients</th>";
                                            echo "<th>Total Commission</th>";
                                        echo "</thead>";
                                        echo "<tbody class='data'>";
                                            echo "<tr><td>$countClients</td><td>$totalCommission</td></tr>";
                                        echo "</tbody>";
                                    echo "</table>";

                                    $_SESSION['countClientsIB'] = $countClients;

                                    ?>
                                    <form class="print-report" method="post" action="FILETOPDFIB.php" target="_blank">
                                        <input type="text" name="fName" value="<?php echo $_SESSION['IBFname']; ?>" style="display: none;">
                                        <input type="text" name="lName" value="<?php echo $_SESSION['IBLname']; ?>" style="display: none;">
                                        <input type="text" name="TotalComission" value="<?php echo $totalCommission; ?>" style="display: none;">
                                        <input type="text" name="CountClientsIB" value="<?php echo $countClients; ?>" style="display: none;">
                                        <input type="text" name="start" value="<?php echo $_SESSION['startDate']; ?>" style="display: none;">
                                        <input type="text" name="end" value="<?php echo $_SESSION['endDate']; ?>" style="display: none;">
                                        <?php
                                            
                                            if(count($tradeAccounts)==1 && $tradeAccounts[0] == NULL) {
                                                $numberOfItems = 0;
                                            } else {
                                                $numberOfItems = count($tradeAccounts);
                                            }
                                    
                                        ?>
                                        <input type="text" name="numberOfItemsIB" value="<?php echo $numberOfItems; ?>" style="display: none;">
                                        
                                        <?php
                                             for ($i = 0; $i < count($_SESSION['account']); $i++) {
                                                 
                                                        echo "<input type='hidden' name='account$i' value='" . $_SESSION['account'][$i] . "' >";
                                                        echo "<input type='hidden' name='comission$i' value='" . $_SESSION['comission'][$i] . "' >";
                                                        echo "<input type='hidden' name='volume$i' value='" . $_SESSION['volume'][$i] . "' >";  
                                                    
                                                    
                                                   
                                                } 
                                        ?>
                                        <input class="report-btn PDFreport" type="submit" name="printToPDF" value="Get PDF IB Report">
                                    </form>

                                
                                
                                
                                <?php
                              
                                }
                                 
                                
                                
                        }
                        
                        }else {
                        echo "<div class='noItems'> <span class='glyphicon glyphicon-warning-sign'></span>&nbsp;&nbsp;&nbsp; CHOOSE DATE RANGE </div>";
                    } ?>
                                
                </div>
            </div>
        </div>


        <?php include('../includes/footer.php'); ?>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/scrollToTop.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(".activeReports span").css("color", "#ff9900");
           
      </script>
    </body>
</html>
