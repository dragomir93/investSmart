<?php

session_start();

$_SESSION = array();

if (isset($_COOKIE['IBProfileUsername']) && isset($_COOKIE['IBProfilePassword'])) {
    setcookie("IBProfileUsername", "", time() - 3600, '/');
    setcookie("IBProfilePassword", '', time() - 3600, '/');
}

session_destroy();

header("location: login.php");
exit();
