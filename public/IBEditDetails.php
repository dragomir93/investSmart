<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);
$updateObj = new stdClass();

$updateObj->FirstName = $_POST['firstName'];
$updateObj->LastName = $_POST['lastName'];
$updateObj->Phone = '+' . ltrim($_POST['phone']);
$updateObj->Mobile = '+' . ltrim($_POST['mobile']);
$updateObj->Country = $_POST['country'];
$updateObj->CityRegion = $_POST['city'];
$updateObj->Address = $_POST['address'];
$updateObj->Zip = $_POST['zip'];
$updateObj->Company = $_POST['company'];
$updateObj->WebsiteUrl = $_POST['url'];
$updateObj->PartnerType = $_POST['partnerType'];
$token = $_POST['token'];

$updateResult = $client->UpdateUser(array('token' => $token, 'input' => $updateObj))->UpdateUserResult;
$updateMessage = $updateResult->Message;
$updateSuccess = $updateResult->Success;

if ($updateSuccess == TRUE) {
    echo "<div class='updateTrue' style='color:green;'>USER DETAILS UPDATED SUCCESSFULLY! Refresh page to see changes!</div>";
} else {
    echo "<div class='updateFalse' style='color:red;'>" . $updateMessage . "</div>";
}
?>
<html>
    <body>
       <?php include_once("analyticstracking.php") ?>  
        <script type="text/javascript">
           var res = "<?php echo $updateSuccess;?>";
                if(res == true){
                   $(".updatingDetailsAnimation").attr("hidden",true);
                }
                else{
                    $(".updatingDetailsAnimation").attr("hidden",true);
                }
        </script>
    </body>
</html>
