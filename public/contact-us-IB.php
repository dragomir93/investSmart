<?php
session_start();
error_reporting(0);
//NOT TO ALLOW APPROACH IF THERE IS NO TOKEN
if ($_SESSION['IBFname'] == "" || $_SESSION['username'] == "") {
//    setcookie("IBProfilePassword", '' , time() - 3600,'/');
    header("location: login.php"); //back to login page
}
?>
<html>
    <head>
        <title>Contact Us</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" href="css/select-jquery.css"/>
        <link rel="stylesheet" type="text/css" href="css/contact-us-IB.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    </head>
    <body>
        <?php include_once("analyticstracking.php") ?>  
        <div class="top-header">
            <div class="header-logo">
                <?php
                if (isset($_COOKIE['IBProfilePassword'])) {
                    echo '<a href="../public/accountdetails.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                } else {
                    echo '<a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                }
                ?>
                <!--                <a href="../public/index.php">
                                    <img class="header-logo" src="../public/img/logo-small.png" alt="logo"/>
                                </a>-->
            </div>
            <?php include('../includes/social-icons.php'); ?>
        </div>
        <div class="container-fluid account-details">
            <div class="row">
                <div class="col-lg-3 col-lg-push-9 text-right login-info">
                    <p>Hello&#58;</p>
                    <h3 style="color: #13869E;"><?php echo $_SESSION['username']; ?></h3>
                    <form method="post" action="logout.php">
                        <button name="LOGOUT" type="submit" class="btn btn-primary">Log Out</button>
                    </form>
                </div>
                <!-- Start Menu -->
                <?php include('../includes/menu-admin.php'); ?>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row background shadowContainer">
                <div class="contactFormContainer col-md-6">
                    <h1 class="animated fadeInDown">Message Us</h1>
                    <p class="animated fadeInDown">Contact our support team by filling out the form below, 
                        and we’ll get back to you within one business day or less.</p>
                    <form id="contact-form"  method="post" role="form" name="SendMail">
                        <div class="form-group animated fadeInDown contact-ib">
                            <label for="">First Name</label>
                            <input type="text" class="form-control " id="Fname" name="Fname" required placeholder="First name *" value="<?php echo $_SESSION['IBFname'] ?>" readonly pattern="[A-Z][a-z]+" title="John">
                        </div>
                        <div class="form-group animated fadeInDown contact-ib">
                            <label for="">Last Name</label>
                            <input type="text" class="form-control " id="Lname" name="Lname" required placeholder="Last name *" value="<?php echo $_SESSION['IBLname'] ?>" readonly pattern="[A-Z][a-z]+" title="Smith">
                        </div>
                        <div class="form-group animated fadeInDown contact-ib">
                            <label for="">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" required placeholder="Email *" value="<?php echo $_SESSION['username'] ?>" readonly pattern="([a-z0-9]{1,})+(?:.[a-z0-9]{1,})+@([a-z]{1,}\.[a-z]{1,})+$" title="example@yahoo.com">
                        </div>
                        <div class="form-group animated fadeInDown contact-ib">
                            <label for="">Phone</label>
                            <input type="text" class="form-control " id="IBPhone" name="IBPhone" required placeholder="Phone *" value="<?php echo $_SESSION['IBPhone'] ?>" readonly>
                        </div>
                        <div class="form-group animated fadeInDown contact-ib" >
                            <label for="" >Subject</label>
                            <input type="text" class="form-control" id="subject"  name="subject" required placeholder="Subject *">
                        </div>
                        <div class="form-group animated fadeInDown contact-ib" >
                            <label for=""  >Message</label>
                            <textarea class="form-control " rows="5" id='message' name="message" required placeholder="Your message *"></textarea>
                        </div>
                        <div class="form-groub animated fadaInDown">
                            <button type="submit" id="sendMail" name="sendMessage"  class="btn btn-default animated fadeInDown">Submit</button>
                        </div>
                        
                    </form>
                    <div class="sendingMailAnimation" hidden>
                        <i class="fa fa-spinner" ></i>
                    </div>
                    <div id="mailResult" style="text-align: center;">

                    </div>

                </div>
                
                <div class="infoText col-md-6">
                    <h1 class="animated fadeInDown">Contact Us</h1>
                    <p class="animated fadeInDown">
                        Please, be free to ask any question about STP Affiliates You have.<br><br>
                        We're happy to answer any question You have.<br><br>
                        Use the contact form to ask question or use the email below.
                    </p>
                    <h3 class="animated fadeInDown">e-mail address:</h3>
                    <h4 class="animated fadeInDown">stpaffiliate@support.com</h4>
                    <div class="animated fadeInDown"> 
                        <a href='#'><i class="fa fa-facebook animated fadeInDown" aria-hidden="true"></i></a>
                        <a href='#'><i class="fa fa-twitter animated fadeInDown" aria-hidden="true"></i></a>
                        <a href='#'><i class="fa fa-google-plus animated fadeInDown" aria-hidden="true"></i></a>
                        <a href='#'><i class="fa fa-youtube-play animated fadeInDown" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <!-- / Information Box -->
        <?php include('../includes/footer.php'); ?>

        <!-- Include JS File Here -->
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
        <!--<script src='js/select-jquery.js'></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/IB-contact-validation.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(".activeContact span").css("color", "#ff9900");
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#contact-form").submit(function (event) {
                    event.preventDefault();
                    $(".sendingMailAnimation").removeAttr("hidden");
                    $("#mailResult").empty();
                    var Fname = document.getElementById('Fname').value;
                    var Lname = document.getElementById('Lname').value;
                    var email = document.getElementById('email').value;
                    var IBPhone = document.getElementById('IBPhone').value;
                    var subject = document.getElementById('subject').value;
                    var message = document.getElementById('message').value;
                    var data = "Fname=" + Fname + "&Lname=" + Lname + "&email=" + email + "&IBPhone=" + IBPhone + "&subject=" + subject + "&message=" + message;
                    $.ajax({
                        type: 'post',
                        url: "mailingIB.php",
                        data: data,
                        cache: false,
                        success: function (html) {
                            $("#mailResult").html(html);
                            setTimeout(function () {
                                $("#mailResult").empty();
                            }, 5000);
                        }
                    });
                    return false;
                });
            });
        </script>
    </body>
</html>





