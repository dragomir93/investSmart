<?php

session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);

$oldPassword = $_POST['pass'];
$newPassword = $_POST['newPass'];
$token = $_POST['token'];

$changePasswordResult = $client->ChangePassword(array('token' => $token, 'oldPassword' => $oldPassword, 'newPassword' => $newPassword))->ChangePasswordResult;

$resultMessage = $changePasswordResult->Message;
$resultSuccess = $changePasswordResult->Success;

if ($resultSuccess == TRUE) {
    echo "<div class='updateTrue' style='color:green;'>PASSWORD UPDATED! Refresh page to see changes!</div>";
} else {
    echo "<div class='updateFalse' style='color:red;'>" . $resultMessage . "</div>";
}
?>
<html>
    <body>
         <?php include_once("analyticstracking.php") ?>  
        <script type="text/javascript">
           var res = "<?php echo $resultSuccess;?>";
                if(res == true){
                   $(".changePasswordAnimation").attr("hidden",true);
                   
                   $("#NewPassword").val("");
                   $("#confirmNewPassword").val("");
                   document.getElementById("confirmNewPassword").style.border="1px solid lightgray";
                }
                else{
                    $(".changePasswordAnimation").attr("hidden",true);
                }
        </script>
    </body>
</html>