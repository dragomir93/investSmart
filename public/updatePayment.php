<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);
$inputObj = new stdClass();

$inputObj -> Method = $_POST['method'];
$inputObj -> AccountNumber = $_POST['accountNumber'];
$inputObj -> BankName = $_POST['bankName'];
$inputObj -> BankAddress = $_POST['bankAddress'];
$inputObj -> BankCountry = $_POST['bankCountry'];
$inputObj -> BankState = $_POST['bankState'];
$inputObj -> Swift = $_POST['swift'];
$inputObj -> Iban = $_POST['iban'];
$inputObj -> Aba = $_POST['aba'];
$inputObj -> CorespondenceBank = $_POST['correspondence'];

$savePaymentDetails= $client->SavePaymentDetail(array('token'=>$_SESSION['TOKEN'],'input'=>$inputObj))->SavePaymentDetailResult;
$savePaymentMessage = $savePaymentDetails->Message;
$savePaymentSuccess = $savePaymentDetails->Success;

if($savePaymentSuccess==TRUE){
            echo "<div style='color:green;'>PAYMENT DETAILS UPDATED</div>";
}
else {
            echo "<div style='color:green;'>".$savePaymentMessage."</div>";         
}
?>
<html>
    <body>
         <?php include_once("analyticstracking.php") ?>  
        <script type="text/javascript">
           var res = "<?php echo $resultSuccess;?>";
                if(res == true){
                   $(".updatingPaymentAnimation").attr("hidden",true);
                }
                else{
                    $(".updatingPaymentAnimation").attr("hidden",true);
                }
        </script>
    </body>
</html>