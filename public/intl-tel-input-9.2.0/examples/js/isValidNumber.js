


var telInput = $("#phone"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");
//$("#submit").attr('hidden',true);

// initialise plugin
telInput.intlTelInput({
  utilsScript: "../../build/js/utils.js"
});

var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hide");
  validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hide");
      $('#phone').css("border","2px solid green");
      //$("#submit").removeAttr('hidden');
    } else {
      telInput.addClass("error");
      errorMsg.removeClass("hide");
      $('#phone').css("border","2px solid red");
      //$("#submit").attr('disabled',true);
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);