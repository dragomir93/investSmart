<?php
session_start();
error_reporting(0);
include '../configs/config.inc.php';
$client = new SoapClient(URL);
//TO REDIRECT ON MAIN PAGE IF LOG IN IS SUCCESSFULL
if (isset($_POST['LOGIN'])) {
    $logUSERNAME = $_POST['loginUSERNAME']; //from login.php
    $logPASSWORD = $_POST['loginPASSWORD'];
    $rememberMe = $_POST['remember-me'];

    setcookie("IBProfilePassword", $logPASSWORD, time() + (90 * 24 * 60 * 60), "/");

    if (isset($rememberMe)) {
        $send = $client->UserLogin(array("email" => $logUSERNAME, "password" => $logPASSWORD))->UserLoginResult;
        $sendToken = $send->Token;
        $sendMessage = $send->Message;
        $_SESSION['TOKEN'] = $sendToken;
        $_SESSION['message'] = $sendMessage;
        if ($_SESSION['TOKEN'] == "") {
            header("location: login.php"); //back to login page        
        } else {
            setcookie("IBProfileUsername", $logUSERNAME, time() + (90 * 24 * 60 * 60), "/");
            setcookie("IBProfilePassword", $logPASSWORD, time() + (90 * 24 * 60 * 60), "/");
        }
    } else {
        $send = $client->UserLogin(array("email" => $logUSERNAME, "password" => $logPASSWORD))->UserLoginResult;
        $sendToken = $send->Token;
        $sendMessage = $send->Message;
        $_SESSION['TOKEN'] = $sendToken;
        $_SESSION['message'] = $sendMessage;
        if ($_SESSION['TOKEN'] == "") {
            header("location: login.php"); //back to login page        
        }
    }
}
//GETTING INFO FROM WEB SERVICE FOR SPECIFIC USER
$sendingToken = $client->GetUserDetails(array("token" => $_SESSION['TOKEN']))->GetUserDetailsResult;
$_SESSION['UserID'] = $sendingToken->ID;
$firstName = $sendingToken->FirstName;
$lastName = $sendingToken->LastName;
$phone = $sendingToken->Phone;
$password = $sendingToken->Password;
$mobile = $sendingToken->Mobile;
$email = $sendingToken->Email;
$country = $sendingToken->Country;
$city = $sendingToken->CityRegion;
$address = $sendingToken->Address;
$ZIP = $sendingToken->Zip;
$company = $sendingToken->Company;
$websiteUrl = $sendingToken->WebsiteUrl;
$partnerType = $sendingToken->PartnerType;
$customCommission = $sendingToken->CustomCommission;
$clientNumber = $sendingToken->NumberOfClients;
$depositAmount = $sendingToken->DepositAmount;
$comment = $sendingToken->Comment;
//FOR CONTACT US PAGE AND FOR LOGOUT SYSTEM
$_SESSION['IBFname'] = $firstName;
$_SESSION['IBLname'] = $lastName;
$_SESSION['IBPhone'] = $mobile;
$_SESSION['username'] = $email;
//GETTING PAYMENT DETAILS FOR USER ONLOAD PAGE
$paymentDetails = $client->GetPaymentDetails(array('token' => $_SESSION['TOKEN']))->GetPaymentDetailsResult;
$method = $paymentDetails->Method;
$accountNumber = $paymentDetails->AccountNumber;
$bankName = $paymentDetails->BankName;
$bankAddress = $paymentDetails->BankAddress;
$bankCountry = $paymentDetails->BankCountry;
$bankState = $paymentDetails->BankState;
$swift = $paymentDetails->Swift;
$iban = $paymentDetails->Iban;
$aba = $paymentDetails->Aba;
$correspondence = $paymentDetails->CorespondenceBank;
//NOT TO ALLOW APPROACH IF THERE IS NO TOKEN
if ($_SESSION['IBFname'] == "" || $_SESSION['username'] == "") {
//    setcookie("IBProfilePassword", '' , time() - 3600,'/');
    header("location: login.php"); //back to login page
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/logofav.jpg"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if IE 7]> <link href="css/ie7.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 8]> <link href="css/ie8.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <!--[if IE 9]> <link href="css/ie9.css" media="screen" rel="stylesheet" type="text/css" ><![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display' rel='stylesheet' type='text/css'/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Oswald:300,400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/tooltipAccountDetails.css"/>
        <link rel="stylesheet" type="text/css" href="css/accountDetails.css" /> <!--styling PROFILE page-->
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
        <style>
            .error {color: #ff0000;}
            .intro {
                color: red;
            }
        </style>
        <title>Account Details</title>
    </head>
    <body>
        <?php include_once("analyticstracking.php") ?>  
        <!-- Start Top Header -->
        <div class="top-header">
            <div class="header-logo">
                <?php
                if (isset($_COOKIE['IBProfilePassword'])) {
                    echo '<a href="../public/accountdetails.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                } else {
                    echo '<a href="../public/index.php">
                    <img class="header-logo" src="../public/img/logo.png" alt=""/>
                </a>';
                }
                ?> 

                <!--                <a href="../public/index.php">
                                    <img class="header-logo" src="img/logo.png" alt=""/>
                                </a>-->
            </div>
            <?php include('../includes/social-icons.php'); ?>
        </div>
        <div class="container-fluid account-details">
            <div class="row">
                <div class="col-lg-3 col-lg-push-9 text-right login-info">
                    <p>Hello&#58;</p>
                    <h3 style="color: #13869E;"><?php echo $_SESSION['username']; ?></h3>
                    <form method="post" action="logout.php">
                        <button name="LOGOUT" type="submit" class="btn btn-primary">Log Out</button>
                    </form>
                </div>
                <!-- Start Menu -->
                <?php include('../includes/menu-admin.php'); ?>
            </div>
        </div>
        <!-- Start Sidebar -->
        <div class="aside" id="wrapper">
            <?php include('../includes/sidebar-wrapper.php'); ?>
            <!-- Start Sidebar -->
            <!-- Page Content -->
            <a href="#menu-toggle" class="btn btn-primary animated fadeInLeft account-toggle" id="menu-toggle">Hide&#47;Show Menu</a>
            <div id="page-content-wrapper">
                <!-- PERSONAL INFORMATION -->
                <?php include('../includes/personal-information.php'); ?>
                <!-- PERSONAL DETAILS CONTAINER -->
                <?php include('../includes/personal-details.php'); ?>
                <!-- CHANGE PASSWORD -->
                <?php include('../includes/change-password.php'); ?>
                <!-- PAYMENT DETAILS -->
                <?php include('../includes/payment-details.php'); ?>
            </div>
        </div>
        <!-- Start Footer -->
        <?php include('../includes/footer.php');  ?>
        <!-- Country finder -->
<!--        <script type="text/javascript">
            var p = document.getElementById("baseCountry");
            var country;
            function jsonpCallback(data) {
                country = data.address.country;
                p.text = country;
                p.value = country;
            }
        </script>-->
        <script src="http://api.wipmania.com/jsonp?callback=jsonpCallback" type="text/javascript"></script><!-- Country finder --> 
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/PassCheck.js"></script>  <!--password edit check-->
        <script src="js/main.js" type="text/javascript"></script>
        <script type="text/javascript">
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <!-- Container Hide/Show -->
        <script type="text/javascript">
            //DEFAULT ONLOAD PAGE
            $(document).ready(function () {
                $('#personalInformation').addClass('focused');
                $('#personalInformationContainer').show();
            });
            //PERSONAL DETAILS (BASIC PAGE TAB)
            $("#personalDetails").click(function (e) {
                e.preventDefault();
                $("#personalDetailsContainer").show(500);
                $("#personalDetails").addClass('focused');
                $("#changePasswordContainer").hide(500);
                $("#changePassword").removeClass('focused');
                $("#marketingInformationContainer").hide(500);
                $("#marketingInformation").removeClass('focused');
                $("#paymentDetails").removeClass('focused');
                $("#paymentDetailsContainer").hide(500);
                $("#personalInformationContainer").hide(500);
                $("#personalInformation").removeClass('focused');
            });
            //CHANGE PASSWORD
            $("#changePassword").click(function (e) {
                e.preventDefault();
                $("#changePasswordContainer").show(500);
                $("#changePassword").addClass('focused');
                $("#personalDetailsContainer").hide(500);
                $("#personalDetails").removeClass('focused');
                $("#marketingInformationContainer").hide(500);
                $("#marketingInformation").removeClass('focused');
                $("#paymentDetails").removeClass('focused');
                $("#paymentDetailsContainer").hide(500);
                $("#personalInformationContainer").hide(500);
                $("#personalInformation").removeClass('focused');
            });
            // PAYMENT DETAILS
            $("#paymentDetails").click(function () {
                $("#paymentDetails").addClass('focused');
                $("#paymentDetailsContainer").show(500);
                $("#personalDetailsContainer").hide(500);
                $("#personalDetails").removeClass('focused');
                $("#changePasswordContainer").hide(500);
                $("#changePassword").removeClass('focused');
                $("#marketingInformationContainer").hide(500);
                $("#marketingInformation").removeClass('focused');
                $("#personalInformationContainer").hide(500);
                $("#personalInformation").removeClass('focused');
            });
            //PERSONAL INFORMATION
            $("#personalInformation").click(function () {
                $('#personalInformation').addClass('focused');
                $('#personalInformationContainer').show(500);
                $("#personalDetailsContainer").hide(500);
                $("#personalDetails").removeClass('focused');
                $("#changePasswordContainer").hide(500);
                $("#changePassword").removeClass('focused');
                $("#marketingInformationContainer").hide(500);
                $("#marketingInformation").removeClass('focused');
                $("#paymentDetails").removeClass('focused');
                $("#paymentDetailsContainer").hide(500);
            });
        </script>
        <!--TIME FOR SHOWING UPDATE MESSAGE-->
        <script type="text/javascript">
            setTimeout(function () {
                $(".updateTrue").fadeOut().empty();
            }, 5000);
        </script>
        <script type="text/javascript">
            setTimeout(function () {
                $(".updateFalse").fadeOut().empty();
            }, 7000);
        </script>
        <!--UPDATE USER DETAILS-->
        <script type="text/javascript">
            $(document).ready(function () {
                $("#userDetailsUpdateForm").submit(function (e) {
                    e.preventDefault();
                    $(".updatingDetailsAnimation").removeAttr("hidden");
                    var firstName = document.getElementById("IBFirstName").value;
                    var lastName = document.getElementById("IBLastName").value;
                    var phone = document.getElementById("IBphone").value;
                    var mobile = document.getElementById("IBmobile").value;
//                    var country = document.getElementById("IBCountry").value;
                    var country = $(".CountryList").val();
                    var city = document.getElementById("IBCity").value;
                    var address = document.getElementById("IBAddress").value;
                    var zip = document.getElementById("IBZip").value;
                    var company = document.getElementById("IBCompany").value;
                    var url = document.getElementById("IBUrl").value;
//                    var partnerType = document.getElementById("IBPartnerType").value;
                    var partnerType = $(".partnershipType").val();
                    var token = "<?php echo $_SESSION['TOKEN']; ?>";
                    var datas = "firstName=" + firstName + "&token=" + token + "&lastName=" + lastName + "&phone=" + phone + "&mobile=" + mobile + "&country=" + country + "&city=" + city + "&address=" + address + "&zip=" + zip + "&company=" + company + "&url=" + url + "&partnerType=" + partnerType;
                    $.ajax({
                        type: "post",
                        url: "IBEditDetails.php",
                        data: datas,
                        cache: false,
                        success: function (message) {
                            $('.userDetailsUpdateResult').html(message);
                            setTimeout(function () {
                                $(".userDetailsUpdateResult").empty();
                            }, 5000);
                        }
                    });
                    return false;
                });
            });
        </script>
        <!--UPDATING PASSWORD FUNCTION-->
        <script type="text/javascript">
            $(document).ready(function () {
                $("#updatePasswordForm").submit(function (event) {
                    event.preventDefault();
                    $(".changePasswordAnimation").removeAttr("hidden");
                    var pass = document.getElementById('oldPasswordIB').value;
                    var newPass = document.getElementById('NewPassword').value;
                    var token = "<?php echo $_SESSION['TOKEN']; ?>";
                    var data = "pass=" + pass + "&newPass=" + newPass + "&token=" + token;
                    $.ajax({
                        type: 'POST',
                        url: "IBpassChange.php",
                        data: data,
                        cache: false,
                        success: function (html) {
                            $('.passUpdateResult').html(html);
                            setTimeout(function () {
                                $(".passUpdateResult").empty();
                            }, 5000);
                        }
                    });
                    return false;
                });
            });
        </script>
        <!--AJAX FOR UPDATING PAYMENT DETAILS FOR USER-->
        <script type="text/javascript">
            $(document).ready(function () {
                $("#updatePaymentDetails").submit(function (e) {
                    e.preventDefault();
                    $(".updatingPaymentAnimation").removeAttr("hidden");
                    var method = document.getElementById('paymentMethod').value;
                    var accountNumber = document.getElementById('accountNumber').value;
                    var bankName = document.getElementById('bankName').value;
                    var bankAddress = document.getElementById('bankAddress').value;
                    var bankCountry = document.getElementById('bankCountry').value;
                    var bankState = document.getElementById('bankState').value;
                    var swift = document.getElementById('swift').value;
                    var iban = document.getElementById('iban').value;
                    var aba = document.getElementById('aba').value;
                    var correspondence = document.getElementById('correspondence').value;
                    var sendData = 'method=' + method + '&accountNumber=' + accountNumber + '&bankName=' + bankName + '&bankAddress=' + bankAddress + '&bankCountry=' + bankCountry
                            + '&bankState=' + bankState + '&swift=' + swift + '&iban=' + iban + '&aba=' + aba + '&correspondence=' + correspondence;
                    $.ajax({
                        type: "post",
                        url: "updatePayment.php",
                        data: sendData,
                        cache: false,
                        success: function (message) {
                            $('#paymentUpdate').html(message);
                            setTimeout(function () {
                                $("#paymentUpdate").empty();
                            }, 5000);
                        }
                    });
                    return false;
                });
            });
        </script>
        <script type="text/javascript">
            $('#NewPassword').keyup(function () {
                var pass = document.getElementById('NewPassword').value;
                var RePass = document.getElementById('confirmNewPassword').value;
                if (pass.length < 5 || pass != RePass) {
                    $('#confirmNewPassword').css('border', '2px solid red');
                    $('#updatePassword').attr('disabled', true);
                } else {
                    $('#confirmNewPassword').css('border', '2px solid green');
                    $('#updatePassword').removeAttr('disabled');
                }
            });
            $('#confirmNewPassword').keyup(function () {
                var pass = document.getElementById('NewPassword').value;
                var RePass = document.getElementById('confirmNewPassword').value;
                if (RePass.length < 5 || pass != RePass) {
                    $('#confirmNewPassword').css('border', '2px solid red');
                    $('#updatePassword').attr('disabled', true);
                } else {
                    $('#confirmNewPassword').css('border', '2px solid green');
                    $('#updatePassword').removeAttr('disabled');
                }
            });
        </script>
        <script type="text/javascript">
            $(".activeProfile span").css("color", "#ff9900");
        </script>
    </body>
</html>